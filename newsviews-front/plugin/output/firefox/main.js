function NewsViewsExtension() {
    var self = this;
    
    var details = {
        url: 'main.css',
        method: 'GET',
        async: true,
        contentType: 'text'
    };
    kango.xhr.send(details, function(data) {
            var content = data.response;
            kango.console.log(content);
            addStyle(content);
    });
    
    kango.ui.browserButton.addEventListener(kango.ui.browserButton.event.COMMAND, function() {
        if (self.isShowingNewsViews) {
            // Cancel the loading
            self.closeNewsViews();
        } else {
            // Load NewsViews
            self.runNewsViews();
        }
    });
    
    kango.addMessageListener('NewsViewsRequestClose', function(event) {
        self.closeNewsViews(); 
    });
}

NewsViewsExtension.prototype = {
    runNewsViews: function() {
        this.isShowingNewsViews = true;
        kango.browser.tabs.getCurrent(function(tab) {
            tab.dispatchMessage("NewsViewsRun", {});
        });
    },
    closeNewsViews:function() {
        this.isShowingNewsViews = false;
        kango.browser.tabs.getCurrent(function(tab) {
            tab.dispatchMessage("NewsViewsClose", {});
        });
    },
    isShowingNewsViews: false
};

var extension = new NewsViewsExtension();
