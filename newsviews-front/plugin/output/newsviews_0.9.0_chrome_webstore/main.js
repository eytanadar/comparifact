function NewsViewsExtension() {
    var self = this;
    kango.ui.browserButton.addEventListener(kango.ui.browserButton.event.COMMAND, function() {
        self._onCommand();
    });
}

NewsViewsExtension.prototype = {

    _onCommand: function() {
        kango.browser.tabs.create({url: 'http://kangoextensions.com/'});
    }
};

var extension = new NewsViewsExtension();