// ==UserScript==
// @name NewsViews
// @include http://*
// @include https://*
// ==/UserScript==

var _baseURL = 'http://104.236.255.214', _backgroundDiv = null, _containerDiv = null, _cleanUpTimeout = null, content = null;

var keys = {37: 1, 38: 1, 39: 1, 40: 1};

function isInNewsViewsDiv (e) {
    if (_backgroundDiv != null && e.target == _backgroundDiv) {
        return true;
    }
    if (_containerDiv != null && e.target == _containerDiv) {
        return true;
    }
    if (content != null && e.target == content) {
        return true;
    }
    
    return false;
}

function preventDefault(e) {
  e = e || window.event;
  if (isInNewsViewsDiv) {
      return;
  }

  if (e.preventDefault)
      e.preventDefault();
  e.returnValue = false;  
}

function preventDefaultForScrollKeys(e) {
    if (isInNewsViewsDiv) {
      return;
    }
    
    if (keys[e.keyCode]) {
        preventDefault(e);
        return false;
    }
}

function disableScroll() {
  if (window.addEventListener) // older FF
      window.addEventListener('DOMMouseScroll', preventDefault, false);
  window.onwheel = preventDefault; // modern standard
  window.onmousewheel = document.onmousewheel = preventDefault; // older browsers, IE
  window.ontouchmove  = preventDefault; // mobile
  document.onkeydown  = preventDefaultForScrollKeys;
}

function enableScroll() {
    if (window.removeEventListener)
        window.removeEventListener('DOMMouseScroll', preventDefault, false);
    window.onmousewheel = document.onmousewheel = null; 
    window.onwheel = null; 
    window.ontouchmove = null;  
    document.onkeydown = null;  
}

kango.addMessageListener('NewsViewsRun', function(event) {
    if (_cleanUpTimeout) {
        clearTimeout(_cleanUpTimeout);
        _cleanUpTimeout = null;
    }

    if (_backgroundDiv == null) {
        disableScroll();
        
        _backgroundDiv = document.createElement('div');
        document.getElementsByTagName('body')[0].appendChild(_backgroundDiv);
        
        _backgroundDiv.style.position = 'fixed';
        _backgroundDiv.style.left = '0';
        _backgroundDiv.style.right = '0';
        _backgroundDiv.style.top = '0';
        _backgroundDiv.style.bottom = '0';
        _backgroundDiv.style.background = 'rgb(230, 230, 230)';
        _backgroundDiv.style.opacity = '0.0';
        _backgroundDiv.style.zIndex = '2147483647';
        
        // _backgroundDiv.style.-webkit-transition = 'opacity 0.75s';
        // _backgroundDiv.style.-moz-transition = 'opacity 0.75s';
        _backgroundDiv.style.transition = 'opacity 0.33s';
        
        _backgroundDiv.onclick = function() {
            kango.dispatchMessage("NewsViewsRequestClose", {});
        };
        
        _containerDiv = document.createElement('div');
        document.getElementsByTagName('body')[0].appendChild(_containerDiv);
        
        _containerDiv.style.position = 'fixed';
        _containerDiv.style.left = '0';
        _containerDiv.style.right = '0';
        _containerDiv.style.top = '100%';
        _containerDiv.style.height = '100%';
        _containerDiv.style.background = 'transparent';
        _containerDiv.style.zIndex = '2147483647';
        
        _containerDiv.style.transition = 'top 0.33s cubic-bezier(.07,.58,.3,1.06)';
        
        _containerDiv.onclick = function() {
            kango.dispatchMessage("NewsViewsRequestClose", {});
        };
        
        content = document.createElement('div');
        _containerDiv.appendChild(content);
        
        content.style.boxShadow = '0 0 5px rgba(0, 0, 0, 0.4)';
        content.style.backgroundColor = 'rgb(251, 251, 251)';
        content.style.maxWidth = '94ex';
        content.style.width = '95%';
        content.style.margin = '2.5% auto 2.5% auto';
        content.style.display = 'block';
        content.style.height = '95%';
        
        content.innerHTML = '<iframe scrolling="yes" frameborder="0" id="newsViewsFrame" name="newsViewsFrame" src="' + 
                            _baseURL + '?plugin=' + encodeURIComponent(window.location.href) + 
                            '" style="position: relative; width:100%; top:0px; left:0px; height:100%; display:block; border-style:none;">' +
                            '</frame>';
    }
    
    setTimeout(function() {
        // Animate the background change
        _backgroundDiv.style.opacity = '1.0';
        _containerDiv.style.top = '0%';
    }, 1);
});

kango.addMessageListener('NewsViewsClose', function(event) {
    if (_backgroundDiv != null) {
        _backgroundDiv.style.opacity = '0.0';
        _containerDiv.style.top = '100%';
        
        _cleanUpTimeout = setTimeout(function() {
            enableScroll();
            
            content = null;
            
            windowProxy.removeEventListener(onMessage);
            windowProxy = null;
            
            _backgroundDiv.parentElement.removeChild(_backgroundDiv);
            _backgroundDiv = null;
            
            _containerDiv.parentElement.removeChild(_containerDiv);
            _containerDiv = null;
            
            _cleanUpTimeout = null;
        }, 330);
    }
});

