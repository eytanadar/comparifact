var dataMap = d3.map();
var projection;

function loadMap(keys, values, title, yRange, isStateLevel, annotations, initialLocations) {
    var container = $('#visualization-container');

    container.append("<div class='map-container'><div class='fill'></div></div>");
    container = container.find('.fill');

    var margins = {
        left: 0,
        right: 0,
        top: 0,
        bottom: 0
    };

    var width = container.width() - margins.left - margins.right;
    var height = container.height() - margins.top - margins.bottom;

    dataMap = d3.map();
    for (var i = 0; i < keys.length; i++) {
        dataMap.set(keys[i], values[i]);
    }

    var scale = createScale(yRange);
    /*var projection = d3.geo.albersUsa()
        .scale(width)
        .translate([width / 2 + margins.left, height / 2 + margins.top]);*/
    
    if (showMapBackground) {
        projection = d3.geo.mercator()
            .center([-96, 38.3])
            .scale(width);
    } else {
        projection = d3.geo.albersUsa()
            .scale(width)
            .translate([width / 2 + margins.left, height / 2 + margins.top]);
    }

    var path = d3.geo.path()
        .projection(projection);
    
    var zoom = d3.behavior.zoom()
        .translate([0, 0])
        .scale(1)
        .scaleExtent([1, 10])
        .on("zoom", zoomed);
    
    var graph = d3.select(container.get(0)).append("svg:svg")
        .attr("width", width + margins.left + margins.right)
        .attr("height", height + margins.top + margins.bottom)
        .call(zoom)
        .append("svg:g")
        .attr("transform", "translate(" + margins.left + "," + margins.top + ")");
    
    var tile, raster;
    if (showMapBackground) {
        tile = d3.geo.tile()
            .size([width, height]);
        raster = graph.append("g")
            .attr("class", "tile-background");
    }
        
    
    var features = {};
    var className = "";

    if (isStateLevel) {
        features = topojson.feature(us, us.objects.states).features;
        className = "states";
    } else {
        features = topojson.feature(us, us.objects.counties).features;
        className = "counties";
    }
    
    var initialBounds = null;
    var boundedMin = Number.POSITIVE_INFINITY, boundedMax = Number.NEGATIVE_INFINITY;
    graph.append('g')
        .attr("class", className)
        .selectAll("path")
        .data(features)
        .enter().append("path")
        .each(function(d) {
            var value = dataMap.get(d.id);
            
            var currentLocationIsInInitialLocations =
                (initialLocations[d.id] !== undefined) ||
                (String(d.id).length == 4 && initialLocations[String(d.id).substring(0,1)] !== undefined) || // Single digit state fips code
                (String(d.id).length == 5 && initialLocations[String(d.id).substring(0,2)] !== undefined); // DOuble digit state fips codes
            
            if (currentLocationIsInInitialLocations) {
                // We should update our bounds as appropraite
                if (initialBounds == null) {
                    initialBounds = path.bounds(d);
                } else {
                    bounds = path.bounds(d);
                    if (initialBounds[0][0] > bounds[0][0]) {
                        initialBounds[0][0] = bounds[0][0]
                    }
                    if (initialBounds[0][1] > bounds[0][1]) {
                        initialBounds[0][1] = bounds[0][1]
                    }
                    if (initialBounds[1][0] < bounds[1][0]) {
                        initialBounds[1][0] = bounds[1][0]
                    }
                    if (initialBounds[1][1] < bounds[1][1]) {
                        initialBounds[1][1] = bounds[1][1]
                    }
                }
                
                boundedMin = Math.min(value, boundedMin);
                boundedMax = Math.max(value, boundedMax);
            }
        })
        .attr("class", entityScale(yRange, scale))
        .attr("d", path);
    
    if (!isStateLevel) {
        // Add outline around the states
        graph.append('g')
            .attr("class", "stateOutline")
            .append("path")
            .datum(topojson.mesh(us, us.objects.states, function (a, b) {
                return a !== b;
            }))
            .attr("class", "statesOutlines")
            .attr("d", path);
    }

    var locationsById = {};
    for (var i = 0; i < features.length; i++) {
        var feature = features[i];
        locationsById[feature.id] = feature;
    }

    // Extract all the place that have annotations
    var annotatedPlaces = [];
    for (var i = 0; i < annotations.length; i++) {
        var annotation = annotations[i];

        // Get the location
        var location = [0, 0];
        var coordinates = JSON.parse(annotation.location);

        if (coordinates.hasOwnProperty('lng')) {
            location = projection([coordinates.lng, coordinates.lat]);
        } else {
            var feature = locationsById[annotation.location];
            if (feature == undefined) {
                continue;
            }
            location = path.centroid(feature);
        }

        annotatedPlaces.push({
            title: annotation.title,
            text: annotation.text,
            url: annotation.url,
            location: location
        });
    }

    displayAnnotations(graph, {
        width: width,
        height: height
    }, annotatedPlaces);

    graph.append("text")
        .attr("x", (width / 2))
        .attr("y", (margins.top / 2 + 20))
        .attr("class", "title")
        .attr("text-anchor", "middle")
        .text(title);
    
    function updateTiles() {
        if (!showMapBackground) {
            return;
        }
        
        var projectionOrigin = projection([0, 0]);
        var zoomTranslation  = zoom.translate();
        projectionOrigin[0] *= zoom.scale();
        projectionOrigin[1] *= zoom.scale();
        
        var tiles = tile
            .scale(projection.scale() * zoom.scale() * 2 * Math.PI)
            .translate([projectionOrigin[0] + zoomTranslation[0], projectionOrigin[1] + zoomTranslation[1]])
            .zoomDelta((window.devicePixelRatio || 1) - .5)();

        var image = raster
            .attr("transform", "scale(" + tiles.scale + ")translate(" + tiles.translate[0] + "," + tiles.translate[1] + ")")
            .selectAll("image")
            .data(tiles, function(d) { return d; });

        image.exit()
            .remove();

        image.enter().append("image")
            .attr("xlink:href", function(d) { 
                return "https://api.mapbox.com/v4/mapbox.streets/" + d[2] + "/" + d[0] + "/" + d[1] + ".png?access_token=pk.eyJ1IjoieXVyb3BhIiwiYSI6IktZNTY0SEEifQ.-y0oZrmBr7GEbhejFYdqcA";
            })
            .attr("width", 1)
            .attr("height", 1)
            .attr("x", function(d) { return d[0]; })
            .attr("y", function(d) { return d[1]; });
    }
    
    
    
    // Apply the determined bounds and scale to the map
    if (initialBounds != null) {
        var dx = initialBounds[1][0] - initialBounds[0][0],
            dy = initialBounds[1][1] - initialBounds[0][1],
            x = (initialBounds[0][0] + initialBounds[1][0]) / 2,
            y = (initialBounds[0][1] + initialBounds[1][1]) / 2,
            scale = .9 / Math.max(dx / width, dy / height),
            translate = [width / 2 - scale * x, height / 2 - scale * y];
    
        graph.transition().duration(0).call(zoom.translate(translate).scale(scale).event);
    }
    
    // All the locations to be toggleable if already enabled
    setEnableEntityToggling(isInVisualizationAreaEditMode);
    
    // Set the scale
    if (Math.abs(boundedMin - boundedMax) > 0.001 && boundedMin < Number.POSITIVE_INFINITY, boundedMax > Number.NEGATIVE_INFINITY) {
        $('#binning-min').val(boundedMin);
        $('#binning-max').val(boundedMax);
        loadVisualizationScale();
    }
    
    updateTiles();
    
    // Add zooming
    function zoomed() {
        graph.select('.' + className).attr("transform", "translate(" + d3.event.translate[0] + "," + d3.event.translate[1] + ")scale(" + d3.event.scale + ")");
        graph.select(".stateOutline").attr("transform", "translate(" + d3.event.translate[0] + "," + d3.event.translate[1] + ")scale(" + d3.event.scale + ")");
        graph.select(".statesOutlines").style("stroke-width", 2.0 / d3.event.scale + "px");
        graph.select(".states").style("stroke-width", 1.0 / d3.event.scale + "px");
        updateTiles();
        
        graph.select('.annotationsAnchors').attr("transform", "translate(" + d3.event.translate[0] + "," + d3.event.translate[1] + ")scale(" + d3.event.scale + ")");
        graph.select('.annotationsAnchors').style("stroke-width", 1.0 / d3.event.scale + "px");
        graph.select('.annotationsAnchors').selectAll('circle').attr("r", 2.0 / d3.event.scale + "px");
        
        graph.select('.annotationConnectors').attr("transform", "translate(" + d3.event.translate[0] + "," + d3.event.translate[1] + ")scale(" + d3.event.scale + ")");
        graph.select('.annotationConnectors').selectAll('line').style("stroke-width", 1.0 / d3.event.scale + "px");
        
        $('.map-container').find('.annotationTextContainer')
            .css("top", d3.event.translate[1] + 'px')
            .css("left", d3.event.translate[0] + 'px')
            .css("transform", "scale(" + d3.event.scale + ")")
            .each(function() {
                var child = $(this).find('.annotationContainer');
                child.css("transform", "scale(" + (1.0/d3.event.scale) + ")");
            });
    }
    
    // Add resizing
    function resize() {
        // adjust things when the window size changes
        width = container.width() - margins.left - margins.right;
        height = container.height() - margins.top - margins.bottom;

        // update projection
        projection.translate([width / 2 + margins.left, height / 2 + margins.top])
            .scale(width);

        // resize the map container
        d3.select(graph.node().parentNode).attr("width", width + margins.left + margins.right)
             .attr("height", height + margins.top + margins.bottom)

        // resize the map
        d3.selectAll("path").attr('d', path);
        d3.select('.title').attr("x", (width / 2));
        
        tile = d3.geo.tile().size([width, height]);
        updateTiles();
    }
    
    graph.on('click', function(e) {
        // Process the click event
        if (!areAnnotationsAddable) {
            return;
        }
        
        // Add a annotation at the click point
        
        /*
        location: [414.3437049980968, 345.03771235502]
        text: "The minimum value is 28.7 at 8025"
        title: "Minimum Value"
        url: ""
        */
        
        var location = d3.mouse(this);
        
        var annotation = {
            location: location,
            text: "",
            title: "New Location",
            url: ""
        };
        addAnnotation(annotation);
        d3.event.stopPropagation();
    });
    
    d3.select(window).on('resize', resize);
}

function mapTranslation() {
    var linesTransform = $('#visualization-container')
        .find('.annotationConnectors')
        .attr('transform');
    
    var digitString = '\\d+';
    var signString = '[+-]?';
    var numberString = signString + digitString + '\\.?' + '\\d*' + signString + '[Ee]?' + '\\d*';
    
    var translationString = linesTransform.match('\s*translate\\(\\s*' + numberString + '\\s*\\,\\s*' + numberString + '\\s*\\)')[0];
    var translation = translationString.replace('translate', '').replace('(', '').replace(')', '').split(',').map(function(x) {return parseFloat(x.trim());});
    return translation;
}

function mapScale() {
    var linesTransform = $('#visualization-container')
        .find('.annotationConnectors')
        .attr('transform');
    
    var digitString = '\\d+';
    var signString = '[+-]?';
    var numberString = signString + digitString + '\\.?' + '\\d*' + signString + '[Ee]?' + '\\d*';
    
    var scaleString = linesTransform.match('\s*scale\\(\\s*' + numberString + '\\s*\\)')[0];
    var scale = parseFloat(scaleString.match(numberString)[0]);
    return scale;
}

function setEnableEntityToggling(enabled) {
    var container = $('#visualization-container');
    var elements = d3.select(container.get(0)).selectAll(".states, .counties").selectAll('path');
    
    elements.classed('graph-entity-hover', enabled)
    
    if (enabled) {
        elements.on('click', function(){
            var entity = d3.select(this);
            entity.classed('graph-entity-hidden', !entity.classed('graph-entity-hidden'));
        });
    } else {
        elements.on('click', null);
    }
}

function createScale(yRange) {
    if (useQuantizeScale) {
        return d3.scale.quantile()
            .range(d3.range(9).map(function (i) {
                return "q" + i + "-9";
            }))
            .domain(yRange);
    } else {
        return d3.scale.threshold()
            .domain(ss.jenks(dataMap.values(), 9))
            .range(d3.range(9).map(function (i) {
                return "q" + (i - 1) + "-9";
            }));
    }
}

function entityScale(yRange, scale) {
    return function(d) { 
        var value = dataMap.get(d.id);
        if (value == undefined) {
            value = yRange[0];
        }

        var s = scale(value);
        if (s == undefined) {
            return "q8-9";
        }
        return s;
    }
}

function udpateScale(yRange) {
    var scale = createScale(yRange);
    
    var container = $('#visualization-container');
    var elements = d3.select(container.get(0)).selectAll(".states, .counties").selectAll('path');
    elements.attr("class", entityScale(yRange, scale));
    
    // Make sure entity toggling is still enabled if needed
    setEnableEntityToggling(isInVisualizationAreaEditMode);
}
