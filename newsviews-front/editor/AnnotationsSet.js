function AnnotationsSet(type) {
    this.type = type;
    this._count = {};
    this.onChange = function(){};
}

AnnotationsSet.prototype.clear = function() {
    this._count = {};
    this.onChange();
}

AnnotationsSet.prototype.add = function(text) {
    if (typeof text !== 'string') {
        if (!$(text).hasClass(this.type)) {
            return;
        }
        
        text = $(text).text();
    } 
    
    text = text.trim();
    
    if (text in this._count) {
        this._count[text] = this._count[text] + 1;
    } else {
        this._count[text] = 1;
    }
    
    this.onChange();
}

AnnotationsSet.prototype.remove = function(text) {
    if (typeof text !== 'string') {
        if (!$(text).hasClass(this.type)) {
            return;
        }
        text = $(text).text();
    }
    
    text = text.trim();
    
    this._count[text] = this._count[text] - 1;
    if (this._count[text] <= 0) {
        delete this._count[text];
    }
    
    this.onChange();
}

AnnotationsSet.prototype.contains = function(str) {
    if (typeof str !== 'string') {
        if (!$(str).hasClass(this.type)) {
            return false;
        }
        
        str = $(str).text();
    }
    
    str = str.trim();
    
    if (str in this._count) {
        return true;
    }
    
    return false;
}

AnnotationsSet.prototype.list = function() {
    return Object.keys(this._count).sort();
}