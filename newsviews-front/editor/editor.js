var popupHTML = 
'<div class="ui special popup annotation">' +
    '<div class="ui four column divided center aligned grid" style="width: 500px;">' +
        '<div class="column">' +
            '<p>This is <em>what</em> to visualize</p>' +
            '<button class="ui basic button noun-button" id="annotation-noun">Choose</button>' +
        '</div>' +
        '<div class="column">' +
            '<p>This is <em>where</em> to visualize</p>' +
            '<button class="ui basic button location-button" id="annotation-location">Choose</button>' +
        '</div>' +
        '<div class="column">' +
            '<p>This is <em>when</em> to visualize</p>' +
            '<button class="ui basic button date-button" id="annotation-date">Choose</button>' +
        '</div>' +
        '<div class="column">' +
            '<button class="ui basic button location-button" id="add-map-button" style="position: absolute; right: 31px; top: 15px; padding-left: 0.5rem; padding-right: 0.5rem;">Add to Map</button>' +
            '<button class="ui button red" id="annotation-delete" style="position: absolute; right: 31px; bottom: 15px;">Delete</button>' +
        '</div>' +
    '</div>' +
'</div>';

function annotateSelection(type) {
    if (window.getSelection) {
        var sel = window.getSelection();
        if (sel.rangeCount) {
            var range = sel.getRangeAt(0).cloneRange();
            /*if ($(range.commonAncestorContainer).parent('#text-area').length <= 0) {
                return;
            }*/
            
            var span = highlight(range, type);
            sel.removeAllRanges();
            $(span).popup('show');
            
            // Load the variables
            loadVariables(document.getElementById('var-load-button'));
        }
    }
}

function annotateText(sender) {
    $(sender).addClass('loading disabled');
    
    clearAnotations();
    
    var textArea = document.getElementById('text-area');
    var data = {
        article: $(textArea).text(),
        url: '',
        date: $('#article-date').text(),
        usePMI: usePMI
    }; 
    asyncReqst({
        url: baseURL + "/tag",
        async: true,
        type: 'POST',
        data: JSON.stringify(data),
        dataType: 'text',
        contentType: 'text/plain',
        success: function (result) {
            $(sender).removeClass('loading disabled');
            
            result = JSON.parse(result);
            for (var i = 0; i < result.length; i++) {
                var annotation = result[i];
        
                var range = document.createRange();
                var textArea = document.getElementById('text-area');
                
                var start = elementForCharacterIndex(textArea, annotation.start);
                var end = elementForCharacterIndex(textArea, annotation.end);
                range.setStart(start.element, start.index);
                range.setEnd(end.element, end.index + 1); // Move to contain the last character
                
                // Skip the numbers for the time being
                if (annotation.type == 'NUMBER' || annotation.type == 'PERCENT' || annotation.type == 'MONEY') {
                    continue;
                }
                
                highlight(range, annotation.type);
            }
            
            loadVariables(document.getElementById('var-load-button'));
        }, 
        error: function() {
            $(sender).removeClass('loading disabled');
        }
    });
}

function elementForCharacterIndex(element, index) {
    if (element.nodeType == 3) {
        // Text node
        if (index < element.length) {
            return {
                element: element,
                index: index
            }
        } else {
            return {
                element: null,
                index: element.length
            }
        }
    }
    
    // This isn't a text node so we'll need to interate over all the children
    var length = 0;
    for (var i = 0; i < element.childNodes.length; i++) {
        var child = element.childNodes[i];
        var result = elementForCharacterIndex(child, index - length);
        
        if (result.element != null) {
            // The child contained the index
            return result;
        } else {
            // The child wasn't long enough to contain the index
            length += result.index;
        }
    }
    
    return {
        element: null,
        index: length
    }
}

var rangeElements = [];
var lastPopupSpan = null;
function highlight(range, type) {
    var span = document.createElement('span');
    span.current_text_range = range;
    
    var typeClass = '';
    var text = $(range.cloneContents()).text();
    if (type == 'LOCATION') {
        typeClass = 'location';
        annotatedLocations.add(text);
    } else if (type == 'OTHER' || type == 'PERSON' || type == 'ORGANIZATION') {
        typeClass = 'noun';
        annotatedNounPharses.add(text);
    } else if (type == 'DATE') {
        typeClass = 'date';
        annotatedDates.add(text);
    } else if (type == 'NUMBER' || type == 'PERCENT' || type == 'MONEY') {
        typeClass = 'number';
    }
    
    span.className = 'ui extracted ' + typeClass;
    range.surroundContents(span);
    rangeElements.push(span);
    
    if ($('.special.popup.annotation').length == 0) {
        $(document.body).append(popupHTML);
    }
    
    // Add popup menu
    $(span).popup({
        popup: '.special.popup.annotation',
        on : 'click',
        exclusive : false,
        onShow: function() {
            if (lastPopupSpan != null) {
                $(lastPopupSpan).popup('hide');
            }
            lastPopupSpan = span;
        },
        onVisible: function(s, pop) {
            
            var p = $('.special.popup.annotation');
            
            function updateAddToMapButton() {
                var button = p.find('#add-map-button');
                if ($(span).hasClass('location')) {
                    button.prop("disabled",false);
                } else {
                    button.prop("disabled",true);
                }
            }
            
            p.find('#annotation-delete').off('click').on('click', function() {
                $(span).popup('hide');
                
                lastPopupSpan = null;
                removeAnnotation(span);
                var index = rangeElements.indexOf(span);
                rangeElements.splice(index, 1);
                updateAddToMapButton();
            });
            p.find('#annotation-date').off('click').on('click', function() {
                annotatedLocations.remove(span);
                annotatedNounPharses.remove(span);
                annotatedDates.remove(span);
                
                var classString = 'date';
                span.className = 'ui extracted ' + classString;
                annotatedDates.add(span);
                updateAddToMapButton();
            });
            p.find('#annotation-location').off('click').on('click', function() {
                annotatedLocations.remove(span);
                annotatedNounPharses.remove(span);
                annotatedDates.remove(span);
                
                var classString = 'location';
                span.className = 'ui extracted ' + classString;
                annotatedLocations.add(span);
                updateAddToMapButton();
            });
            p.find('#annotation-noun').off('click').on('click', function() {
                annotatedLocations.remove(span);
                annotatedNounPharses.remove(span);
                annotatedDates.remove(span);
                
                var classString = 'noun';
                span.className = 'ui extracted ' + classString;
                annotatedNounPharses.add(span);
                updateAddToMapButton();
            });
            p.find('#add-map-button').off('click').on('click', function() {
                var location = $(span).text();
                annotateLocations([location]);
            });
            updateAddToMapButton();
        }
    });
    
    return span;
}

var annotatedLocations = new AnnotationsSet('location');
var annotatedNounPharses = new AnnotationsSet('noun');
var annotatedDates = new AnnotationsSet('date');
function clearAnotations() {
    while (rangeElements.length > 0) {
        var element = rangeElements.pop();
        removeAnnotation(element);
    }
    
    annotatedLocations.clear();
    annotatedNounPharses.clear();
    annotatedDates.clear();
}

function removeAnnotation(element) {
    annotatedLocations.remove(element);
    annotatedNounPharses.remove(element);
    annotatedDates.remove(element);
    
    var parent = element.parentNode;
    while (element.firstChild) {
        parent.insertBefore(element.firstChild, element);
    }
    parent.removeChild(element);
    
    loadVariables(document.getElementById('var-load-button'));
}

annotatedLocations.onChange = function() {
    highlightLocationsInComparisons();
}

annotatedNounPharses.onChange = function() {
    updateHighlighedNounsInComparisons();
}

annotatedDates.onChange = function() {
    updateHighlighedDatesInComparisons();
}

function loadVariables(sender) {
    $(sender).addClass('loading disabled');
    
    var keywords = [];
    for (var i = 0; i < rangeElements.length; i++) {
        var span = $(rangeElements[i]);
        if (span.hasClass('noun')) {
            keywords.push(span.text());
        }
    }

    
    var data = {
        keywords: keywords,
        usePMI: usePMI
    }
    
    clearVariables();
    
    asyncReqst({
        url: baseURL + "/find-variables",
        async: true,
        type: 'POST',
        data: JSON.stringify(data),
        dataType: 'text',
        contentType: 'text/plain',
        success: function (result) {
            $(sender).removeClass('loading disabled');
        
            result = JSON.parse(result);
            var table = $('#variables-table');
            
            var html = '';
            for (var i = 0; i < result.length; i++) {
                html += '<tr class="scroll-table-cell"><td class="scroll-table-cell-data">';
                html += result[i];
                html += '</td></tr>';
            }
            table.html(html);
            
            table.find('.scroll-table-cell').click(function() {
                $(this).addClass('scroll-table-cell-selected').siblings().removeClass('scroll-table-cell-selected');
                var variable = $(this).find('.scroll-table-cell-data').text();
                loadVariable(variable);
            });
    
            // The second child will be the highest ranking varible which isn't a reference map
            loadVariable(table.find('.scroll-table-cell:nth-child(1)').text());
        }, 
        error: function() {
            $(sender).removeClass('loading disabled');
        }
    });
}

function clearVariables() {
    $('#variables-table').children().remove();
    
    // Remove the table sorter
    $('#data-table-container').unbind('appendCache applyWidgetId applyWidgets sorton update updateCell click.tablesort')
        .removeClass('sortable')
        .find('thead th')
        .unbind('click mousedown')
        .removeClass('sorted ascending descending');
}

function loadVariable(variable) {
    var data = {
        variable: variable,
        usePMI: usePMI
    };
    
    clearTable();
    $('#data-table').append('<div class="ui text loader" style="position: relative; display: block; left: 50%; top: 50%;"></div>');
    
    // Set the Variable Field
    $('#variable-field').val(variable);
    
    asyncReqst({
        url: baseURL + "/variable-data",
        async: true,
        type: 'POST',
        data: JSON.stringify(data),
        dataType: 'text',
        contentType: 'text/plain',
        success: function (result) {
            $('#data-table').find('.ui.text.loader').remove();
        
            result = JSON.parse(result);
            var table = $('#data-table');
            
            var html = '';
            for (var i = 0; i < result.length; i++) {
                var containerClass = 'variable-table-cell';
                if (annotatedLocations.contains(result[i].loc)) {
                    containerClass += ' location';
                }
            
                html += '<tr class="' + containerClass + '"><td class="variable-table-cell-first" contenteditable="true">';
                html += result[i].loc;
                html += '</td><td class="variable-table-cell-second" contenteditable="true">'
                html += result[i].value;
                html += '</td></tr>';
            }
            table.html(html);
            
            $('#data-table-container').tablesort();
            
            generateVisualization(document.getElementById('viz-gen-button'));
        }, 
        error: function() {
            $('#data-table').find('.ui.text.loader').remove();
        }
    });
}


function clearTable() {
    $('#data-table').children().remove();
    $('#variable-field').val("");
}

var resizeCallbacks = [];
function generateVisualization(sender, checkForReferenceMap) {
    checkForReferenceMap = (checkForReferenceMap == undefined) ? true : checkForReferenceMap;
    
    // Generate viz
    clearVisualization();
    $(sender).addClass('loading disabled');
    
    var values = [];
    var isAllDataTheSame = true;
    var dataValue = null;
    
    $('#data-table').find('.variable-table-cell').each(function(i, element){
        var datapoint = {
            loc: $(element).find('.variable-table-cell-first').text(),
            value: $(element).find('.variable-table-cell-second').text()
        }
        
        if (dataValue == null) {
            dataValue = datapoint.value;
        } else if (dataValue != datapoint.value) {
            isAllDataTheSame = false;
        }
        
        values.push(datapoint);
    });
    
    // Update the maps style if necessary
    if (isAllDataTheSame && checkForReferenceMap) {
        document.getElementById('map-bkg-toggle').checked = true;
        showMapBackground = true;
        $('#map-style').dropdown('set selected', 'None');
        setVisualizationClass($('#map-style'));
    } else {
        if ($('#map-style').val() == "None") {
            $('#map-style').dropdown('set selected', 'Blues');
            setVisualizationClass($('#map-style'));
        }
    }
    
    var data = {
        variable: $('#variable-field').val(),
        data: values,
        usePMI: usePMI,
        locations: annotatedLocations.list(),
        dates: annotatedDates.list(),
        nouns: annotatedNounPharses.list(),
        article: $('#text-area').text()
    }
    
    asyncReqst({
        url: baseURL + "/convert-data",
        async: true,
        type: 'POST',
        data: JSON.stringify(data),
        dataType: 'text',
        contentType: 'text/plain',
        success: function (result) {
            var visualizationData = JSON.parse(result);
            var annotations = visualizationData.annotations;
            var title = visualizationData["title"];
            if (title == undefined) {
                title = "";
            }

            var debug = visualizationData["debug-info"];
            if (debug == undefined) {
                debug = "";
            }

            // Convert the debug string
            debug = debug.replace(new RegExp("\n", "g"), "<br>");
            debug = debug.replace(new RegExp("\t", "g"), "<span style='display:inline-block; width:2rem;'></span>");
    
            // Load a timeseries graph
            var entities = visualizationData.entities;
            var values = visualizationData.values;
    
            var initialLocations = [];
            for (var i = 0; i < rangeElements.length; i++) {
                var span = $(rangeElements[i]);
                if (span.hasClass('location')) {
                    initialLocations.push(span.text());
                }
            }
    
            var locationsData = {
                data: initialLocations
            }
            
            var binningSlider = document.getElementById('binning-slider');
            binningSlider.noUiSlider.set([0, 1]);
            $('#binning-min').val(visualizationData["y-range"][0]); 
            $('#binning-max').val(visualizationData["y-range"][1]);       
    
    
            asyncReqst({
                url: baseURL + "/find-fips",
                async: true,
                type: 'POST',
                data: JSON.stringify(locationsData),
                dataType: 'text',
                contentType: 'text/plain',
                success: function (result) {
                    $(sender).removeClass('loading disabled');
                    
                    var resolvedFIPSCodes = JSON.parse(result);
                    var identifiedLocations = {};
                    for (var i = 0; i < resolvedFIPSCodes.length; i++) {
                        var fips = resolvedFIPSCodes[i];
                        identifiedLocations[fips] = parseInt(fips);
                    }
                    
                    loadMap(entities, values, title, visualizationData["y-range"], visualizationData["use-state-level"], annotations, identifiedLocations);
                    checkComparisonsCorrectness($('#comparions-check-button'));
                },
                error: function() {
                    $(sender).removeClass('loading disabled');
                }
            });
        }, 
        error: function() {
            $(sender).removeClass('loading disabled');
        }
    });
}

function checkComparisonsCorrectness(sender) {
    $(sender).addClass('loading disabled');
    
    $('#comparisons-table').find('.scroll-table-cell-data').removeClass('comparison-table-visualized');
    
    var container = $('#visualization-container');
    var elements = d3.select(container.get(0)).selectAll(".states, .counties").selectAll('path');
    
    var mapData = {};
    elements.each(function(d) {
        var id = d.id;
        var className = d3.select(this).attr("class");
        className = className.replace('q', '').replace('-9', '');
        
        mapData[id] = className;
    });
    
    var data = getComparisonsInformation();
    data.map = mapData;
    
    asyncReqst({
        url: baseURL + "/passing-comparisons",
        async: true,
        type: 'POST',
        data: JSON.stringify(data),
        dataType: 'text',
        contentType: 'text/plain',
        success: function (result) {
            $(sender).removeClass('loading disabled');
            
            var json = JSON.parse(result);
            var comparisonResults = json.passing;
            $('#comparisons-table').find('.scroll-table-cell-data').each(function(i) {
                if (comparisonResults[i]) {
                    $(this).addClass('comparison-table-visualized');
                }
            });
        },
        error: function() {
            $(sender).removeClass('loading disabled');
        }});
}

function clearVisualization() {
    $('#visualization-container').children().remove();
    $('#comparisons-table').find('.scroll-table-cell-data').removeClass('comparison-table-visualized');
}

var isInVisualizationAreaEditMode = false;
function toggleVisualizationEditMode(sender) {
    if ($('#move-annotation-button').hasClass('active')) {
        toggleAnnotationsMoveable($('#move-annotation-button'));
    }
    if ($('#add-annotation-button').hasClass('active')) {
        toggleAddAnnotations($('#add-annotation-button'));
    }
    if ($('#remove-annotation-button').hasClass('active')) {
        toggleRemoveAnnotations($('#remove-annotation-button'));
    }
    
    var button = $(sender);
    if (button.hasClass('active')) {
        isInVisualizationAreaEditMode = false;
        button.removeClass('active');
    } else {
        isInVisualizationAreaEditMode = true;
        button.addClass('active');
    }
    
    setEnableEntityToggling(isInVisualizationAreaEditMode);
}

function loadVisualizationScale() {
    var binningSlider = document.getElementById('binning-slider');
    var binningValues = binningSlider.noUiSlider.get();
    var minValue = parseFloat($('#binning-min').val()); 
    var maxValue = parseFloat($('#binning-max').val());   
    
    function map(x, min, max, outMin, outMax) {
        return (x - min) * (outMax - outMin) / (max - min) + outMin;
    }
    
    var scale = [map(binningValues[0], 0.0, 1.0, minValue, maxValue), map(binningValues[1], 0.0, 1.0, minValue, maxValue)];
    udpateScale(scale);
    checkComparisonsCorrectness($('#comparions-check-button'));
}

var useQuantizeScale = true;
function setUseQuantizeScale() {
    if (!useQuantizeScale) {
        $('#quantize-button').addClass('active');
        $('#natural-break-button').removeClass('active');
        useQuantizeScale = true;
        
        $('#binning-min').prop('disabled', false).parent().removeClass('disabled');
        $('#binning-max').prop('disabled', false).parent().removeClass('disabled');
        document.getElementById('binning-slider').removeAttribute('disabled', true);
        
        loadVisualizationScale();
    }
}
function setUseNaturalBreakScale() {
    if (useQuantizeScale) {
        $('#quantize-button').removeClass('active');
        $('#natural-break-button').addClass('active');
        useQuantizeScale = false;
        
        $('#binning-min').prop('disabled', true).parent().addClass('disabled');
        $('#binning-max').prop('disabled', true).parent().addClass('disabled');
        document.getElementById('binning-slider').setAttribute('disabled', true);
        
        loadVisualizationScale();
    }
}


var usePMI = false;
function setUsePMI() {
    if (!usePMI) {
        $('#pmi-button').addClass('active');
        $('#sr-button').removeClass('active');
        usePMI = true;
        
        // Make srue we have some data to reload
        if (rangeElements.length > 0) {
            loadVariables(document.getElementById('var-load-button'));
        }
    }
}

function setUseSR() {
    if (usePMI) {
        $('#pmi-button').removeClass('active');
        $('#sr-button').addClass('active');
        usePMI = false;
        
        // Make srue we have some data to reload
        if (rangeElements.length > 0) {
            loadVariables(document.getElementById('var-load-button'));
        }
    }
}

var currentVisualizationClass = 'Blues';
function setVisualizationClass(sender) {
    $('#visualization-container').removeClass(currentVisualizationClass);
    currentVisualizationClass = $(sender).val();
    $('#visualization-container').addClass(currentVisualizationClass);
}

// North vs South: http://violentdeathproject.com/images/United%20States%20Region%20Murders%20Map-small.jpg
// West, midwest, ... southeast: https://howingtongeo.wikispaces.com/file/view/regionsmap.gif/449773036/regionsmap.gif
var regionOptions = 
'<option value="09 10 16 17 18 19 23 25 26 27 30 31 33 34 36 38 39 41 42 44 46 50 53 55 56">North</option>' +
'<option value="01 04 05 05 06 08 12 13 20 21 22 24 28 29 32 35 37 40 45 47 48 49 51 54">South</option>' +
'<option value="06 08 16 30 32 41 49 53 56">West</option>' +
'<option value="17 18 19 20 26 27 29 31 38 39 46 55">Mid-West</option>' +
'<option value="09 10 23 24 25 33 34 36 42 44 50">North East</option>' +
'<option value="04 35 40 48">South West</option>' +
'<option value="01 05 12 13 21 22 28 37 45 47 51 54">South East</option>';

var comparisonHtmlRow = 
'<tr class="scroll-table-cell">' +
    '<td class="scroll-table-cell-data">' +
        '<div style="width: 24%; display: inline-block;">' +
            '<select class="ui dropdown fluid display-selection" style="width:25%;">' +
                '<option value="">Type</option>' +
                '<option value="5">Value</option>' +
                '<option value="0">Clustering</option>' +
                '<option value="1">Comparison</option>' +
                '<option value="2">Outlier</option>' +
                // '<option value="3">Trend over Time</option>' +
                // '<option value="4">Time Comparison</option>' +
            '</select>' +
        '</div>' + 
        
        '<div class="clustering-ui comparison-data" style="margin-left:1%; padding-left:1%;">' + 
            '<select class="ui search fluid dropdown region-location-dropdown" multiple="">' +
                '<option value="">Location</option>' +
            '</select>' +
            '<div style="width:67%; margin-top: 1rem; display:inline-block; display:none;">' + 
                '<select class="ui search fluid dropdown noun-dropdown" multiple="">' +
                    '<option value="">Nouns</option>' +
                '</select>' +
            '</div>' +
            '<div style="width:27%; margin-left: 2%; display:inline-block; display:none;">' +
                '<select class="ui fluid search dropdown vis-dropdown">' +
                    '<option value="">Visibility</option>' +
                    '<option value="0">Shown</option>' +
                    '<option value="1">Interaction</option>' +
                    '<option value="2">Not Shown</option>' +
                '</select>' +
            '</div>' +
            '<div class="ui input fluid" style="margin-top: 1rem; display:none;">' +
                '<input type="text" class="comparison-text" placeholder="Comparison Text">' +
            '</div>' + 
        '</div>' +
        
        '<div class="comparison-ui comparison-data">' + 
            '<div style="width:38%; display:inline-block;">' + 
                '<select class="ui fluid search dropdown region-location-dropdown first-location" multiple="">' +
                    '<option value="">Location</option>' +
                '</select>' +
            '</div>' + 
            '<div style="width:16%; margin-left:2%; margin-right:2%; display:inline-block;">' + 
                '<select class="ui fluid search dropdown comp-dropdown">' +
                    '<option value=""></option>' +
                    '<option value="0">≫</option>' +
                    '<option value="1">></option>' +
                    '<option value="2">=</option>' +
                    '<option value="3">≈</option>' +
                    '<option value="4"><</option>' +
                    '<option value="5">≪</option>' +
                '</select>' +
            '</div>' + 
            '<div style="width:38%; display:inline-block;">' + 
                '<select class="ui fluid search dropdown region-location-dropdown second-location" multiple="">' +
                    '<option value="">Location</option>' +
                '</select>' +
            '</div>' + 
            '<br>' + 
            '<div style="width:67%; margin-left: 3%; margin-top: 1rem; display:inline-block; display:none;">' + 
                '<select class="ui search fluid dropdown noun-dropdown" multiple="">' +
                    '<option value="">Nouns</option>' +
                '</select>' +
            '</div>' +
            '<div style="width:27%; margin-left: 2%; display:inline-block; display:none;">' +
                '<select class="ui fluid search dropdown vis-dropdown">' +
                    '<option value="">Visibility</option>' +
                    '<option value="0">Shown</option>' +
                    '<option value="1">Interaction</option>' +
                    '<option value="2">Not Shown</option>' +
                '</select>' +
            '</div>' +
            '<div class="ui input fluid" style="margin-top: 1rem; display:none;">' +
                '<input type="text" class="comparison-text" placeholder="Comparison Text">' +
            '</div>' + 
        '</div>' +
        
        '<div class="outlier-ui comparison-data" style="margin-left:1%; padding-left:1%;">' + 
            '<select class="ui search fluid dropdown region-location-dropdown" multiple="">' +
                '<option value="">Location</option>' +
            '</select>' +
            '<div style="width:67%; margin-top: 1rem; display:inline-block; display:none;">' + 
                '<select class="ui search fluid dropdown noun-dropdown" multiple="">' +
                    '<option value="">Nouns</option>' +
                '</select>' +
            '</div>' +
            '<div style="width:27%; margin-left: 2%; display:inline-block; display:none;">' +
                '<select class="ui fluid search dropdown vis-dropdown">' +
                    '<option value="">Visibility</option>' +
                    '<option value="0">Shown</option>' +
                    '<option value="1">Interaction</option>' +
                    '<option value="2">Not Shown</option>' +
                '</select>' +
            '</div>' +
            '<div class="ui input fluid" style="margin-top: 1rem; display:none;">' +
                '<input type="text" class="comparison-text" placeholder="Comparison Text">' +
            '</div>' + 
        '</div>' +
        
        '<div class="trend-ui comparison-data">' + 
            '<div style="width:30%; display:inline-block;">' + 
                '<select class="ui fluid search dropdown trend-dropdown">' +
                    '<option value="">Trend</option>' +
                    '<option value="0">Upwards</option>' +
                    '<option value="1">Constant</option>' +
                    '<option value="2">Downward</option>' +
                '</select>' +
            '</div>' + 
            '<div style="width:64%; margin-left:2%; display:inline-block;">' + 
                '<select class="ui search fluid dropdown region-location-dropdown" multiple="">' +
                    '<option value="">Location</option>' +
                '</select>' +
            '</div>' +
            '<br>' + 
            '<div style="width:67%; margin-left: 3%; margin-top: 1rem; display:inline-block; display:none;">' + 
                '<select class="ui search fluid dropdown noun-dropdown" multiple="">' +
                    '<option value="">Nouns</option>' +
                '</select>' +
            '</div>' +
            '<div style="width:27%; margin-left: 2%; display:inline-block; display:none;">' +
                '<select class="ui fluid search dropdown vis-dropdown">' +
                    '<option value="">Visibility</option>' +
                    '<option value="0">Shown</option>' +
                    '<option value="1">Interaction</option>' +
                    '<option value="2">Not Shown</option>' +
                '</select>' +
            '</div>' +
            '<div class="ui input fluid" style="margin-top: 1rem; display:none;">' +
                '<input type="text" class="comparison-text" placeholder="Comparison Text">' +
            '</div>' + 
        '</div>' +
        
        '<div class="time-comparison-ui comparison-data">' + 
            '<div style="width:38%; display:inline-block;">' + 
                '<select class="ui fluid search dropdown date-dropdown first-date">' +
                    '<option value="">Date</option>' +
                '</select>' +
            '</div>' + 
            '<div style="width:16%; margin-left:2%; margin-right:2%; display:inline-block;">' + 
                '<select class="ui fluid search dropdown comp-dropdown">' +
                    '<option value=""></option>' +
                    '<option value="0">≫</option>' +
                    '<option value="1">></option>' +
                    '<option value="2">=</option>' +
                    '<option value="3">≈</option>' +
                    '<option value="4"><</option>' +
                    '<option value="5">≪</option>' +
                '</select>' +
            '</div>' + 
            '<div style="width:38%; display:inline-block;">' + 
                '<select class="ui fluid search dropdown date-dropdown second-date">' +
                    '<option value="">Date</option>' +
                '</select>' +
            '</div>' + 
            '<br>' + 
            '<div style="width:96%; margin-left: 3%; margin-top: 1rem; display:inline-block;">' + 
                '<select class="ui search fluid dropdown region-location-dropdown" multiple="">' +
                    '<option value="">Location</option>' +
                '</select>' +
            '</div>' +
            '<br>' + 
            '<div style="width:67%; margin-left: 3%; margin-top: 1rem; display:inline-block; display:none;">' + 
                '<select class="ui search fluid dropdown noun-dropdown" multiple="">' +
                    '<option value="">Nouns</option>' +
                '</select>' +
            '</div>' +
            '<div style="width:27%; margin-left: 2%; display:inline-block; display:none;">' +
                '<select class="ui fluid search dropdown vis-dropdown">' +
                    '<option value="">Visibility</option>' +
                    '<option value="0">Shown</option>' +
                    '<option value="1">Interaction</option>' +
                    '<option value="2">Not Shown</option>' +
                '</select>' +
            '</div>' +
            '<div class="ui input fluid" style="margin-top: 1rem; display:none;">' +
                '<input type="text" class="comparison-text" placeholder="Comparison Text">' +
            '</div>' + 
        '</div>' +
    
        '<div class="value-ui comparison-data" style="margin-left:1%; padding-left:1%;">' + 
            '<div style="width:67%; display:inline-block;">' + 
                '<select class="ui search fluid dropdown region-location-dropdown" multiple="">' +
                    '<option value="">Location</option>' +
                '</select>' +
            '</div>' +
            '<div style="width:27%; margin-left: 2%; display:inline-block;">' +
                '<div class="ui fluid input value-field">' +
                    '<input type="text" placeholder="Value">' + 
                '</div>' +
            '</div>' +
            '<div style="width:67%; margin-top: 1rem; display:inline-block; display:none;">' + 
                '<select class="ui search fluid dropdown noun-dropdown" multiple="">' +
                    '<option value="">Nouns</option>' +
                '</select>' +
            '</div>' +
            '<div style="width:27%; margin-left: 2%; display:inline-block; display:none;">' +
                '<select class="ui fluid search dropdown vis-dropdown">' +
                    '<option value="">Visibility</option>' +
                    '<option value="0">Shown</option>' +
                    '<option value="1">Interaction</option>' +
                    '<option value="2">Not Shown</option>' +
                '</select>' +
            '</div>' +
            '<div class="ui input fluid" style="margin-top: 1rem; display:none;">' +
                '<input type="text" class="comparison-text" placeholder="Comparison Text">' +
            '</div>' + 
        '</div>' +
    '</td>' +
'</tr>';

function addComparisonRow() {
    var row = $($.parseHTML(comparisonHtmlRow));
    $('#comparisons-table').append(row);
    
    row.find('.ui.dropdown').dropdown();
    row.find('.display-selection').dropdown({
        onChange: function(row){ return function(value, text, $selectedItem) {
            row.find('.clustering-ui').css('display', 'none');
            row.find('.comparison-ui').css('display', 'none');
            row.find('.outlier-ui').css('display', 'none');
            row.find('.trend-ui').css('display', 'none');
            row.find('.time-comparison-ui').css('display', 'none');
            row.find('.value-ui').css('display', 'none');
            
            if (value == 0) {
                row.find('.clustering-ui').css('display', 'inline-block');
            } else if (value == 1) {
                row.find('.comparison-ui').css('display', 'inline-block');
            } else if (value == 2) {
                row.find('.outlier-ui').css('display', 'inline-block');
            } else if (value == 3) {
                row.find('.trend-ui').css('display', 'inline-block');
            } else if (value == 4) {
                row.find('.time-comparison-ui').css('display', 'inline-block');
            } else if (value == 5) {
                row.find('.value-ui').css('display', 'inline-block');
            }
        }}(row)
    });
    
    row.find('.scroll-table-cell-data').click(function(row) {return function(){
        row.toggleClass('comparison-selected');
    };}(row)).children().click(function() {
        return false;
    });;
    
    highlightLocationsInComparisons(row);
    updateHighlighedDatesInComparisons(row);
    updateHighlighedNounsInComparisons(row);
    checkComparisonsCorrectness($('#comparions-check-button'));
}

function highlightLocationsInComparisons(elm) {
    // Perform highlighting
    updateDropdown('region-location-dropdown', annotatedLocations, 'Locations', elm);
}
function updateHighlighedDatesInComparisons(elm) {
    updateDropdown('date-dropdown', annotatedDates, 'Date', elm);
}
function updateHighlighedNounsInComparisons(elm) {
    updateDropdown('noun-dropdown', annotatedNounPharses, 'Nouns', elm);
}

function updateDropdown(cls, annotatedContainer, defaultTitle, elm) {
    var dropdowns;
    if (elm == undefined) {
        dropdowns = $('#comparisons-table').find('.' + cls + ' select');
    } else {
        dropdowns = $(elm).find('.' + cls + ' select');
    }
    
    var allValues = {};
    dropdowns.each(function(elementIndex) {
        var value = $(this).dropdown('get value');
        var remainingValues = [];
        for (var i = 0; i < value.length; i++) {
            var index = value[i];
            if (index == null) {
                continue;
            }
            
            var v = $($(this).dropdown('get item', index)[0]).text();
            if (v === '' || annotatedContainer.contains(v)) {
                remainingValues.push(v);
            }
        }
        allValues[elementIndex] = remainingValues;
    });
    dropdowns.children().not('[value=""]').remove();
    dropdowns.dropdown('clear');
    dropdowns.append(annotatedContainer.list().map(function(str, i) {
        return '<option value="' + i + '">' + str + '</option>';
    }).join(' '));
    
    setTimeout(function() {
        dropdowns.dropdown('refresh');
        dropdowns.dropdown('set text', defaultTitle);
        dropdowns.each(function(i) {
            var values = allValues[i];
            for (var j = 0; j < values.length; j++) {
                $(this).dropdown('set selected', values[j]); 
            } 
        });
    }, 5);
}

function removeSelectedComparisonRow() {
    $('#comparisons-table').find('.comparison-selected').remove();
    checkComparisonsCorrectness($('#comparions-check-button'));
}

function getComparisonsInformation() {
    var comparisons = [];
    $('#comparisons-table').find('.scroll-table-cell-data').map(function(i, elm) {
        var elm = $(elm);

        function comparisonIndex(elm, cls) {
            var i = $(elm).find('.' + cls + ' select').dropdown('get value');

            if (typeof i == 'object') {
                i = i[0];
            }

            if (typeof i == 'string') {
                switch (parseInt(i)) {
                    case 0:
                        return '>>';
                    case 1:
                        return '>';
                    case 2:
                        return '=';
                    case 3:
                        return '~=';
                    case 4:
                        return '<';
                    case 5:
                        return '<<';
                }
            }
            return '';
        }
        function trendIndex(elm, cls) {
            var i = $(elm).find('.' + cls).dropdown('get value');
            if (typeof i == 'string') {
                switch (parseInt(i)) {
                    case 0:
                        return '>';
                    case 1:
                        return '=';
                    case 2:
                        return '<';
                }
            }
            return '';
        }
        function extractValues(elm, cls) {
            var dropdown = $(elm).find('.' + cls);
            var values = dropdown.dropdown('get value');
            var result = [];

            if (values == null | values == undefined) {
                return result;
            }

            if (typeof values === 'object') {
                for (var i = 0; i < values.length; i++) {
                    var text = $(dropdown.dropdown('get item', values[i])).text();
                    result.push(text);
                }
            } else if (typeof values === 'string') {
                result.push(values);
            }

            return result;
        }
        function timeString(elm, cls) {
            var t = $(elm).find('.' + cls);
            var value = t.dropdown('get value');
            if (typeof value === 'string') {
                var elm = t.dropdown('get item', value);
                if (elm) {
                    return elm.text();
                }
            }

            return '';
        }
        function visibility(elm, cls) {
            var i = $(elm).find('.' + cls).dropdown('get value');
            if (typeof i == 'string') {
                switch (parseInt(i)) {
                    case 0:
                        return 'shown';
                    case 1:
                        return 'interaction';
                    case 2:
                        return 'not shown';
                }
            }

            return '';
        }

        switch (parseInt(elm.find('.display-selection').dropdown('get value'))) {
            case 0:
                var cluster = elm.find('.clustering-ui');
                return {
                    type: 'cluster',
                    // String of FIPS IDs seperated by spaces
                    locations: extractValues(cluster, 'region-location-dropdown'),
                    // Array of noun phrases
                    nouns: extractValues(cluster, 'noun-dropdown'),
                    visibility: visibility(cluster, 'vis-dropdown'),
                    text: cluster.find('.comparison-text').val()
                };
            case 1:
                var comparison = elm.find('.comparison-ui');
                return {
                    type: 'comparison',
                    // first fips code
                    loc1: extractValues(comparison, 'first-location'),
                    // second fips code
                    loc2: extractValues(comparison, 'second-location'),
                    comp: comparisonIndex(comparison, 'comp-dropdown'),
                    // Array of noun phrases
                    nouns: extractValues(comparison, 'noun-dropdown'),
                    visibility: visibility(comparison, 'vis-dropdown'),
                    text: comparison.find('.comparison-text').val()
                };
            case 2:
                var outlier = elm.find('.outlier-ui');
                return {
                    type: 'outlier',
                    // String of FIPS IDs seperated by spaces
                    locations: extractValues(outlier, 'region-location-dropdown'),
                    // Array of noun phrases
                    nouns: extractValues(outlier, 'noun-dropdown'),
                    visibility: visibility(outlier, 'vis-dropdown'),
                    text: outlier.find('.comparison-text').val()
                };
            case 3:
                var trend = elm.find('.trend-ui');
                return {
                    type: 'trend',
                    // String of FIPS IDs seperated by spaces
                    locations: extractValues(trend, 'region-location-dropdown'),
                    trend: trendIndex(trend, 'trend-dropdown'),
                    // Array of noun phrases
                    nouns: extractValues(trend, 'noun-dropdown'),
                    visibility: visibility(trend, 'vis-dropdown'),
                    text: trend.find('.comparison-text').val()
                };
            case 4:
                var timeComparison = elm.find('.time-comparison-ui');
                return {
                    type: 'time-comparison',
                    date1: timeString(timeComparison, 'first-date'),
                    date2: timeString(timeComparison, 'second-date'),
                    // String of FIPS IDs seperated by spaces
                    locations: extractValues(timeComparison, 'region-location-dropdown'),
                    comp: comparisonIndex(timeComparison, 'comp-dropdown'),
                    // Array of noun phrases
                    nouns: extractValues(timeComparison, 'noun-dropdown'),
                    visibility: visibility(timeComparison, 'vis-dropdown'),
                    text: timeComparison.find('.comparison-text').val()
                };
            case 5:
                var value = elm.find('.value-ui');
                return {
                    type: 'value',
                    value: value.find('.value-field input').val(),
                    // String of FIPS IDs seperated by spaces
                    locations: extractValues(value, 'region-location-dropdown'),
                    // Array of noun phrases
                    nouns: extractValues(value, 'noun-dropdown'),
                    visibility: visibility(value, 'vis-dropdown'),
                    text: value.find('.comparison-text').val()
                };
        }
    }).each(function(i, dat) {
        comparisons.push(dat);
    });
    
    // Save the infromation
    var information = {
        title: $('#article-title').val(),
        date:  $('#article-date').val(),
        text:  $('#text-area').clone().find('.popup').remove().end().text(),
        url:   $('#article-url').val(),
        /* 
        annotations: rangeElements.map(function(span) {
            var r = span.current_text_range; 
        }), 
        */
        comparisons: comparisons
    }
    
    return information;
}

// var documentCount = window.localStorage['documentCount'];
function saveDocument(sender) {
    // Make sure the doc count is well defined
    /*if (documentCount == undefined) {
        documentCount = 0;
    }
    */
    
    $(sender).addClass('loading disabled');
    
    var data = {
        name: $('#name-field').val(),
        data: JSON.stringify(getComparisonsInformation())
    }
    
    asyncReqst({
        url: baseURL + "/save-document",
        async: true,
        type: 'POST',
        data: JSON.stringify(data),
        dataType: 'text',
        contentType: 'text/plain',
        success: function (result) {
            $(sender).removeClass('loading disabled');
            console.log("Success!");
        }, 
        error: function() {
            $(sender).removeClass('loading disabled');
            alert('Unable to save information');
        }
    });
    
/*
    try {
        window.localStorage[documentCount] = JSON.stringify(information);

        // Increase the doc count
        documentCount++;
        window.localStorage['documentCount'] = documentCount;
    } catch(e) {
        if (e.name === 'QUOTA_EXCEEDED_ERR') {
            alert('Out of storage, consider downloading the data.');
        } else {
            alert('Unknown error ' + e + '\n\n' + documentCount);
        }
    }
    */

}

function clearDocument() {
    clearVisualization();
    clearTable();
    clearVariables();
    $('#comparisons-table').children().remove();
    
    clearAnotations();
    
    $('#article-title').val('');
    $('#article-date').val('');
    $('#text-area').empty();
    $('#article-url').val('');
}

var showMapBackground = false;
function toggleMapBackground(sender) {
    // This is also set true in generateVisualization()
    showMapBackground = sender.checked;
    
    // If there is a visualization, regenerate it
    if ($('#visualization-container').find('svg').length > 0) {
        generateVisualization(document.getElementById('viz-gen-button'), false);
    }
}

var areAnnotationsMoveable = false;
function toggleAnnotationsMoveable(sender) {
    if ($('#viz-edit-mode-button').hasClass('active')) {
        toggleVisualizationEditMode($('#viz-edit-mode-button'));
    }
    if ($('#add-annotation-button').hasClass('active')) {
        toggleAddAnnotations($('#add-annotation-button'));
    }
    if ($('#remove-annotation-button').hasClass('active')) {
        toggleRemoveAnnotations($('#remove-annotation-button'));
    }
    
    var button = $(sender);
    if (button.hasClass('active')) {
        areAnnotationsMoveable = false;
        button.removeClass('active');
    } else {
        areAnnotationsMoveable = true;
        button.addClass('active');
    }
    
    changeAreAnnotationsMoveable();
}

var areAnnotationsAddable = false;
function toggleAddAnnotations(sender) {
    if ($('#viz-edit-mode-button').hasClass('active')) {
        toggleVisualizationEditMode($('#viz-edit-mode-button'));
    }
    if ($('#move-annotation-button').hasClass('active')) {
        toggleAnnotationsMoveable($('#move-annotation-button'));
    }
    if ($('#remove-annotation-button').hasClass('active')) {
        toggleRemoveAnnotations($('#remove-annotation-button'));
    }
    
    var button = $(sender);
    if (button.hasClass('active')) {
        areAnnotationsAddable = false;
        button.removeClass('active');
    } else {
        areAnnotationsAddable = true;
        button.addClass('active');
    }
}

var areAnnotationsRemoveable = false;
function toggleRemoveAnnotations(sender) {
    if ($('#viz-edit-mode-button').hasClass('active')) {
        toggleVisualizationEditMode($('#viz-edit-mode-button'));
    }
    if ($('#move-annotation-button').hasClass('active')) {
        toggleAnnotationsMoveable($('#move-annotation-button'));
    }
    if ($('#add-annotation-button').hasClass('active')) {
        toggleAddAnnotations($('#add-annotation-button'));
    }
    
    var button = $(sender);
    if (button.hasClass('active')) {
        areAnnotationsRemoveable = false;
        button.removeClass('active');
    } else {
        areAnnotationsRemoveable = true;
        button.addClass('active');
    }
}

function annotateLocations(locations) {
    if (projection == undefined) {
        return;
    }
    
    var locationsData = {
        data: locations
    }
    
    asyncReqst({
        url: baseURL + "/find-coords",
        async: true,
        type: 'POST',
        data: JSON.stringify(locationsData),
        dataType: 'text',
        contentType: 'text/plain',
        success: function (result) {
            result = JSON.parse(result);
        
            for (var i = 0; i < result.length; i++) {
                var coords = [result[i][1], result[i][0]];
                var position = projection(coords);
                
                var translation = mapTranslation();
                var scale = mapScale();
                
                var annotation = {
                    location: [position[0]*scale + translation[0], position[1]*scale + translation[1]],
                    text: "",
                    title: locations[i],
                    url: ""
                };
                
                if (annotation.location == null || !isFinite(annotation.location[0]) || !isFinite(annotation.location[1])) {
                    continue;
                }
                
                addAnnotation(annotation);
            }
        }
    });
}
