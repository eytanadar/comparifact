// Converts geo data into Name id map and saves it to the specified file
var result = {};
var stateFips = {};
var countiesFips = {};

 function createMap(features, idMap) {
     if (features.length > 0) {
         var id = features[0].id;
         var name = idMap[id];
         var url = "http://en.wikipedia.org/w/api.php?action=query&prop=info&format=json&indexpageids=&titles=" + name;

         $.ajax({
             url: url,
             async: true,
             dataType: 'json',
             dat : {},
             success: function (wikidata) {
                 if (name != "") {
                    var query = wikidata.query;
                    var id = query.pageids[0];
                    result[name] = "" + id;
                 }
                 createMap(features.slice(1), idMap);
             }
         });
     } else {
         var string = JSON.stringify(result);
        var div = document.write(string);
     }
 }

 $(document).ready(function() {
    $.ajax({
        type: "GET",
        url: "../../newsviews/counties.csv",
        dataType: "text",
        async: false,
        success: function(data) {processData(data);}
     });
      
    createMap(us.objects.counties.geometries, countiesFips);
});

function processData(allText) {
    var allTextLines = allText.split(/\r\n|\n/);

    for (var i=1; i<allTextLines.length; i++) {
        var data = allTextLines[i].split(',');
        stateFips[data[1]] = data[0];
        
        var countyID = parseInt(data[1]) + String("000" + data[2]).slice(-3);
        countiesFips[countyID] = data[3] + ", " + data[0];
    }
}