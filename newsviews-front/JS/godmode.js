var showingAdvancedOptions = false;

function toggleGodModeOptions() {
    showingAdvancedOptions = !showingAdvancedOptions;
    
    var toggleLink = document.getElementById('godModeToggle');
    if (showingAdvancedOptions) {
        toggleLink.innerHTML = "Hide";
    } else {
        toggleLink.innerHTML = "Show";
    }
    
    var options = document.getElementById('godModeOptions');
    if (showingAdvancedOptions) {
        options.className = 'godModeOptionsShow';
    } else {
        options.className = '';
    }
}

function optionsJSON() {
    function isChecked(id) {
        return document.getElementById(id).checked
    }
    
    function floatValue(id) {
        return parseFloat(document.getElementById(id).value);
    }
    
    function intValue(id) {
        return parseInt(document.getElementById(id).value);
    }
    
    function selectedIndex(id) {
        return document.getElementById(id).selectedIndex;
    }
    
    function wikifierLanguage(x) {
        switch (x) {
            case 1:
                return "en";
            default:
                return "simple";
        };
    }
    
    function weightingFunction(x) {
        switch (x) {
            case 1:
                return 'exp';
            default:
                return 'lin';
        }
    }
    
    function linkNormalization(x) {
        switch (x) {
            default:
                return 'none';
            case 1:
                return 'doc-size';
            case 2:
                return 'dynamic-range';
            case 3:
                return 'range';
        }
    }
    
    function termNormalization(x) {
        switch (x) {
            default:
                return 'none';
            case 1:
                return 'dynamic-range';
            case 2:
                return 'range';
        }
    }
    
    function locationExtraction(x) {
        switch (x) {
            default:
                return 'none';
            case 1:
                return 'wikidata';
            case 2:
                return 'stanford';
        }
    }
    
    function numberExtraction(x) {
        switch (x) {
            default:
                return 'none';
            case 1:
                return 'stanford';
        }
    }
    
    function dateExtraction(x) {
        switch (x) {
            default:
                return 'none';
            case 1:
                return 'stanford';
        }
    }
    
    var options = {
        'wikifier-enable' : isChecked('useWikifier'),
        'wikifier-lang'   : wikifierLanguage(selectedIndex('wikifierLang')),
        'wikifier-web'    : isChecked('useWikibrainWebApi'),
        'wikifier-weight' : floatValue('wikifierWeight'),
        
        'nlp-enable' : isChecked('useNLP'),
        'nlp-weight' : floatValue('nlpWeight'),
        
        'weight-func' : weightingFunction(selectedIndex('weightingFunction')),
        'weight-exp'  : floatValue('weightingExponent'),
        
        'term-freq-min' : floatValue('minTermFreq'),
        'term-freq-max' : floatValue('maxTermFreq'),
        
        'score-use-link'   : isChecked('useLinkScore'),
        'score-link-norm'  : linkNormalization(selectedIndex('linkNormFunc')),
        'score-link-min'   : floatValue('linkMinValue'),
        'score-link-max'   : floatValue('linkMaxValue'),
        'score-link-scale' : floatValue('linkScale'),
        
        'score-use-term'   : isChecked('useTermFreq'),
        'score-term-norm'  : termNormalization(selectedIndex('termFreqNormFunc')),
        'score-term-min'   : floatValue('termFreqMinValue'),
        'score-term-max'   : floatValue('termFreqMaxValue'),
        'score-term-scale' : floatValue('termFreqScale'),
        
        'extractor-loc' : locationExtraction(selectedIndex('locationExtractor')),
        'extractor-num' : numberExtraction(selectedIndex('locationExtractor')),
        'extractor-date': dateExtraction(selectedIndex('locationExtractor')),
        
        'term-pmi-enable' : isChecked('usePMI'),
        'term-pmi-scale'  : floatValue('pmiScale'),
        'term-sr-enable'  : isChecked('useSR'),
        'term-sr-scale'   : floatValue('srScale'),
        
        'census-enable'   : isChecked('useCensus'),
        'census-count'    : intValue('numberOfCensus'),
        'census-min-freq' : floatValue('censusMinTermFreq'),
        'census-max-freq' : floatValue('censusMaxTermFreq'),
        'stock-enable'    : isChecked('useStock'),
        'atlasify-enable' : isChecked('useAtlastify'),
        'atlasify-count'  : intValue('numberOfAtlasify'),
        
        'reutersAn-enable' : isChecked('useReutersAnnotations'),
        'reutersAn-count'  : intValue('numberOfReutersAnnotations'),
        'nytimesAn-enable' : isChecked('useNYTimesAnnotations'),
        'nytimesAn-count'  : intValue('numberOfNyTimesAnnotations'),
        'mentionAn-enable' : isChecked('useMentionedLocationsAnnotations'),
        'minMaxAn-enable'  : isChecked('useMinMaxAnnotations'),
        
        'viz-rank-min-score' : floatValue('minVizScore'),
        
        'debug-enable' : isChecked('useDebug')
    };
    return options;
}
