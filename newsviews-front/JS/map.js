
function loadMap(keys, values, center, zoom, title, yRange, isStateLevel, annotations, debug) {
    var container = $(createVisualizationDiv());
    
    // Load the debug information
    container.parent().parent().find('.debugInfo').append('<p>' + debug + '</p>');
    
    container.append("<div class='map-container'><div class='fill'></div></div>");
    container = container.find('.fill');

    var resize = function () {
        container.empty();

        var margins = {
            left: 30,
            right: 30,
            top: 30,
            bottom: 30
        };
        if (container.width() < 550) {
            margins.left = 10;
            margins.right = 10;
        }

        var width = container.width() - margins.left - margins.right;
        var height = container.height() - margins.top - margins.bottom;

        var dataMap = d3.map();
        for (var i = 0; i < keys.length; i++) {
            dataMap.set(keys[i], values[i]);
        }

        var quantile = d3.scale.quantile()
            .range(d3.range(9).map(function (i) {
                return "q" + i + "-9";
            }))
            .domain(yRange);


        var projection = d3.geo.albersUsa()
            .scale(width)
            .translate([width / 2 + margins.left, height / 2 + margins.top]);

        var path = d3.geo.path()
            .projection(projection);

        var graph = d3.select(container.get(0)).append("svg:svg")
            .attr("width", width + margins.left + margins.right)
            .attr("height", height + margins.top + margins.bottom)
            .append("svg:g")
            .attr("transform", "translate(" + margins.left + "," + margins.top + ")");
        
        
        var features = {};
        var className = "";
        
        if (isStateLevel) {
            features = topojson.feature(us, us.objects.states).features;
            className = "states";
        } else {
            features = topojson.feature(us, us.objects.counties).features;
            className = "counties";
        }
        
        graph.append('g')
            .attr("class", className)
            .selectAll("path")
            .data(features)
            .enter().append("path")
            .attr("class", function (d) {
                var value = dataMap.get(d.id);
                if (value == undefined) {
                    value = yRange[0];
                }
                return quantile(value);
            })
            .attr("d", path);
        
        if (!isStateLevel) {
            // Add outline around the states
            graph.append('g')
                .attr("class", "stateOutline")
                .append("path")
                .datum(topojson.mesh(us, us.objects.states, function (a, b) {
                    return a !== b;
                }))
                .attr("class", "statesOutlines")
                .attr("d", path);
        }
        
        var locationsById = {};
        for (var i = 0; i < features.length; i++) {
            var feature = features[i];
            locationsById[feature.id] = feature;
        }
        
        // Extract all the place that have annotations
        var annotatedPlaces = [];
        for (var i = 0; i < annotations.length; i++) {
            var annotation = annotations[i];
            
            // Get the location
            var location = [0, 0];
            var coordinates = JSON.parse(annotation.location);
            
            if (coordinates.hasOwnProperty('lng')) {
                location = projection([coordinates.lng, coordinates.lat]);
            } else {
                var feature = locationsById[annotation.location];
                if (feature == undefined) {
                    continue;
                }
                location = path.centroid(feature);
            }
            
            annotatedPlaces.push({
                title: annotation.title,
                text: annotation.text,
                url: annotation.url,
                location: location
            });
        }
        
        displayAnnotations(graph, {width:width, height:height}, annotatedPlaces);
        
        graph.append("text")
            .attr("x", (width / 2))
            .attr("y", (margins.top / 2))
            .attr("class", "title")
            .attr("text-anchor", "middle")
            .text(title);

    };
    resizeCallbacks.push(resize);
    resize();
}
