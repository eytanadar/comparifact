function loadD3LineGraph(keys, values, xRange, yRange, title, annotations, debug) {
    var container = $(createVisualizationDiv());
    
    // Load the debug information
    container.parent().parent().find('.debugInfo').append('<p>' + debug + '</p>');
    
    container.append("<div class='graph-container'><div class='fill'></div></div>");
    container = container.find('.fill');

    var resize = function () {
        container.empty();

        var margins = {
            left: 30,
            right: 30,
            top: 30,
            bottom: 30
        };
        if (container.width() < 550) {
            margins.left = 10;
            margins.right = 10;
        }

        var width = container.width() - margins.left - margins.right;
        var height = container.height() - margins.top - margins.bottom;

        var parseDate = d3.time.format("%Y-%m-%d");
        var dataMap = {};
        var data = keys.map(function (d, i) {
            dataMap[d] = values[i];
            return [parseDate.parse(d), values[i]];
        });
        data.sort(function (a, b) {
            return a[0] - b[0];
        });

        var dataPerPixel = data.length / width;
        var filteredData = data.filter(
            function (d, i) {
                return i % Math.ceil(dataPerPixel) == 0;
            }
        );

        var minDate = parseDate.parse(xRange[0]),
            maxDate = parseDate.parse(xRange[1]);
        var x = d3.time.scale().domain([minDate, maxDate]).range([0, width]);
        var y = d3.scale.linear().domain(yRange).range([height, 0]);
        
        // Extract all the place that have annotations
        var annotatedElement = {};
        for (var i = 0; i < annotations.length; i++) {
            var annotation = annotations[i];
            annotatedElement[annotation.location] = annotation;
            
            annotation.centroid = [];
            annotation.centroid[0] = x(parseDate.parse(annotation.location));
            annotation.centroid[1] = y(dataMap[annotation.location]);
        }
        
        var line = d3.svg.line()
            .x(function (d) { 
                return x(d[0]);
            })
            .y(function (d) { 
                return y(d[1]);
            });

        var graph = d3.select(container.get(0)).append("svg:svg")
            .attr("width", width + margins.left + margins.right)
            .attr("height", height + margins.top + margins.bottom)
            .append("svg:g")
            .attr("transform", "translate(" + margins.left + "," + margins.top + ")");

        var xAxis = d3.svg.axis()
            .scale(x).tickSubdivide(true)
            .ticks(Math.max(width / 75, 2));
        // Add the x-axis.
        graph.append("svg:g")
            .attr("class", "x axis")
            .attr("transform", "translate(0," + (height + 6.0) + ")")
            .call(xAxis);
        // create left yAxis
        var yAxisLeft = d3.svg.axis()
            .scale(y)
            .ticks(Math.max(height / 50, 2))
            .orient("left");
        // Add the y-axis to the left
        graph.append("svg:g")
            .attr("class", "y axis")
            .attr("transform", "translate(-25,0)")
            .call(yAxisLeft);

        // Add the line by appending an svg:path element with the data line we created above
        // do this AFTER the axes above so that the line is above the tick-lines
        graph.append("svg:path")
            .attr("d", line(filteredData))
            .attr("stroke", "steelblue")
            .attr("stroke-width", 2)
            .attr("fill", "none");

        graph.append("text")
            .attr("x", (width / 2))
            .attr("y", 0 - (margins.top / 2))
            .attr("class", "title")
            .attr("text-anchor", "middle")
            .text(title);
        
        var annotatedPoints = [];
        for (var i = 0; i < annotations.length; i++) {
            var annotation = annotations[i];
            
            // Get the location 
            var location = [];
            location[0] = x(parseDate.parse(annotation.location));
            location[1] = y(dataMap[annotation.location]);
            
            annotatedPoints.push({
                title: annotation.title,
                text: annotation.text,
                url: annotation.url,
                location: location
            });
        }
        
        displayAnnotations(graph, {width:width, height:height}, annotatedPoints);
    };
    resizeCallbacks.push(resize);
    resize();
}
