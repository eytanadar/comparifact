
var resizeCallbacks = [];

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? null : decodeURIComponent(results[1].replace(/\+/g, " "));
}

window.onload = function () {
    var url = window.location.href;
    var searchURL = getParameterByName('plugin');
    $('#articleDate').val((new Date()).toLocaleDateString());

    if (searchURL) {
        $('#header').css('display', 'none');
        
        var siteURL = document.location.href.split('?')[0];
        
        $.ajax({
            url: baseURL + "/extract/" + encodeURIComponent(searchURL),
            async: true,
            type: 'GET',
            dataType: 'text',
            contentType: 'text/plain',
            success: function (result) {
                var json = JSON.parse(result);
                
                var articleDate = new Date(json.date);
                var rootDiv = $('<div class="section"><div class="container"><div class="row"><div class="column">' + 
                                '<h4>' + json.title + '</h4>' + 
                                '<p>' + articleDate.toLocaleDateString() + '</p>' + 
                                '<p>' + json.content + '</p><br><br>' +
                                '<small><em>The above article content was extracted automatically. If it appears incorrect, try using <a target=\"_blank\" href=\"' + siteURL + '\">NewsViews</a> directly.</em></small>' +
                                '</div></div></div></div>');
                $('body').append(rootDiv);
                $(rootDiv).insertAfter($('#header'));
                
            
                $('#articleDate').val(articleDate.toLocaleDateString());
                $('#articleURL').val(decodeURIComponent(searchURL));
                $('#articleText').val('');
                submitArticle();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                loader.style.display = 'none';

                var errorMessage = $(createVisualizationDiv());
                errorMessage.append('<div class="errorMessage">' + "<b>Unable to automatically extract article text. Make sure you are connected to the internet, and if the issue persists try using <a target=\"_blank\" href=\"" + siteURL + "\">NewsViews</a> directly<br><br><br>" + xhr.statusText + "\n" + xhr.responseText + "\n" + xhr.status + "\n" + thrownError + '</div>');

            }
        });
    }
}

$(window).resize(function () {
    for (var i = 0; i < resizeCallbacks.length; i++) {
        resizeCallbacks[i]();
    }
});

function clearNewsViews() {
    $('#articleURL').val('');
    $('#articleText').val('');
    $('#articleDate').val((new Date()).toLocaleDateString());

    removeVisualizations();
}

function removeVisualizations() {
    $('body').find('.visualization').remove();
    resizeCallbacks = [];
}

function createVisualizationDiv() {
    var rootDiv = $('<div class="section visualization"><div class="container"><div class="row"><div class="column"></div></div><div class="row debugInfo"></div></div></div>');
    $('body').append(rootDiv);
    $(rootDiv).insertBefore($('#loader'));

    var div = rootDiv.find(".column").first();
    return div;
}

function createVisualizationFromJSON(json) {
    var visualizationData = json;
    var annotations = visualizationData.annotations;
    var title = visualizationData["title"];
    if (title == undefined) {
        title = "";
    }
    
    var debug = visualizationData["debug-info"];
    if (debug == undefined) {
        debug = "";
    }
    
    // Convert the debug string
    debug = debug.replace(new RegExp("\n", "g"), "<br>");
    debug = debug.replace(new RegExp("\t", "g"), "<span style='display:inline-block; width:2rem;'></span>");
    
    if (visualizationData.type == "map") {
        // Load a timeseries graph
        var entities = visualizationData.entities;
        var values = visualizationData.values;
        loadMap(entities, values, visualizationData["geo-center"], visualizationData["geo-zoom"], title, visualizationData["y-range"], visualizationData["use-state-level"], annotations, debug);
    } else if (visualizationData.type == "time") {
        // Load a mpa of the speicifed data
        var entities = visualizationData.entities;
        var values = visualizationData.values;
        loadD3LineGraph(entities, values, visualizationData["x-range"], visualizationData["y-range"], title, annotations, debug);
    } else {
        console.warn("Unsupported visualization type " + visualizationData.type);
    }
}

function submitArticle() {
    var url = $('#articleURL').val();
    var text = $('#articleText').val();
    var date = $('#articleDate').val();
    var data = {
        "article": text,
        "url": url,
        "date": date,
        "options": optionsJSON()
    };

    var loader = document.getElementById('loader');
    loader.style.display = 'inherit';

    removeVisualizations();

    $.ajax({
        url: baseURL + "/visualize",
        async: true,
        type: 'POST',
        data: JSON.stringify(data),
        dataType: 'text',
        contentType: 'text/plain',
        success: function (result) {
            loader.style.display = 'none';

            var json = JSON.parse(result);
            // Try to load visualization data
            var keywords = $(createVisualizationDiv());
            var content = "<h4>Noun Phrases</h4><br><div class='word-columns'>";
            var nounPhrases = json['noun-phrases'];
            for (var i = 0; i < nounPhrases.length; i++) {
                content = content + nounPhrases[i] + '<br>';
            }
            keywords.append(content + '</div>');

            var visualizations = json.visualizations;
            for (var i = 0; i < visualizations.length; i++) {
                createVisualizationFromJSON(visualizations[i]);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            loader.style.display = 'none';

            var errorMessage = $(createVisualizationDiv());
            errorMessage.append('<div class="errorMessage">' + "<b>Unable to generated specified visualization</b><br>Make sure you are connect to the internet and try again<br><br><br>" + xhr.statusText + "\n" + xhr.responseText + "\n" + xhr.status + "\n" + thrownError + '</div>');

        }
    });
}
