function loadImages() {
    $('ui segment').remove();
    $('#submitButton').addClass('loading');
    
    $('.ui.segment.attached').remove();
    
    asyncReqst({
        url: baseURL + "/extract",
        async: true,
        type: 'POST',
        data: JSON.stringify({url :  $('#url-field').val()}),
        dataType: 'text',
        contentType: 'text/plain',
        success: function (result) {
            result = JSON.parse(result);
            var content = result.content;
            var sentences = content.replace(/([.?!])\s*(?=[A-Z])/g, "$1|").split("|");
            content = sentences.slice(0, 3).join(' ');
            
            var lang = useLocalHost ? 'simple' : 'en';
            var url = "images?lang=" + lang + "&method=" + $('.ui.dropdown').dropdown('get value') + "&text=" + encodeURI(content);
            asyncReqst({
                url: baseURL + "/wikibrain",
                async: true,
                type: 'POST',
                data: JSON.stringify({query : url}),
                dataType: 'text',
                contentType: 'text/plain',
                success: function(result) {
                    $('#submitButton').removeClass('loading');
                    result = JSON.parse(result).articles;
                    
                    for (var i = 0; i < result.length; i++) {
                        var article = result[i];
                        var html = '<div class="ui top attached segment" style="background: rgb(249, 250, 251); padding: 0px;"><div class="ui" style="padding: .92857143em .71428571em; font-weight: 700; font-size: 1em; display: inline-block;">';
                        html += article.title;
                        html += '</div></div>';
                        
                        for (var j = 0; j < article.images.length; j++) {
                            if (j == article.images.length - 1) {
                                html += '<div class="ui bottom attached segment">';
                            } else {
                                html += '<div class="ui attached segment">';
                            }
                            
                            html += '<img style="max-width: 75%; max-height: 75%;" src="data:image/png;base64, ' + article.images[j].data + '">';
                            html += '<p>' + article.images[j].caption + '</p>';
                            
                            html += '</div>';
                        }
                        
                        $('#content').append(html);
                    }
                },
                error: function() {
                    $('#submitButton').removeClass('loading');
                }
            });
        },
        error: function() {
            $('#submitButton').removeClass('loading');
        }
    });
}