// ==UserScript==
// @name CompariFact
// @include http://*
// @include https://*
// ==/UserScript==

kango.addMessageListener('CompariFactOpen', function(event) {
    var scrollTop = $(window).scrollTop();
    
    $('body').wrapInner("<div id='comparifact-body-conatiner' class='split'></div>");
    $('body').append("<div id='comparifact-image-conatiner' class='split'></div>");
    
    $('#comparifact-body-conatiner').css({
        'width' : '100%',
        'height' : '100%',
        'overflow-x' : 'auto',
        'overflow-y' : 'auto',
        'position' : 'fixed',
        'top' : '0',
        'left' : '0',
        '-webkit-transition' : 'width 0.3s ease-in-out',
        '-moz-transition' : 'width 0.3s ease-in-out',
        '-o-transition' : 'width 0.3s ease-in-out',
        'transition' : 'width 0.3s ease-in-out'
    });
    $('#comparifact-body-conatiner').scrollTop(scrollTop);
    
    
    $('#comparifact-image-conatiner').css({
        'width' : '25%',
        'height' : '100%',
        'overflow-x' : 'auto',
        'overflow-y' : 'auto',
        'position' : 'fixed',
        'top' : '0',
        'right' : '-25%',
        '-webkit-transition' : 'right 0.3s ease-in-out',
        '-moz-transition' : 'right 0.3s ease-in-out',
        '-o-transition' : 'right 0.3s ease-in-out',
        'transition' : 'right 0.3s ease-in-out',
        'background' : 'blue'
    });
    
    // Animate the sidebar in
    setTimeout(function() {
        $('#comparifact-body-conatiner').css({'width': '75%'});
        $('#comparifact-image-conatiner').css({'right': '0%'});
    }, 1);
});

kango.addMessageListener('CompariFactClose', function(event) {
    var scrollTop = $('#comparifact-body-conatiner').scrollTop();
    
    // Animate the sidebar out
    $('#comparifact-body-conatiner').css({'width': '100%'});
    $('#comparifact-image-conatiner').css({'right': '-25%'});
    
    setTimeout(function() {
        $('#comparifact-body-conatiner').children().appendTo('body');
        $('#comparifact-body-conatiner').remove();
        $('#comparifact-image-conatiner').remove();
        
        $(window).scrollTop(scrollTop);
    }, 300);
});
