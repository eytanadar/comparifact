function CompariFactExtension() {
    var self = this;
    kango.ui.browserButton.addEventListener(kango.ui.browserButton.event.COMMAND, function() {
        if (self.isShowing) {
            // Cancel the loading
            self.closeNewsViews();
        } else {
            // Load NewsViews
            self.runNewsViews();
        }
    });
}

CompariFactExtension.prototype = {
    runNewsViews: function() {
        this.isShowing = true;
        kango.browser.tabs.getCurrent(function(tab) {
            tab.dispatchMessage("CompariFactOpen", {});
        });
    },
    closeNewsViews:function() {
        this.isShowing = false;
        kango.browser.tabs.getCurrent(function(tab) {
            tab.dispatchMessage("CompariFactClose", {});
        });
    },
    isShowing: false
};

var extension = new CompariFactExtension();