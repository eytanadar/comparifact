# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html

import uuid
import logging 
import unicodecsv as csv
from mapr import settings
from PIL import Image
from os import path
import os

def randomFileName():
    if not os.path.exists('images/'):
        os.makedirs('images/')
    
    return 'images/' + str(uuid.uuid4()) + '.png';

def writeItemToCSV(item):
    writer = csv.writer(open(settings.csv_file_path, 'a'), lineterminator='\n')
    
    filename = randomFileName()
    while os.path.isfile(filename):
        filename = randomFileName()
    
    # Write image to file item['viz']
    if not item['viz'] is None:
        item['viz'].save(filename)
    
    data = None
    
    if item['viz'] is None:
        data = [item['title'], item['date'], item['url'], item['text'], '']
    else:
        data = [item['title'], item['date'], item['url'], item['text'], filename]
    
    logging.debug(data)
    writer.writerow(data)

class MaprPipeline(object):
    def process_item(self, item, spider):
        writeItemToCSV(item)
        return item
