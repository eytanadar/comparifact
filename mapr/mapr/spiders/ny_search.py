# This package will contain the spiders of your Scrapy project
#
# Please refer to the documentation for information on how to create and manage
# your spiders.

import scrapy
import logging
from selenium import webdriver
from mapr.items import MaprItem
from selenium.webdriver.remote.remote_connection import LOGGER
from PIL import Image
import StringIO
import sys
import time
from mapr import pipelines

LOGGER.setLevel(logging.WARNING)


class NYSearchSpider(scrapy.Spider):
    name = "nysearch"
    allowed_domains = ["nytimes.com"]
    start_urls = [
        # "http://query.nytimes.com/search/sitesearch/#/*/365days/allresults/1/allauthors/newest/U.S./"
        "http://query.nytimes.com/search/sitesearch/#/*/from20150222to20150319/allresults/1/allauthors/newest/U.S./"
        # "http://query.nytimes.com/search/sitesearch/#/*/since1851/allresults/1/allauthors/newest/The%20Upshot/"
    ]

    def parse(self, response):
        currentUrl = response.request.url
        logging.debug('Started scraping: ' + str(currentUrl))

        driver = webdriver.Firefox()

        try:
            driver.get(currentUrl)

            # Get all the article links
            # '.story .element2 h3 a::attr(href)'
            storyElements = []

            # perform some waiting to make sure the stories are fully loaded
            for i in range(0, 10):
                storyElements = driver.find_elements_by_class_name('story')
                if len(storyElements) > 0:
                    break
                # sleep for 20s
                time.sleep(20)

            for story in storyElements:
                link = story.find_element_by_css_selector('.element2 h3 a').get_attribute("href")
                url = response.urljoin(link)
                yield scrapy.Request(url, callback=self.parseArticle, priority=100)

            # Get the next page and stop if the last page didn't find any story elements (we are probably at the end)
            urlComponents = [x for x in currentUrl.split('/') if x.isdigit()]
            if len(urlComponents) > 0 and len(storyElements) > 0:
                currentPageNumber = int(urlComponents[0])
                # nextPage = "http://query.nytimes.com/search/sitesearch/#/*/365days/allresults/" + str(currentPageNumber + 1) + "/allauthors/newest/U.S./"
                nextPage = "http://query.nytimes.com/search/sitesearch/#/*/from20150222to20150319/allresults/" + str(currentPageNumber + 1) + "/allauthors/newest/U.S./"
                # nextPage = "http://query.nytimes.com/search/sitesearch/#/*/since1851/allresults/" + str(currentPageNumber + 1) + "/allauthors/newest/The%20Upshot/"
                yield scrapy.Request(nextPage, callback=self.parse, dont_filter=True)

        except:
            logging.debug("Unexpected error:" + str(sys.exc_info()[0]))
        finally:
            driver.quit()

    def parseArticle(self, response):
        url = response.request.url
        logging.debug('Parsing Article at url ' + str(url))
        driver = webdriver.Firefox()
        driver.get(url)

        try:
            # look to see if d3 was is contained in the page
            if driver.execute_script("return typeof d3 !== 'undefined'"):
                
                logging.debug('Found D3 Visualization')

                # We have D3 running
                item = MaprItem()
                item['url'] = url
                
                # Try to get the title
                try:
                    item['title'] = driver.find_element_by_id('story-heading').text
                except:
                    logging.debug("Unexpected error:" + str(sys.exc_info()[0]))
                    item['title'] = ''
                
                # Try to get the date
                try:
                    item['date'] = driver.find_element_by_css_selector('.simple-byline-date').text
                except:
                    logging.debug("Unexpected error:" + str(sys.exc_info()[0]))
                    item['date'] = ''
                
                # Try to get the text
                try:
                    item['text'] = '\n'.join(map((lambda elm: elm.text), driver.find_elements_by_css_selector('p.story-content')))
                except:
                    logging.debug("Unexpected error:" + str(sys.exc_info()[0]))
                    item['text'] = ''
                
                # Get the visalization
                item['viz'] = self.captureMapElement(driver)
                
                yield item
                
        except:
            logging.debug("Unexpected error:" + str(sys.exc_info()[0]))
        finally:
            driver.quit()

    def captureMapElement(self, driver):
        mapElement = None

        # Look for a -map element
        canidateElements = driver.find_elements_by_css_selector('div[class$="-map"]')
        if len(canidateElements) > 0:
            # Get the -map element parent (the parent usually contains the labels and such)
            mapElement = canidateElements[0].find_element_by_xpath('..')
        else:
            canidateElements = driver.find_elements_by_css_selector('#g-graphic')
            if len(canidateElements) > 0:
                # Get the g-graphic element parent (the parent usually contains the labels and such)
                mapElement = canidateElements[0]

        if mapElement is None:
            return None

        # Get the web page screenshot and crop it to the desired element
        png = driver.get_screenshot_as_png()
        im = Image.open(StringIO.StringIO(png))
        cropBounds = (mapElement.location['x'], mapElement.location['y'], mapElement.location['x'] + mapElement.size['width'], mapElement.location['y'] + mapElement.size['height'])
        elementImage = im.crop(cropBounds)

        return elementImage
