//
//  WindowController.swift
//  DatasetGatherer
//
//  Created by Josh Ford on 1/6/16.
//  Copyright © 2016 Josh Ford. All rights reserved.
//

import Cocoa

class WindowController: NSWindowController, CHCSVParserDelegate {
    
    
    override func windowDidLoad() {
        super.windowDidLoad()
    
        // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
        if let webView = (contentViewController as? ViewController)?.webView {
            bind("backEnabled", toObject: webView, withKeyPath: "canGoBack", options: nil)
            bind("forwardEnabled", toObject: webView, withKeyPath: "canGoForward", options: nil)
        }
        
        window?.titleVisibility = .Hidden
    }
    
    @IBOutlet weak var urlField: NSTextField!
    @IBAction func moveBrowser(sender: NSSegmentedControl) {
        let vc = contentViewController as? ViewController
        if sender.selectedSegment == 0 {
            vc?.webView?.back(sender)
        } else {
            vc?.webView?.forward(sender)
        }
    }
    @IBAction func addItem(sender: AnyObject) {
        let vc = contentViewController as? ViewController
        vc?.addAticle()
    }
    @IBAction func removeItem(sender: AnyObject) {
        let vc = contentViewController as? ViewController
        vc?.removeArticle()
    }
    
    @IBAction func refresh(sender: NSButton) {
        let vc = contentViewController as? ViewController
        vc?.webView?.reload(sender)
    }
    
    @IBAction func importFile(sender: AnyObject) {
        let panel = NSOpenPanel()
        panel.canChooseFiles = true
        panel.canChooseDirectories = false
        panel.allowsMultipleSelection = false
        panel.beginWithCompletionHandler { result in
            if result == NSFileHandlingPanelOKButton {
                let docURL = panel.URLs[0]
                let reader = CHCSVParser(contentsOfCSVURL: docURL)
                reader.delegate = self
                reader.parse()
            }
        }
    }
    
    var lineItems:[String] = []
    
    func parser(parser: CHCSVParser!, didReadField field: String!, atIndex fieldIndex: Int) {
        lineItems.append(field)
    }
    
    func parser(parser: CHCSVParser!, didEndLine recordNumber: UInt) {
        let newArticle = Article()
        newArticle.title = lineItems[0]
        newArticle.publishDate = lineItems[1]
        newArticle.url = lineItems[2]
        newArticle.text = lineItems[3]
        lineItems = []
        
        let vc = contentViewController as? ViewController
        vc?.arrayController.addObject(newArticle)
    }
    
    @objc var count = NSNumber(unsignedInteger: 0)
    @objc var graphCount = NSNumber(unsignedInteger: 0)
    
    @objc var backEnabled: Bool {
        set {
            browserControls?.setEnabled(newValue, forSegment: 0)
        }
        get {
            return browserControls!.isEnabledForSegment(0)
        }
    }
    @objc var forwardEnabled: Bool {
        set {
            browserControls?.setEnabled(newValue, forSegment: 1)
        }
        get {
            return browserControls!.isEnabledForSegment(1)
        }
    }
    
    @IBOutlet var browserControls: NSSegmentedControl?
    @IBOutlet var refreshControl: NSButton?
    @IBOutlet var urlControl: NSTextField?
    
    func setURL(url: String) {
        urlControl?.stringValue = url
    }
}
