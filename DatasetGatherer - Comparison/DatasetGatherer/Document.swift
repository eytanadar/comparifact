//
//  Document.swift
//  DatasetGatherer
//
//  Created by Josh Ford on 1/7/16.
//  Copyright © 2016 Josh Ford. All rights reserved.
//

import Cocoa

class Document: NSDocument {
    @objc var articles: NSMutableArray = []
    
    override func makeWindowControllers() {
        // Returns the Storyboard that contains your Document window.
        let storyboard = NSStoryboard(name: "Main", bundle: nil)
        let windowController = storyboard.instantiateControllerWithIdentifier("Document Window Controller") as! NSWindowController
        self.addWindowController(windowController)
        
        let arrayController = (windowController.contentViewController as! ViewController).arrayController
        arrayController.bind("contentArray", toObject: self, withKeyPath: "articles", options: nil)
        windowController.bind("count", toObject: arrayController, withKeyPath: "arrangedObjects.@count", options: nil)
        windowController.bind("graphCount", toObject: arrayController, withKeyPath: "arrangedObjects.@sum.graphs.@count", options: nil)
    }

    override func windowControllerDidLoadNib(aController: NSWindowController) {
        super.windowControllerDidLoadNib(aController)
        // Add any code here that needs to be executed once the windowController has loaded the document's window.
    }

    override func dataOfType(typeName: String) throws -> NSData {
        // Insert code here to write your document to data of the specified type. If outError != NULL, ensure that you create and set an appropriate error when returning nil.
        // You can also choose to override -fileWrapperOfType:error:, -writeToURL:ofType:error:, or -writeToURL:ofType:forSaveOperation:originalContentsURL:error: instead.
        
        return NSKeyedArchiver.archivedDataWithRootObject(articles)
    }
    
    override func readFromData(data: NSData, ofType typeName: String) throws {
        // Insert code here to read your document from the given data of the specified type. If outError != NULL, ensure that you create and set an appropriate error when returning false.
        // You can also choose to override -readFromFileWrapper:ofType:error: or -readFromURL:ofType:error: instead.
        // If you override either of these, you should also override -isEntireFileLoaded to return false if the contents are lazily loaded.
        articles = NSKeyedUnarchiver.unarchiveObjectWithData(data) as! NSMutableArray
    }

    override class func autosavesInPlace() -> Bool {
        return true
    }

}
