//
//  ArticleViewController.swift
//  DatasetGatherer
//
//  Created by Josh Ford on 1/6/16.
//  Copyright © 2016 Josh Ford. All rights reserved.
//

import Cocoa

class ArticleViewController: NSViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do view setup here.
        setValue(article, forKey: "article")
    }
    
    @objc var articles: NSArray? {
        get {
            if let a = article {
                return [a]
            } else {
                return nil
            }
        }
        set {
            if let a = newValue where a.count > 0 {
                article = a.firstObject! as? Article
            } else {
                article = nil
            }
        }
    }
    @objc var article: Article? = nil {
        willSet {
            if let _ = article {
                arrayController.unbind("contentArray")
                factsController.unbind("contentArray")
            }
        }
        didSet {
            if titleField == nil {
                return
            }
            
            if let a = article {
                titleField.stringValue = a.title
                publishField.stringValue = a.publisher
                dateField.stringValue = a.publishDate
                urlField.stringValue = a.url
                textField.stringValue = a.text
                arrayController.bind("contentArray", toObject: a, withKeyPath: "graphs", options: nil)
                factsController.bind("contentArray", toObject: a, withKeyPath: "facts", options: nil)
                
                (parentViewController as? ViewController)?.setArticleURL(a.url)
            } else {
                titleField.stringValue = ""
                publishField.stringValue = ""
                dateField.stringValue = ""
                urlField.stringValue = ""
                textField.stringValue = ""
            }
        }
    }
    
    @IBOutlet weak var titleField: NSTextField!
    @IBOutlet weak var publishField: NSTextField!
    @IBOutlet weak var dateField: NSTextField!
    @IBOutlet weak var urlField: NSTextField!
    @IBOutlet weak var textField: NSTextField!
    
    @IBOutlet var arrayController: NSArrayController!
    @IBOutlet var factsController: NSArrayController!
}
