//
//  Article.h
//  DatasetGatherer
//
//  Created by Josh Ford on 1/6/16.
//  Copyright © 2016 Josh Ford. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AppKit/AppKit.h>

typedef NS_ENUM(NSUInteger, GraphType) {
    GraphTypeOther = 0,
    GraphTypeSpatial = 1,
    GraphTypeTemporal = 2
};

@interface Graph : NSObject <NSCoding>

@property (nonatomic) NSString *variable;
@property (nonatomic) NSString *source;
@property (nonatomic) NSNumber *type;
@property (nonatomic) NSData *image;

- (instancetype)init;

@end

typedef NS_ENUM(NSUInteger, ExpressionType) {
    ExpressionTypeVisible = 0,
    ExpressionTypeInteraction = 1,
    ExpressionTypeNotShown = 2
};

typedef NS_ENUM(NSUInteger, ValueType) {
    ValueTypeExplicit = 0,
    ValueTypeRelative = 1,
    ValueTypeGreaterEqual = 2,
    ValueTypeGreater = 3,
    ValueTypeEqual = 4,
    ValueTypeLess = 5,
    ValueTypeLessEqual = 6
};

@class Article;

@interface Fact : NSObject <NSCoding>

@property (nonatomic) NSString *feature;
@property (nonatomic) NSString *entity;
@property (nonatomic) NSString *date;
@property (nonatomic) NSNumber *expression;
@property (nonatomic) NSString *value;
@property (nonatomic) NSNumber *valueType;
@property (nonatomic) NSNumber *relativeValue;
@property (nonatomic) NSNumber *isExplicit;
@property (nonatomic) NSString *articleText;

- (instancetype)init;

@end

@interface Article : NSObject <NSCoding>

@property (nonatomic) NSString *title;
@property (nonatomic) NSString *publishDate;
@property (nonatomic) NSString *publisher;
@property (nonatomic) NSString *url;
@property (nonatomic) NSString *text;
@property (nonatomic) NSMutableArray<Graph *> *graphs;
@property (nonatomic) NSMutableArray<Fact *> *facts;

- (instancetype)init;

@end
