package org.newsviews.QueryExtraction;

import org.newsviews.Reuters.ReutersIndexer;
import org.newsviews.Utilities.Logger;
import org.newsviews.Utilities.Wikibrain;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

/**
 * Created by Josh on 12/8/15.
 */
public class TermSimilarity {
    Wikibrain wikibrain;
    boolean usePMI, useSR;
    float scalePMI, scaleSR;

    public TermSimilarity(Wikibrain wikibrain, boolean usePMI, double scalePMI, boolean useSR, double scaleSR) {
        this(wikibrain, usePMI, (float)scalePMI, useSR, (float)scaleSR);
    }
    public TermSimilarity(Wikibrain wikibrain, boolean usePMI, float scalePMI, boolean useSR, float scaleSR) {
        this.wikibrain = wikibrain;
        this.usePMI = usePMI;
        this.scalePMI = scalePMI;
        this.useSR = useSR;
        this.scaleSR = scaleSR;
    }

    public double similarity(String term1, String term2) {
        double similarity = 0.0;

        if (usePMI) {
            // PMI implementation
            List<String> query = new ArrayList<String>();
            query.add(term2);
            query.add(term1);

            double keywordTermFreq = ReutersIndexer.termFrequency(query);
            double keywordFreq = ReutersIndexer.termFrequency(term2);
            double termFreq = ReutersIndexer.termFrequency(term1);

            double pmi = Math.log(keywordTermFreq / (keywordFreq * termFreq));
            similarity += scalePMI * pmi;
        }

        if (useSR) {
            // SR implmenetation
            double sr = wikibrain.similarity(term1, term2);
            similarity += scaleSR * sr;
        }

        return similarity;
    }

    public TermSimilaritySearch createSearch(String variable, List<String> keywords, List<String> subvariables) {
        return new TermSimilaritySearch(variable, keywords, subvariables, this);
    }

    public static final class TermSimilaritySearch implements Callable<TermSimilaritySearch.TermSimilaritySearchResult> {
        private final String variable;
        private final List<String> subvariables;
        private final List<String> keywords;
        private final TermSimilarity termSimilarity;
        private String debugInformation;
        public  TermSimilaritySearch(String variable, List<String> keywords, List<String> subvariables, TermSimilarity termSimilarity) {
            this.variable = variable;
            this.keywords = keywords;
            this.subvariables = subvariables;
            this.termSimilarity = termSimilarity;
            debugInformation = "";
        }

        public class TermSimilaritySearchResult {
            public final String variable;
            public final List<String> subvariables;
            public final List<String> keywords;
            public final Double value;
            public final String debugInformation;
            TermSimilaritySearchResult(String variable, List<String> keywords, List<String> subvariables, Double value, String debugInformation) {
                this.variable = variable;
                this.keywords = keywords;
                this.subvariables = subvariables;
                this.value = value;
                this.debugInformation = debugInformation;
            }
        }

        public TermSimilaritySearchResult call() throws Exception {
            try {
                double totalValue = 0.0;
                double numberOfTerms = 0.0;

                for (String keyword : keywords) {
                    for (String subvariable : subvariables) {
                        double value = termSimilarity.similarity(keyword, subvariable);

                        debugInformation += subvariable + " : " + keyword + "\t(" + value + ")\n";

                        if (Double.isFinite(value)) {
                            totalValue += value;
                            numberOfTerms += 1.0;
                        }
                    }
                }

                if (numberOfTerms > 0.0) {
                    // TODO: Maybe some kind of weighting function?
                    totalValue /= numberOfTerms;
                } else {
                    totalValue = 0.0;
                }

                return new TermSimilaritySearchResult(variable, keywords, subvariables, totalValue, debugInformation);
            } catch (Exception e) {
                Logger.Log(e);
                throw e;
            }
        }
    }
}
