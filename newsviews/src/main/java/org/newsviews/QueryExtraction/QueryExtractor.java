package org.newsviews.QueryExtraction;

import edu.stanford.nlp.ling.*;
import edu.stanford.nlp.pipeline.*;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.TreeCoreAnnotations;
import edu.stanford.nlp.trees.tregex.TregexMatcher;
import edu.stanford.nlp.trees.tregex.TregexPattern;
import edu.stanford.nlp.util.CoreMap;
import org.apache.commons.lang3.Range;
import org.newsviews.DataStructures.Keywords;
import org.newsviews.Reuters.ReutersIndexer;
import org.newsviews.Spatial.Spatial;
import org.newsviews.Utilities.DateFormatter;
import org.newsviews.Utilities.Logger;
import org.newsviews.Utilities.MapUtilites;
import org.newsviews.Utilities.Wikibrain;

import java.util.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by Josh on 10/13/15.
 */

public class QueryExtractor {
    static private AnnotationPipeline pipeline;
    static private Lock pipelineLock = new ReentrantLock();
    static private void loadNLPPipeline() {
        pipelineLock.lock();
        if (pipeline != null) {
            pipelineLock.unlock();
            return;
        }

        try {
            Properties props = new Properties();
            props.put("annotators", "tokenize,ssplit,pos,lemma,parse,ner,dcoref");
            pipeline = new StanfordCoreNLP(props);
        } catch (Exception e) {
            Logger.Log(e);
        }
        pipelineLock.unlock();
    }

    static private double mapValue(double value, double in_min, double in_max, double out_min, double out_max) {
        return (value - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
    }

    public enum WeightingFunction {
        LINEAR, EXPONENTIAL;

        public static WeightingFunction fromString(String s) {
            if (s.equals("exp")) {
                return EXPONENTIAL;
            }

            return LINEAR;
        }
    }

    public enum NormalizationFunction {
        NONE, DOCUMENT_SIZE, DYNAMIC_RANGE, CUSTOM_RANGE;

        public static NormalizationFunction fromString(String s) {
            if (s.equals("doc-size")) {
                return DOCUMENT_SIZE;
            } else if (s.equals("dynamic-range")) {
                return DYNAMIC_RANGE;
            } else if (s.equals("range")) {
                return CUSTOM_RANGE;
            } else {
                return NONE;
            }
        }

        <T> Map<T, Double> applyToMap(Map<T, Double> map,  double documentSize, double minRange, double maxRange) {
            Map<T, Double> result = new HashMap<T, Double>();

            if (map.size() == 0 || this == NONE) {
                return map;
            }

            double min = 0.0, max = 0.0;
            switch (this) {
                case DOCUMENT_SIZE:
                    min = 0.0;
                    max = documentSize;
                    break;
                case DYNAMIC_RANGE:
                    min = Collections.min(map.values());
                    max = Collections.max(map.values());
                    break;
                case CUSTOM_RANGE:
                    min = minRange;
                    max = maxRange;
            }

            for (T title : map.keySet()) {
                double currentValue = map.get(title);
                double mappedValue = mapValue(currentValue, min, max, 0.0, 1.0);
                result.put(title, mappedValue);
            }

            return result;
        }
    }

    // Methods for extracting
    public enum ExtractionTechnique {
        NONE, WIKIDATA, STANFORD;

        public static ExtractionTechnique fromString(String s) {
            if (s.equals("wikidata")) {
                return WIKIDATA;
            } else if (s.equals("stanford")) {
                return STANFORD;
            } else {
                return NONE;
            }
        }
    }

    // All the instance variables
    Wikibrain wikibrain;
    // Indicates which functions should be used for noun extraction
    boolean useWikibrain, useNLP;
    // Noun Phrase weighting function (using its location in the text to determine it's importance)
    WeightingFunction weightingFunction;
    // Releative scaling of Wikibrain, NLP when combining. The exponent is only used when the weighting function is exponential
    float scaleWikibrain, scaleNLP, exponent;
    // Only noun phrases with a certain frequency in the corpus will be used to hopefully remove noun phrases which are
    // too specific or too general to be useful
    float minTermFrequency, maxTermFrequency;
    // When generating a noun phrase weight we can use both the link (Wikibrain/NLP and text location weighting) and
    // the relative term frequency in the corpus
    // The normalization functions determine how (if any) the values from both these metrics are mapped to the range [0-1]
    NormalizationFunction linkNorm, termFreqNorm;
    // Which of the noun phrase weights should we use
    boolean useLink, useTermFreq;
    // min, max are used for Fixed Range weighting functions, scale will scale the result when combining with the term frequency
    float linkMin, linkMax, linkScale;
    // min, max are used for Fixed Range weighting functions, scale will scale the result when combining with the link
    float termFreqMin, termFreqMax, termFreqScale;
    // How should we extract (if any) locations, numbers, and dates
    // Note that Wikidata can only be used for locations, numbers and dates are limited to NLP
    ExtractionTechnique locationTechnique, numberTechnique, dateTechnique;

    // Obnoxious Constructor with Doubles instead of floats
    public QueryExtractor(Wikibrain wikibrain, boolean useWikibrain, double scaleWikibrain, boolean useNLP, double scaleNLP, WeightingFunction weightingFunction,
                   double exponent, double minTermFrequency, double maxTermFrequency, boolean useLink, boolean useTermFreq,  NormalizationFunction linkNorm,
                   NormalizationFunction termFreqNorm, double linkMin, double linkMax, double linkScale, double termFreqMin,
                   double termFreqMax, double termFreqScale, ExtractionTechnique locationTechnique,
                   ExtractionTechnique numberTechnique, ExtractionTechnique dateTechnique) {
        this(wikibrain, useWikibrain, (float)scaleWikibrain, useNLP, (float)scaleNLP, weightingFunction, (float)exponent,
                (float)minTermFrequency, (float)maxTermFrequency, useLink, useTermFreq, linkNorm, termFreqNorm, (float)linkMin, (float)linkMax,
                (float)linkScale, (float)termFreqMin, (float)termFreqMax, (float)termFreqScale, locationTechnique,
                numberTechnique, dateTechnique);
    }

    // Obnoxious Constructor :P
    public QueryExtractor(Wikibrain wikibrain, boolean useWikibrain, float scaleWikibrain, boolean useNLP, float scaleNLP, WeightingFunction weightingFunction,
                   float exponent, float minTermFrequency, float maxTermFrequency, boolean useLink, boolean useTermFreq, NormalizationFunction linkNorm,
                   NormalizationFunction termFreqNorm, float linkMin, float linkMax, float linkScale, float termFreqMin,
                   float termFreqMax, float termFreqScale, ExtractionTechnique locationTechnique,
                   ExtractionTechnique numberTechnique, ExtractionTechnique dateTechnique) {
        this.wikibrain = wikibrain;
        this.useWikibrain = useWikibrain;
        this.scaleWikibrain = scaleWikibrain;
        this.useNLP = useNLP;
        this.scaleNLP = scaleNLP;
        this.weightingFunction = weightingFunction;
        this.exponent = exponent;
        this.minTermFrequency = minTermFrequency;
        this.maxTermFrequency = maxTermFrequency;
        this.useLink = useLink;
        this.useTermFreq = useTermFreq;
        this.linkNorm = linkNorm;
        this.termFreqNorm = termFreqNorm;
        this.linkMin = linkMin;
        this.linkMax = linkMax;
        this.linkScale = linkScale;
        this.termFreqMin = termFreqMin;
        this.termFreqMax = termFreqMax;
        this.termFreqScale = termFreqScale;
        this.locationTechnique = locationTechnique;
        this.numberTechnique = numberTechnique;
        this.dateTechnique = dateTechnique;
    }

    private double weightingFunction(double value) {
        if (weightingFunction == WeightingFunction.LINEAR) {
            return value;
        } else {
            return Math.pow(value, exponent);
        }
    }

    // Represents an annotation in a piece of text
    // It keeps track of the range and some other metadata which is used in extractKeywords()
    public static class AnnotationRange {
        final public Keywords.KeywordType type;
        final public String originalText;
        final public String resolvedText;
        final public Double score;
        final public Range<Integer> range;

        public AnnotationRange(String originalText, String resolvedText, Keywords.KeywordType type, Double score, Range<Integer> range) {
            this.originalText = originalText;
            this.resolvedText = resolvedText;
            this.type = type;
            this.score = score;
            this.range = range;
        }

        public AnnotationRange(String originalText, String resolvedText, Keywords.KeywordType type, Double score, int startIndex, int length) {
            this(originalText, resolvedText, type, score, Range.between(startIndex, startIndex + length - 1));
        }

        @Override
        public boolean equals(Object obj) {
            if (!(obj instanceof AnnotationRange)) {
                return false;
            }

            AnnotationRange annotation = (AnnotationRange)obj;
            return type == annotation.type && originalText == annotation.originalText && resolvedText == annotation.resolvedText
                    && score == annotation.score && range == annotation.range;
        }

        @Override
        public int hashCode() {
            return type.hashCode() ^ originalText.hashCode() ^ resolvedText.hashCode() ^ score.hashCode() ^ range.hashCode();
        }

        public int startLocation() {
            return range.getMinimum();
        }

        public int endLocation() {
            return range.getMaximum();
        }

        public int length() {
            return endLocation() + 1 - startLocation();
        }
    }

    public Keywords extractKeywords(String text, Date articleDate) {
        Keywords keywords = new Keywords();

        List<AnnotationRange> annotations = annotateText(text, articleDate);
        annotations.addAll(extractNumerals(text, articleDate));

        // Combine all of the annotations which appear multiple times by adding their score
        Map<Keywords.Keyword, Double> phrasesScores = new HashMap<Keywords.Keyword, Double>();

        // Combine all the extracted annotations
        for (AnnotationRange annotation : annotations) {
            String key = annotation.resolvedText;
            Double value = annotation.score;
            Keywords.KeywordType type = annotation.type;

            Keywords.Keyword keyword = new Keywords.Keyword(key, type);

            MapUtilites.addOrUpdate(phrasesScores, keyword, value);
        }

        // Sort all and add all of the keywords
        for (Keywords.Keyword phrase : MapUtilites.sortedKeys(phrasesScores, false)) {
            keywords.addKeyword(phrase);
        }

        return keywords;
    }

    // Will search through the text and produce a list of AnnotaitonRanges
    // This will find noun phrases, dates, numbers, etc (depending on the settings of the QueryExtractor
    public List<AnnotationRange> annotateText(String text, Date articleDate) {
        List<AnnotationRange> annotationRanges = new ArrayList<AnnotationRange>(extractNounPhrases(text));
        annotationRanges.addAll(extractNumerals(text, articleDate));
        return annotationRanges;
    }

    private List<AnnotationRange> extractNounPhrases(String text) {
        List<AnnotationRange> result = new ArrayList<AnnotationRange>();

        try {
            List<AnnotationRange> foundPhrases = new ArrayList<AnnotationRange>();
            Set<Wikibrain.QueryLocalLink> links = new HashSet<Wikibrain.QueryLocalLink>(wikibrain.wikify(text));

            if (useWikibrain) {
                for (Wikibrain.QueryLocalLink l : links) {
                    String title = l.title;
                    // Remove wikipedia parenthesis
                    title = title.replace("\\(([^)]+)\\)", "");

                    Double weight = 1.0 - (double) l.location / (double) text.length();
                    weight = scaleWikibrain * weightingFunction(weight);

                    foundPhrases.add(new AnnotationRange(l.text, l.title, extractionTypeForString(l.title), weight, l.location, l.text.length()));
                }
            }

            if (useNLP) {
                loadNLPPipeline();

                // Extract all the noun phrases
                Annotation document = new Annotation(text);
                pipeline.annotate(document);

                for (CoreMap sentence : document.get(CoreAnnotations.SentencesAnnotation.class)) {
                    Tree tree = sentence.get(TreeCoreAnnotations.TreeAnnotation.class);
                    TregexMatcher matcher = TregexPattern.compile("@NP").matcher(tree);
                    while (matcher.find()) {
                        Tree nounPhrase = matcher.getMatch();
                        List<Tree> leaves = nounPhrase.getChildrenAsList();
                        StringBuilder stringBuilder = new StringBuilder();

                        for (Tree tree1 : leaves) {
                            String val = tree1.label().value();
                            Tree nn[] = tree1.children();
                            String ss = Sentence.listToString(nn[0].yield());
                            stringBuilder.append(ss).append(" ");
                        }

                        String word = stringBuilder.toString();
                        if (word.length() > 0) {
                            Label label = nounPhrase.getLeaves().get(0).label();
                            HasOffset ofs = (HasOffset) label;
                            int location = ofs.beginPosition();
                            Double weight = 1.0 - (double) location / (double) text.length();
                            weight = Math.pow(2.0, weight) - 1.0;
                            weight = scaleNLP * weightingFunction(weight);

                            foundPhrases.add(new AnnotationRange(word, word, extractionTypeForString(word), weight, location, word.length()));
                        }
                    }
                }
            }

            Map<AnnotationRange, Double> termFrequencies = new HashMap<AnnotationRange, Double>();
            Map<AnnotationRange, Double> remainingRanges = new HashMap<AnnotationRange, Double>();

            for (AnnotationRange range : foundPhrases) {
                Double termFrequency = ReutersIndexer.termFrequency(range.resolvedText);

                if (termFrequency >= minTermFrequency && termFrequency <= maxTermFrequency) {
                    // Make sure that the page has a reasonably term frequency
                    termFrequencies.put(range, termFrequency);
                    remainingRanges.put(range, range.score);
                }
            }

            int documentSize = text.split("\\s+").length;
            remainingRanges = linkNorm.applyToMap(remainingRanges, documentSize, linkMin, linkMax);
            termFrequencies = termFreqNorm.applyToMap(termFrequencies, 0.0, termFreqMin, termFreqMax);

            // Update the occurrence values
            Map<AnnotationRange, Double> occurrences = new HashMap<AnnotationRange, Double>();
            for (AnnotationRange annotation : remainingRanges.keySet()) {
                double value = linkScale * remainingRanges.get(annotation) + termFreqScale * termFrequencies.get(annotation);
                occurrences.put(annotation, value);
            }

            result = MapUtilites.sortedKeys(occurrences, false);
        } catch (Exception e) {
            Logger.Log(e);
        }

        return result;
    }

    private List<AnnotationRange> extractNumerals(String text, Date date) {
        List<AnnotationRange> result = new ArrayList<AnnotationRange>();

        loadNLPPipeline();
        Annotation document = new Annotation(text);
        document.set(CoreAnnotations.DocDateAnnotation.class, DateFormatter.format(date));
        pipeline.annotate(document);

        for (CoreMap sentence : document.get(CoreAnnotations.SentencesAnnotation.class)) {
            for (CoreLabel token : sentence.get(CoreAnnotations.TokensAnnotation.class)) {
                String entityTag = token.get(CoreAnnotations.NamedEntityTagAnnotation.class);
                String entity = token.get(CoreAnnotations.NormalizedNamedEntityTagAnnotation.class);
                String original = token.get(CoreAnnotations.OriginalTextAnnotation.class);
                Integer startIndex = token.beginPosition();

                if (entity != null) {
                    Keywords.KeywordType type = Keywords.KeywordType.TypeFromString(entityTag);
                    // AnnotationRange(String originalText, String resolvedText, Keywords.KeywordType type, Double score, Range<Integer> range)
                    AnnotationRange annotaiton = new AnnotationRange(original, entity, type, 1.0, startIndex, original.length());

                    // Make sure it wasn't just added, sometimes we can reach the same value multiple times
                    // we traversing the parsed tree
                    if (result.size() > 0 && result.get(result.size() - 1).equals(annotaiton)) {
                        continue;
                    }

                    // Make sure it is a numerial (which would be weird if it wasn't, but this has
                    // popped up a few times
                    if (type != Keywords.KeywordType.NUMBER && type != Keywords.KeywordType.DATE &&
                            type != Keywords.KeywordType.PERCENT && type != Keywords.KeywordType.MONEY) {
                        // This isn't a numeric, so continue
                        continue;
                    }

                    // Make sure that we are suppose to extract it
                    if (type == Keywords.KeywordType.DATE && dateTechnique != ExtractionTechnique.STANFORD) {
                        continue;
                    } else if (type != Keywords.KeywordType.DATE && numberTechnique != ExtractionTechnique.STANFORD) {
                        continue;
                    }

                    result.add(annotaiton);
                }
            }
        }

        return result;
    }

    private Keywords.KeywordType extractionTypeForString(String entity) {
        loadNLPPipeline();

        Annotation document = new Annotation(entity);
        pipeline.annotate(document);



        for (CoreMap sentence : document.get(CoreAnnotations.SentencesAnnotation.class)) {
            for (CoreLabel token : sentence.get(CoreAnnotations.TokensAnnotation.class)) {
                String entityTag = token.get(CoreAnnotations.NamedEntityTagAnnotation.class);
                Keywords.KeywordType type = Keywords.KeywordType.TypeFromString(entityTag);

                // Check for the dates
                if (type == Keywords.KeywordType.LOCATION && locationTechnique != ExtractionTechnique.STANFORD) {
                    continue;
                }

                if (type != Keywords.KeywordType.OTHER) {
                    return type;
                }
            }
        }

        Spatial spatial = new Spatial(wikibrain);
        if (locationTechnique == ExtractionTechnique.WIKIDATA && spatial.getCoordinatesForArticle(entity) != null) {
            // It is a location
            return Keywords.KeywordType.LOCATION;
        }

        return Keywords.KeywordType.OTHER;
    }
}
