package org.newsviews.Spatial;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.json.JSONObject;
import org.newsviews.Utilities.Logger;
import org.newsviews.Utilities.Wikibrain;
import org.wikibrain.conf.ConfigurationException;
import org.wikibrain.conf.Configurator;
import org.wikibrain.core.cmd.Env;
import org.wikibrain.core.cmd.EnvBuilder;
import org.wikibrain.core.dao.LocalLinkDao;
import org.wikibrain.core.dao.LocalPageDao;
import org.wikibrain.core.lang.Language;
import org.wikibrain.core.model.LocalPage;
import org.wikibrain.wikidata.LocalWikidataStatement;
import org.wikibrain.wikidata.WikidataDao;
import org.wikibrain.wikidata.WikidataValue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by Josh on 11/29/15.
 */
public class Spatial {

    public static class Location {
        public double latitude;
        public double longitude;

        Location(double latitude, double longitude) {
            this.latitude = latitude;
            this.longitude = longitude;
        }

        public JSONObject toJSON() {
            JSONObject json = new JSONObject();
            json.put("lat", latitude);
            json.put("lng", longitude);
            return json;
        }

        public List<Double> toList() {
            ArrayList<Double> result = new ArrayList<Double>();
            result.add(latitude);
            result.add(longitude);
            return result;
        }
    }

    Wikibrain wikibrain;
    public Spatial(Wikibrain wikibrain) {
        this.wikibrain = wikibrain;
    }

    public Location getCoordinatesForArticle(String article) {
        // P625
        final int coorindateLocationWikiDataPropertyID = 625;

        try {
            Map<String, List<LocalWikidataStatement>> statements = wikibrain.getWikidataProperties(article);
            if (statements == null) {
                return null;
            }

            for (String statement : statements.keySet()) {
                List<LocalWikidataStatement> wikiStatements = statements.get(statement);

                for (LocalWikidataStatement wikiStatement : wikiStatements) {
                    if (wikiStatement.getStatement().getProperty().getId() == coorindateLocationWikiDataPropertyID) {
                        // Found the coordinate location location
                        WikidataValue value = wikiStatement.getStatement().getValue();
                        if (value.getTypeName().equals("globecoordinate")) {
                            JsonObject jsonObject = (JsonObject)value.getJsonValue();
                            double latitude = jsonObject.getAsJsonPrimitive("latitude").getAsDouble();
                            double longitude = jsonObject.getAsJsonPrimitive("longitude").getAsDouble();
                            return new Location(latitude, longitude);
                        } else  {
                            Logger.Log("Unknown coordinate type " + value.getTypeName());
                        }
                    }
                }
            }
        } catch (Exception e) {
            Logger.Log(e);
        }

        return null;
    }

    public String getFIPSCodeString(String article) {
        // Various Wikidata Properties
        final int localedInAdministrationDistrictTerrestrialEntity = 131;
        final int FIPS6_4Code = 882;
        final int FIPS5_2Code = 883;

        try {
            Map<String, List<LocalWikidataStatement>> statements = wikibrain.getWikidataProperties(article);
            if (statements == null) {
                return null;
            }

            Map<Integer, List<LocalWikidataStatement>> statementsByID = new HashMap<Integer, List<LocalWikidataStatement>>();

            for (String statement : statements.keySet()) {
                List<LocalWikidataStatement> wikiStatements = statements.get(statement);
                for (LocalWikidataStatement wikiStatement : wikiStatements) {
                    int id = wikiStatement.getStatement().getProperty().getId();
                    if (statementsByID.containsKey(id)) {
                        statementsByID.get(id).add(wikiStatement);
                    } else {
                        List<LocalWikidataStatement> list = new ArrayList<LocalWikidataStatement>();
                        list.add(wikiStatement);
                        statementsByID.put(id, list);
                    }
                }
            }

            if (statementsByID.containsKey(FIPS6_4Code)) {
                for (LocalWikidataStatement wikiStatement : statementsByID.get(FIPS6_4Code)) {
                    // FIPS code
                    WikidataValue value = wikiStatement.getStatement().getValue();
                    try {
                        Integer.parseInt(value.getStringValue());
                        return value.getStringValue();
                    } catch (Exception e) {
                        // Not a number
                    }
                }
            } else if (statementsByID.containsKey(FIPS5_2Code)) {
                for (LocalWikidataStatement wikiStatement : statementsByID.get(FIPS5_2Code)) {
                    // FIPS code
                    WikidataValue value = wikiStatement.getStatement().getValue();
                    try {
                        Integer.parseInt(value.getStringValue());
                        return value.getStringValue();
                    } catch (Exception e) {
                        // Not a number
                    }
                }
            } else if (statementsByID.containsKey(localedInAdministrationDistrictTerrestrialEntity)) {
                for (LocalWikidataStatement wikidataStatement : statementsByID.get(localedInAdministrationDistrictTerrestrialEntity)) {
                    String value = getFIPSCodeString(wikidataStatement.getStatement().getValue().getStringValue());
                    if (value != null) {
                        return value;
                    }
                }
            }
        } catch (Exception e) {
            Logger.Log(e);
        }

        return null;
    }
}
