package org.newsviews.Utilities;

import org.json.JSONArray;
import org.json.JSONObject;
import org.newsviews.NewsViews;
import org.wikibrain.conf.Configurator;
import org.wikibrain.core.cmd.Env;
import org.wikibrain.core.cmd.EnvBuilder;
import org.wikibrain.core.dao.LocalLinkDao;
import org.wikibrain.core.dao.LocalPageDao;
import org.wikibrain.core.lang.Language;
import org.wikibrain.core.model.LocalLink;
import org.wikibrain.core.model.LocalPage;
import org.wikibrain.sr.SRMetric;
import org.wikibrain.sr.wikify.Wikifier;
import org.wikibrain.wikidata.LocalWikidataStatement;
import org.wikibrain.wikidata.WikidataDao;

import java.net.HttpURLConnection;
import java.net.URLEncoder;
import java.util.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by Josh on 11/8/15.
 */
public class Wikibrain {
    public final static String WikibrainBaseURL = "http://localhost:9000"; // "http://flagon.cs.umn.edu:8000"; // "http://como.macalester.edu/wikibrain";

    private static WikidataDao wdDao;
    private static LocalPageDao lpDao;
    private static LocalLinkDao llDao;
    private static Wikifier wikifier;
    private static SRMetric srMetric;
    private static final Language wikifierLang = Language.SIMPLE;
    private static Lock wikibrainLock = new ReentrantLock();

    private static void loadWikidataDaoIfNeeded() {
        wikibrainLock.lock();
        if (wdDao != null) {
            wikibrainLock.unlock();
            return;
        }

        Logger.Log("======================");
        Logger.Log("Wikibrain Stack Track:");
        for (StackTraceElement ste : Thread.currentThread().getStackTrace()) {
            Logger.Log(ste.toString());
        }

        try {
            Logger.Log("Loading Wikibrain");
            Env env = new EnvBuilder().build();
            Configurator conf = env.getConfigurator();

            Logger.Log("Loading Local Page");
            lpDao = conf.get(LocalPageDao.class);
            Logger.Log("Loading Local Link");
            llDao = conf.get(LocalLinkDao.class);
            Logger.Log("Loading Wikidata");
            wdDao = conf.get(WikidataDao.class);
            Logger.Log("Loading Wikifier");
            wikifier = env.getConfigurator().get(Wikifier.class, "websail", "language", wikifierLang.getLangCode());
            // wikifier = env.getConfigurator().get(Wikifier.class, "milnewitten", "language", wikifierLang.getLangCode());
            Logger.Log("Loading SR");
            srMetric = env.getConfigurator().get(SRMetric.class, "ensemble", "language", wikifierLang.getLangCode());
            Logger.Log("Loading Complete");
        } catch (Exception e) {
            Logger.Log(e);
        }
        wikibrainLock.unlock();
    }

    static public class QueryLocalLink {
        public String title;
        public String text;
        public int location;
        public String language;
        public int articleId;

        QueryLocalLink(JSONObject json) {
            title = json.getString("title");
            text = json.getString("text");
            location = json.getInt("index");
            language = json.getString("lang");
            articleId = json.getInt("articleId");
        }
        QueryLocalLink() {
            title = "";
            text = "";
            location = -1;
            language = "";
            articleId = -1;
        }
        QueryLocalLink(String title, String text, int location, String language, int articleId) {
            this.title = title;
            this.text = text;
            this.location = location;
            this.language = language;
            this.articleId = articleId;
        }

        @Override
        public boolean equals(Object obj) {
            if (!(obj instanceof QueryLocalLink)) {
                return false;
            }

            QueryLocalLink link = (QueryLocalLink)obj;

            return text == link.text && location == link.location;
        }

        @Override
        public int hashCode() {
            return 31*text.hashCode() + location;
        }
    }

    Language lang;
    boolean useWebApi;

    public Wikibrain(String lang, boolean useWebApi) {
        this(Language.getByLangCode(lang), useWebApi);
    }

    public Wikibrain(Language lang, boolean useWebApi) {
        this.useWebApi = useWebApi;
        if (!useWebApi) {
            this.lang = Language.SIMPLE;
        } else {
            this.lang = lang;
        }
    }

    public double pageRank(String text) {
        try {
            if (useWebApi) {
                // Use the wikibrain web api
                String url = WikibrainBaseURL + "/pageRank?title=" + URLEncoder.encode(text) + "&lang=" + lang.getLangCode();
                String jsonString = Networking.downloadFile(url).content;
                JSONObject json = new JSONObject(jsonString);

                if (json.getBoolean("success") == true) {
                    return json.getDouble("pageRank");
                }
            } else {
                // Use Wikibrain directly
                LocalPage page = lpDao.getByTitle(lang, text);
                return llDao.getPageRank(page.toLocalId());
            }
        } catch (Exception e) {
            Logger.Log(e);
        }

        return 0.0;
    }

    public List<QueryLocalLink> wikify(String text) {
        loadWikidataDaoIfNeeded();

        List<QueryLocalLink> result = new ArrayList<QueryLocalLink>();

        try {
            if (useWebApi) {
                // If the length of the text is longer than 2048 characters, well pretend the network call failed, so that
                // we divide the input in half
                Networking.NetworkingResult networkingResult;
                if (text.length() > 2048) {
                    networkingResult = new Networking.NetworkingResult(HttpURLConnection.HTTP_REQ_TOO_LONG);
                } else {
                    networkingResult = Networking.downloadFile(WikibrainBaseURL + "/wikify?lang=" + lang.getLangCode() + "&text=" + URLEncoder.encode(text));
                }

                String jsonString = networkingResult.content;

                // Divide the result smaller if the request was too large
                if (networkingResult.HttpCode == HttpURLConnection.HTTP_REQ_TOO_LONG) {
                    // Our input was too long, so lets divide it in half
                    List<String> words = Arrays.asList(text.split(" "));

                    int middleIndex = words.size() / 2;
                    String firstString = String.join(" ", words.subList(0, middleIndex));
                    String secondString = String.join(" ", words.subList(middleIndex, words.size()));

                    List<QueryLocalLink> firstList = wikify(firstString);
                    List<QueryLocalLink> secondList = wikify(secondString);

                    for (int i = 0; i < secondList.size(); i++) {
                        QueryLocalLink link = secondList.get(i);
                        link.location += middleIndex;
                        secondList.set(i, link);
                    }

                    firstList.addAll(secondList);

                    return firstList;
                }

                // Process the returned result
                JSONObject jsonObject = new JSONObject(jsonString);
                if (jsonObject.has("success") && jsonObject.getBoolean("success")) {
                    // Successfully called wikibrain wikifier
                    JSONArray array = jsonObject.getJSONArray("references");

                    for (int i = 0; i < array.length(); i++) {
                        result.add(new QueryLocalLink(array.getJSONObject(i)));
                    }
                } else {
                    Logger.Log("Wikiciation Failed - " + jsonObject.getString("message"));
                }
            } else {
                for (LocalLink link : wikifier.wikify(text)) {
                    try {
                        LocalPage localPage = lpDao.getById(link.getLanguage(), link.getDestId());
                        // String title, String text, int location, String language, int articleId
                        QueryLocalLink queryLocalLink = new QueryLocalLink(
                                localPage.getTitle().getCanonicalTitle(),
                                link.getAnchorText(),
                                link.getLocation(),
                                link.getLanguage().getLangCode(),
                                link.getDestId());
                        result.add(queryLocalLink);
                    } catch (Exception e) {
                        Logger.Log(e);
                    }
                }
            }
        } catch (Exception e) {
            Logger.Log(e);
        }

        return result;
    }

    public Map<String, List<LocalWikidataStatement>> getWikidataProperties(String article) {
        loadWikidataDaoIfNeeded();

        // Always default to simple english
        Language lang = Language.SIMPLE;
        try {
            LocalPage page = lpDao.getByTitle(lang, article);
            if (page == null) {
                return null;
            }

            Map<String, List<LocalWikidataStatement>> statements = wdDao.getLocalStatements(page);
            return statements;
        } catch (Exception e) {
            Logger.Log(e);
        }

        return new HashMap<String, List<LocalWikidataStatement>>();
    }

    public double similarity(String entity1, String entity2) {
        try {
            if (useWebApi) {
                String jsonString = Networking.downloadFile(WikibrainBaseURL + "/similarity?lang=" + lang.getLangCode() + "&phrases="
                        + URLEncoder.encode(entity1) + "|" + URLEncoder.encode(entity2)).content;

                JSONObject jsonObject = new JSONObject(jsonString);
                if (jsonObject.has("success") && jsonObject.getBoolean("success")) {
                    // Successfully called wikibrain wikifier
                    Double score = jsonObject.getDouble("score");
                    return score;
                } else {
                    Logger.Log("SR Failed - " + entity1 + " " + entity2 + " : " + jsonObject.getString("message"));
                }
            } else {
                return srMetric.similarity(entity1, entity2, false).getScore();
            }
        } catch (Exception e){
            Logger.Log(e);
        }

        return 0.0;
    }
}
