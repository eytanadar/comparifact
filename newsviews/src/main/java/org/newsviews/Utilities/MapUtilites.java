package org.newsviews.Utilities;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

/**
 * Created by Josh on 2/6/16.
 */
public class MapUtilites {
    static public <T,U extends Comparable<U>> void sortList(List<T> list, Map<T, U> valuesMap, boolean ascending) {
        class ValueComparator implements Comparator<T> {
            public Map<T, U> valuesMap;
            public boolean ascending;
            public int compare(T a, T b) {
                if (ascending) {
                    return valuesMap.get(a).compareTo(valuesMap.get(b));
                } else {
                    return valuesMap.get(b).compareTo(valuesMap.get(a));
                }
            }
        }

        ValueComparator comparator = new ValueComparator();
        comparator.ascending = ascending;
        comparator.valuesMap = valuesMap;

        list.sort(comparator);
    }

    static public <T, U extends Comparable<U>> List<T> createSortedList(List<T> list, Map<T, U> valuesMap, boolean ascending) {
        List<T> result = new ArrayList<T>(list);
        sortList(result, valuesMap, ascending);
        return result;
    }

    static public <T, U extends Comparable<U>> List<T> sortedKeys(Map<T, U> valuesMap, boolean ascending) {
        return createSortedList(new ArrayList<T>(valuesMap.keySet()), valuesMap, ascending);
    }

    static public <T> void addOrUpdate(Map<T, Double> map, T key, Double value) {
        if (map.containsKey(key)) {
            value = value + map.get(key);
        }
        map.put(key, value);
    }
}
