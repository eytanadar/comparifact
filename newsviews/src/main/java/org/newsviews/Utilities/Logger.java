package org.newsviews.Utilities;

/**
 * Created by Josh on 11/8/15.
 */
public class Logger {
    public static void Log(String s) {
        StackTraceElement stackTraceElement = Thread.currentThread().getStackTrace()[2];
        System.out.println("[" + stackTraceElement.getFileName() + ":" + stackTraceElement.getLineNumber() + "] " + s);
    }

    public static void Log(Exception e) {
        StackTraceElement stackTraceElement = Thread.currentThread().getStackTrace()[2];
        System.out.println("[" + stackTraceElement.getFileName() + ":" + stackTraceElement.getLineNumber() + "] " + e);
        // e.printStackTrace();
    }
}
