package org.newsviews.Utilities;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Josh on 10/20/15.
 */
public class Networking {
    static public class NetworkingResult {
        public int HttpCode;
        public String content;

        NetworkingResult() {
            HttpCode = HttpURLConnection.HTTP_INTERNAL_ERROR;
            content = "";
        }

        NetworkingResult(int HttpCode) {
            this.HttpCode = HttpCode;
            this.content = "";
        }

        NetworkingResult(String content, int HttpCode) {
            this.content = content;
            this.HttpCode = HttpCode;
        }

        NetworkingResult(String content) {
            this.content = content;
            this.HttpCode = HttpURLConnection.HTTP_OK;
        }
    }

    static public NetworkingResult downloadFile(String location) {
        return downloadFile(location, 0);
    }

    static public NetworkingResult downloadFile(String location, int timeout) {
        try {
            URL url = new URL(location);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setConnectTimeout(timeout);
            connection.setReadTimeout(timeout);

            connection.setRequestMethod("GET");

            StringBuilder stringBuilder = new StringBuilder();
            int HttpResult = connection.getResponseCode();
            if (HttpResult == HttpURLConnection.HTTP_OK) {
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream(), "utf-8"));
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    stringBuilder.append(line + "\n");
                }
                bufferedReader.close();
                return new NetworkingResult(stringBuilder.toString());
            } else {
                Logger.Log(connection.getResponseMessage());
                return new NetworkingResult(HttpResult);
            }
        } catch (Exception e) {
            Logger.Log(e);
            return new NetworkingResult();
        }
    }

    static public NetworkingResult PostRequest(String locaion, JSONObject data) {
        return PostRequest(locaion, data.toString());
    }

    static public NetworkingResult PostRequest(String locaion, String ... data) {
        try {
            URL url = new URL(locaion);

            HttpURLConnection connection = (HttpURLConnection)url.openConnection();
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setRequestProperty("Accept", "text/plain");
            connection.setRequestMethod("POST");
            if (data.length == 1) {
                connection.setRequestProperty("Content-Type", "application/json");
            } else  {
                connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            }

            OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());

            if (data.length == 1) {
                writer.write(data.toString());
            } else {
                List<NameValuePair> parameters = new ArrayList<NameValuePair>();
                for (int i = 0; i < data.length / 2; i ++) {
                    parameters.add(new BasicNameValuePair(data[2 * i], data[2 * i + 1]));
                }
                writer.write(getQuery(parameters));
            }

            writer.flush();

            StringBuilder stringBuilder = new StringBuilder();
            int HttpResult = connection.getResponseCode();
            if (HttpResult == HttpURLConnection.HTTP_OK) {
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream(), "utf-8"));
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    stringBuilder.append(line + "\n");
                }
                bufferedReader.close();
                return new NetworkingResult(stringBuilder.toString());
            } else {
                Logger.Log("Request: " + connection.getRequestMethod());
                Logger.Log(connection.getResponseMessage());
                Logger.Log(connection.getResponseCode() + "");
                return new NetworkingResult(HttpResult);
            }
        } catch (Exception e) {
            Logger.Log(e);
            e.printStackTrace();
            return new NetworkingResult();
        }
    }

    private static String getQuery(List<NameValuePair> params) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;

        for (NameValuePair pair : params) {
            if (first) {
                first = false;
            } else {
                result.append("&");
            }

            result.append(URLEncoder.encode(pair.getName()));
            result.append("=");
            result.append(URLEncoder.encode(pair.getValue()));
        }

        return result.toString();
    }
}
