package org.newsviews.Annotations;

import org.newsviews.DataStructures.Keywords;
import org.newsviews.DataStructures.VisualizationData;
import org.newsviews.QueryExtraction.QueryExtractor;
import org.newsviews.Reuters.ReutersIndexer;
import org.newsviews.Spatial.Spatial;
import org.newsviews.Utilities.DateFormatter;
import org.newsviews.Utilities.Wikibrain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Josh on 11/30/15.
 */
public class ReutersAnnotator extends ArticleAnnotator {
    public String name() {
        return "Reuters";
    }

    private Wikibrain wikibrain;
    private Spatial spatial;

    public ReutersAnnotator(Wikibrain wikibrain, int maxNumberOfAnnotaitons) {
        super(maxNumberOfAnnotaitons);
        this.wikibrain = wikibrain;
        spatial = new Spatial(wikibrain);
    }

    @Override
    protected List<Article> searchArticles(List<String> searchTerms, VisualizationData visualization, Keywords keywords) {
        List<Article> result = new ArrayList<Article>();
        List<String> reuterSearchTerms = new ArrayList<String>(visualization.annotationKeywords);
        for (int i = 0; i < reuterSearchTerms.size(); i++) {
            reuterSearchTerms.set(i, reuterSearchTerms.get(i).toLowerCase());
        }
        List<ReutersIndexer.ReutersArticle> articles = ReutersIndexer.searchArticles(reuterSearchTerms);

        for (ReutersIndexer.ReutersArticle article : articles) {
            String location = "";

            switch (visualization.type) {
                case Time:
                    Date articleDate = article.publishDate;
                    if (articleDate == null) {
                        articleDate = new Date();
                    }
                    location = DateFormatter.format(articleDate);

                    break;
                case Map:
                    String articleLocation = article.location;
                    if (articleLocation.contains(",")) {
                        articleLocation = articleLocation.substring(0, articleLocation.lastIndexOf(","));
                    }
                    articleLocation = articleLocation.toLowerCase();
                    Spatial.Location coordinates = spatial.getCoordinatesForArticle(articleLocation);

                    if (coordinates != null) {
                        location = coordinates.toJSON().toString();
                    }

                    break;
            }

            result.add(new Article(article.headline, article.body, location, ""));
        }

        return result;
    }
}
