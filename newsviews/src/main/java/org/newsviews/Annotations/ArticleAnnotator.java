package org.newsviews.Annotations;

import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.ling.Sentence;
import edu.stanford.nlp.process.DocumentPreprocessor;
import org.newsviews.DataStructures.Keywords;
import org.newsviews.DataStructures.VisualizationData;
import org.newsviews.QueryExtraction.QueryExtractor;
import org.newsviews.Spatial.Spatial;
import org.newsviews.Utilities.DateFormatter;

import java.io.StringReader;
import java.util.*;

/**
 * Created by Josh on 12/2/15.
 */
public abstract class ArticleAnnotator implements AnnotationGenerator.Annotator {
    private int maxNumberOfAnnotaitons;

    protected class Article {
        // Location is a generic string representing the location of the article
        // i.e. Coordinates, FIPS code or Date
        public String location;
        public String headline;
        public String body;
        public String url;
        public Article() {
            location = headline = body = url = "";
        }

        public Article(String headline, String body, String location, String url) {
            this.headline = headline;
            this.body = body;
            this.location = location;
            this.url = url;
        }
    }

    ArticleAnnotator(int maxNumberOfAnnotaitons) {
        this.maxNumberOfAnnotaitons = maxNumberOfAnnotaitons;
    }

    public List<VisualizationData.Annotation> annotateVisualization(VisualizationData visualization, Keywords keywords) {
        List<VisualizationData.Annotation> result = new ArrayList<VisualizationData.Annotation>();

        List<String> searchTerms = new ArrayList<String>();

        searchTerms.addAll(visualization.annotationKeywords);
        switch (visualization.type) {
            case Time:
                if (keywords.dates().size() > 0) {
                    searchTerms.add(DateFormatter.format(keywords.dates().get(0)));
                }
                break;
            case Map:
                if (keywords.locations().size() > 0) {
                    searchTerms.add(keywords.locations().get(0));
                }

                break;
        }

        if (searchTerms.size() < 2) {
            return result;
        }

        int annotationsAdded = 0;
        List<Article> articles = searchArticles(searchTerms,visualization, keywords);
        for (Article article : articles) {
            if (annotationsAdded >= maxNumberOfAnnotaitons) {
                break;
            }

            if (article.location.length() <= 0) {
                continue;
            }

            String articleContent = article.body;
            if (articleContent.length() <= 0) {
                continue;
            }

            // Extract the smallest subset of contiguous sentences such that all of the search terms are contained
            // First get all of the sentences
            List<String> sentences = new ArrayList<String>();
            DocumentPreprocessor documentPreprocessor = new DocumentPreprocessor(new StringReader(articleContent));
            for (List<HasWord> sentence : documentPreprocessor) {
                sentences.add(Sentence.listToString(sentence));
            }

            int minLength = Integer.MAX_VALUE;
            int lowerIndex = -1;
            int upperIndex = -1;
            int currentLowerIndex = -1;
            for (int i = 0; i < sentences.size(); i++) {
                Set<String> remainingKeyWords = new HashSet<String>(searchTerms);
                if (checkKeywords(remainingKeyWords, sentences.get(i))) {
                    currentLowerIndex = i;
                }

                if (remainingKeyWords.size() == 0) {
                    int length = i - currentLowerIndex + 1;
                    if (length < minLength) {
                        lowerIndex = currentLowerIndex;
                        upperIndex = i;
                        minLength = length;
                    }
                }

                for (int j = i + 1; j < sentences.size(); j++) {
                    checkKeywords(remainingKeyWords, sentences.get(j));

                    if (remainingKeyWords.size() == 0) {
                        int length = i - currentLowerIndex + 1;
                        if (length < minLength) {
                            lowerIndex = currentLowerIndex;
                            upperIndex = i;
                            minLength = length;
                        }

                        break;
                    }
                }
            }

            String content = "";
            if (lowerIndex >= 0 && upperIndex >= 0) {
                for (int i = lowerIndex; i <= upperIndex; i++) {
                    if (content.length() > 0) {
                        content += " ";
                    }

                    content += sentences.get(i);
                }
            } else {
                content = article.body;
            }

            VisualizationData.Annotation annotation = new VisualizationData.Annotation(article.headline, content, article.url, article.location);
            result.add(annotation);
            annotationsAdded++;
        }

        return result;
    }

    private boolean checkKeywords(Set<String> keywords, String searchString) {
        boolean containedTerm = false;
        List<String> termsToRemove = new ArrayList<String>();

        for (String s : keywords) {
            if (searchString.contains(s)) {
                containedTerm = true;
                termsToRemove.add(s);
            }
        }

        for (String term : termsToRemove) {
            keywords.remove(term);
        }

        return containedTerm;
    }

    protected abstract List<Article> searchArticles(List<String> searchTerms, VisualizationData visualization, Keywords keywords);
}
