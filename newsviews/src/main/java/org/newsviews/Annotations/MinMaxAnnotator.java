package org.newsviews.Annotations;

import org.newsviews.DataStructures.FIPS;
import org.newsviews.DataStructures.Keywords;
import org.newsviews.DataStructures.VisualizationData;
import org.newsviews.Utilities.DateFormatter;
import org.newsviews.Utilities.Logger;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Josh on 11/9/15.
 */
public class MinMaxAnnotator implements AnnotationGenerator.Annotator {

    public String name() {
        return "MinMax";
    }

    public List<VisualizationData.Annotation> annotateVisualization(VisualizationData visualization, Keywords keywords) {
        List<VisualizationData.Annotation> result = new ArrayList<VisualizationData.Annotation>();

        String minEntity = "", maxEntity = "";
        Double minValue = Double.POSITIVE_INFINITY, maxValue = Double.NEGATIVE_INFINITY;

        if (visualization.values.size() < 2) {
            return result;
        }

        for (int i = 0; i < visualization.values.size(); i++) {
            Double value = visualization.values.get(i);
            String entity = visualization.entities.get(i);

            if (value < minValue) {
                minEntity = entity;
                minValue = value;
            }

            if (value > maxValue) {
                maxEntity = entity;
                maxValue = value;
            }
        }

        String minEntityName = minEntity;
        String maxEntityName = maxEntity;

        // Convert FIPS codes to place names
        if (visualization.type == VisualizationData.VisualizationType.Map) {
            minEntityName = new FIPS(minEntity).entityName();
            maxEntityName = new FIPS(maxEntity).entityName();
        } else if (visualization.type == VisualizationData.VisualizationType.Time) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("MMMMM d, yyyy");
            minEntityName = dateFormat.format(DateFormatter.parse(minEntity));
            maxEntityName = dateFormat.format(DateFormatter.parse(maxEntity));
        }

        DecimalFormat decimalFormat = new DecimalFormat("#.##");

        VisualizationData.Annotation minAnnotation = new VisualizationData.Annotation("Minimum Value", "The minimum value is " + decimalFormat.format(minValue) + " in " + minEntityName, "", minEntity);
        VisualizationData.Annotation maxAnnotation = new VisualizationData.Annotation("Maximum Value", "The maximum value is " + decimalFormat.format(maxValue) + " in " + maxEntityName, "", maxEntity);

        result.add(minAnnotation);
        result.add(maxAnnotation);

        return result;
    }
}
