package org.newsviews.Annotations;

import org.newsviews.DataStructures.FIPS;
import org.newsviews.DataStructures.Keywords;
import org.newsviews.DataStructures.VisualizationData;
import org.newsviews.Spatial.Spatial;
import org.newsviews.Utilities.Wikibrain;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Josh on 11/11/15.
 */
public class LocationAnnotator implements AnnotationGenerator.Annotator {
    public String name() {
        return "Location";
    }

    private Wikibrain wikibrain;
    private Spatial spatial;

    public LocationAnnotator(Wikibrain wikibrain) {
        this.wikibrain = wikibrain;
        spatial = new Spatial(wikibrain);
    }

    public List<VisualizationData.Annotation> annotateVisualization(VisualizationData visualization, Keywords keywords) {
        List<VisualizationData.Annotation> result = new ArrayList<VisualizationData.Annotation>();

        for (String location : keywords.locations()) {
            Spatial.Location coordinates = spatial.getCoordinatesForArticle(location);
            VisualizationData.Annotation annotation = new VisualizationData.Annotation(location, "", "", coordinates.toJSON().toString());
            result.add(annotation);
        }

        return result;
    }
}
