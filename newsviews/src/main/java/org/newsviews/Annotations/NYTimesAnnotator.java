package org.newsviews.Annotations;

import org.json.JSONArray;
import org.json.JSONObject;
import org.newsviews.DataStructures.FIPS;
import org.newsviews.DataStructures.Keywords;
import org.newsviews.Spatial.Spatial;
import org.newsviews.Utilities.DateFormatter;
import org.newsviews.Utilities.Logger;
import org.newsviews.Utilities.Networking;
import org.newsviews.DataStructures.VisualizationData;
import org.newsviews.QueryExtraction.QueryExtractor;
import org.newsviews.Utilities.Wikibrain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Josh on 11/9/15.
 */
public class NYTimesAnnotator extends ArticleAnnotator {
    private QueryExtractor queryExtractor;
    private Wikibrain wikibrain;
    private Spatial spatial;

    public NYTimesAnnotator(Wikibrain wikibrain, QueryExtractor queryExtractor, int maxNumberOfAnnotaitons) {
        super(maxNumberOfAnnotaitons);
        this.queryExtractor = queryExtractor;
        this.wikibrain = wikibrain;
        spatial = new Spatial(wikibrain);
    }

    public String name() {
        return "NYTimes";
    }

    public static class NYTimesArticle {

        public String url;
        public String title;
        public Date publishedDate;
        public String leadPharagraph;
        NYTimesArticle(String title, String url, String date, String leadPharagraph) {
            this.url = url;
            this.title = title;
            this.leadPharagraph = leadPharagraph;
            try {
                this.publishedDate = DateFormatter.parse(date);
            } catch (Exception e) {
                Logger.Log(e);
                this.publishedDate = new Date();
            }
        }
    }

    private static final String NYTimesAPIKey = "85f96420dacfdf4dc66f8514f39967b2:9:73379282";
    public static List<NYTimesArticle> getPossibleNYTimesArticles(List<String> keywords) {
        List<NYTimesArticle> articles = new ArrayList<NYTimesArticle>();
        String searchURL = "http://api.nytimes.com/svc/search/v2/articlesearch.json?q=";

        searchURL += String.join("+", keywords).replace(" ", "+");
        searchURL += "&fl=web_url%2Cheadline%2Cpub_date%2Clead_paragraph%2Cprint_page&api-key=" + NYTimesAPIKey;

        String articleJSON = Networking.downloadFile(searchURL).content;

        try {
            JSONObject json = new JSONObject(articleJSON);
            JSONArray jsonArticles = json.getJSONObject("response").getJSONArray("docs");
            for (int i = 0; i < jsonArticles.length(); i++) {
                JSONObject article = jsonArticles.getJSONObject(i);
                try {
                    String title = article.getJSONObject("headline").getString("main");
                    String url = article.getString("web_url");
                    String date = article.getString("pub_date");
                    String leadPharagraph = "";
                    if (!article.isNull("lead_paragraph")) {
                        leadPharagraph = article.getString("lead_paragraph");
                    }

                    NYTimesArticle nyTimesArticle = new NYTimesArticle(title, url, date, leadPharagraph);
                    articles.add(nyTimesArticle);
                } catch (Exception e) {
                    Logger.Log(e);
                }
            }

        } catch (Exception e) {
            Logger.Log(e);
        }

        return articles;
    }

    @Override
    protected List<Article> searchArticles(List<String> searchTerms, VisualizationData visualization, Keywords keywords) {
        List<Article> result = new ArrayList<Article>();
        List<NYTimesArticle> articles = getPossibleNYTimesArticles(searchTerms);

        for (NYTimesArticle article : articles) {
            Date articleDate = article.publishedDate;
            if (articleDate == null) {
                articleDate = new Date();
            }

            Keywords articleKeywords = queryExtractor.extractKeywords(article.leadPharagraph, articleDate);
            String location = "";

            switch (visualization.type) {
                case Time:
                    if (articleKeywords.dates().size() > 0) {
                        location = DateFormatter.format(articleKeywords.dates().get(0));
                    } else {
                        location = DateFormatter.format(articleDate);
                    }
                    break;
                case Map:
                    String place = "";
                    if (articleKeywords.locations().size() > 0) {
                        place = articleKeywords.locations().get(0);
                    } else if (keywords.locations().size() > 0) {
                        place = keywords.locations().get(0);
                    }

                    Spatial.Location coordinates = spatial.getCoordinatesForArticle(place);
                    if (coordinates != null) {
                        location = coordinates.toJSON().toString();
                    }

                    break;
            }

            result.add(new Article(article.title, article.leadPharagraph, location, article.url));
        }

        return result;
    }
}
