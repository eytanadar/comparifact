package org.newsviews.Annotations;

import org.newsviews.DataStructures.Keywords;
import org.newsviews.Utilities.Logger;
import org.newsviews.DataStructures.VisualizationData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Josh on 11/4/15.
 */
public class AnnotationGenerator {
    public interface Annotator {
        List<VisualizationData.Annotation> annotateVisualization(VisualizationData visualization, Keywords keywords);
        String name();
    }


    private ArrayList<Annotator> annotators = new ArrayList<Annotator>();
    public void addAnnotator(Annotator a) {
        annotators.add(a);
    }

    public void annotateVisualization(VisualizationData visualization, Keywords keywords) {
        ArrayList<VisualizationData> results = new  ArrayList<VisualizationData>();

        for (Annotator annotator : annotators) {
            Logger.Log("ANNOTATOR - " + annotator.name());
            try {
                List<VisualizationData.Annotation> annotations = annotator.annotateVisualization(visualization, keywords);
                visualization.annotations.addAll(annotations);
            } catch (Exception e) {
                Logger.Log(e);
            }
        }
    }

    public void annotateVisualizations(List<VisualizationData> visualizations, Keywords keywords) {
        for (Annotator annotator : annotators) {
            Logger.Log("ANNOTATOR - " + annotator.name());
            for (VisualizationData visualization : visualizations) {
                try {
                    List<VisualizationData.Annotation> annotations = annotator.annotateVisualization(visualization, keywords);
                    visualization.annotations.addAll(annotations);
                } catch (Exception e) {
                    Logger.Log(e);
                }
            }
        }
    }
}
