package org.newsviews;

import au.com.bytecode.opencsv.CSVWriter;
import org.apache.commons.collections4.map.PassiveExpiringMap;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.newsviews.Annotations.AnnotationGenerator;
import org.newsviews.Database.Database;
import org.newsviews.Utilities.Logger;

import javax.ws.rs.core.UriBuilder;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URI;
import java.util.Collections;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Created by Josh on 10/10/15.
 */
public class Server {
    private final String baseURL;
    private final int portNumber;
    private final URI baseURI;
    private HttpServer server;

    public static Map<String, String> turkKeys;
    public static CSVWriter logWritter;
    public static CSVWriter codeLogger;
    public static int currentID = 1;

    public Server(String baseURL, int portNumber) {
        try {
            turkKeys = Collections.synchronizedMap(new PassiveExpiringMap<String, String>(10, TimeUnit.MINUTES));
            logWritter = new CSVWriter(new FileWriter("logs/turk.csv", true));
            codeLogger = new CSVWriter(new FileWriter("logs/codes.csv", true));
        } catch (Exception e) {
            Logger.Log(e);
        }

        this.baseURL = baseURL;
        this.portNumber = portNumber;
        Logger.Log("Creating server with URL " + baseURL + " on port " + portNumber);
        baseURI = UriBuilder.fromUri(baseURL).port(portNumber).build();
    }

    public void startServer() throws IOException {
        ResourceConfig rc = new ResourceConfig().packages("org.newsviews");
        rc.register(CORSFilter.class);
        rc.register(JacksonFeature.class);

        Logger.Log("Starting server...");
        server = GrizzlyHttpServerFactory.createHttpServer(baseURI, rc);
        server.start();
        Logger.Log("Server started and is available at " + baseURI);
        Logger.Log("Press any key to stop...");
    }

    public void stopServer() {
        server.shutdownNow();
        Logger.Log("Server stopped");
    }
}
