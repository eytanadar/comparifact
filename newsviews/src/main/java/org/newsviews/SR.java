package org.newsviews;

import org.json.JSONArray;
import org.json.JSONObject;
import org.newsviews.Utilities.Logger;
import org.newsviews.Utilities.Networking;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Josh on 5/6/16.
 */
public class SR {
    public static double esa(String phrase1, String phrase2) {
        return sr(phrase1, phrase2, "esa");
    }

    public static double sr(String phrase1, String phrase2) {
        return sr(phrase1, phrase2, "similarity");
    }

    private static double sr(String phrase1, String phrase2, String method) {
        Networking.NetworkingResult result = Networking.downloadFile("http://spatialization.cs.umn.edu:9000/" + method
                + "?lang=en&phrases=" + URLEncoder.encode(phrase1) + "|" + URLEncoder.encode(phrase2));
        try {
            JSONObject json = new JSONObject(result.content);
            return json.getDouble("score");
        } catch (Exception e) {
            Logger.Log(e);
        }

        return 0.0;
    }

    public static List<String> pageWithImage(String title) {
        Networking.NetworkingResult networkResult = Networking.downloadFile("http://spatialization.cs.umn.edu:9000/imagePages"
                + "?lang=en&titles=" + URLEncoder.encode(title));
        try {
            JSONObject json = new JSONObject(networkResult.content);
            JSONArray pages = json.getJSONArray("pages");
            List<String> result = new ArrayList<String>();

            for (int i = 0; i < pages.length(); i++) {
                result.add(pages.getJSONObject(i).getString("title"));
            }

            return result;
        } catch (Exception e) {
            Logger.Log(e);
        }

        return Collections.emptyList();
    }
}
