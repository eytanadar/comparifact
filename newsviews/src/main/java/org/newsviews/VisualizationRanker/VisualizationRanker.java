package org.newsviews.VisualizationRanker;

import org.newsviews.DataStructures.Keywords;
import org.newsviews.DataStructures.VisualizationData;
import org.newsviews.QueryExtraction.TermSimilarity;

import java.util.*;

/**
 * Created by Josh on 12/15/15.
 */
public class VisualizationRanker {
    float minVisualizationScore;
    TermSimilarity termSimilarity;

    public VisualizationRanker(TermSimilarity termSimilarity, float minVisualizationScore) {
        this.termSimilarity = termSimilarity;
        this.minVisualizationScore = minVisualizationScore;
    }

    public List<VisualizationData> rankVisualizations(List<VisualizationData> visualizations, final Keywords keywords) {
        final HashMap<Long, Double> visualizationRanks = new HashMap<Long, Double>();
        for (VisualizationData visualization : visualizations) {
            Double score = visualizationRank(visualization, keywords);
            visualizationRanks.put(visualization.id, score);
        }

        Comparator<VisualizationData> comparator = new Comparator<VisualizationData>() {
            public int compare(VisualizationData o1, VisualizationData o2) {
                double vizScore1 = visualizationRanks.get(o1.id);
                double vizScore2 = visualizationRanks.get(o2.id);
                return Double.compare(vizScore2, vizScore1);
            }
        };

        ArrayList<VisualizationData> result = new ArrayList<VisualizationData>(visualizations);
        result.sort(comparator);

        // Remove the visualizations with a low rank
        for (int i = 0; i < result.size(); i++) {
            VisualizationData visualization = result.get(i);
            Double score = visualizationRanks.get(visualization.id);
            if (!Double.isFinite(score)) {
                result.remove(i);
                i--;
            } else if (score < minVisualizationScore) {
                result.remove(i);
                i--;
            }
        }

        for (VisualizationData data : result) {
            data.addDebugStatement("Ranked with a score of " + visualizationRanks.get(data.id));
        }

        return result;
    }

    public double visualizationRank(VisualizationData visualization, Keywords keywords) {
        double score = 0.0;

        for (String keyword : keywords.entities()) {
            for (String visualizationKeyword : visualization.annotationKeywords) {
                double similarity = termSimilarity.similarity(keyword, visualizationKeyword);
                if (Double.isFinite(similarity)) {
                    score += similarity;
                }
            }
        }

        score /= keywords.entities().size() * visualization.annotationKeywords.size();
        return score;
    }
}
