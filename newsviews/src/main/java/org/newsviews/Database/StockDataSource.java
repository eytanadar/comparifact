package org.newsviews.Database;

import org.json.JSONArray;
import org.json.JSONObject;
import org.newsviews.DataStructures.Keywords;
import org.newsviews.Utilities.DateFormatter;
import org.newsviews.Utilities.Logger;
import org.newsviews.Utilities.Networking;
import org.newsviews.DataStructures.VisualizationData;
import org.newsviews.Utilities.Wikibrain;

import java.io.BufferedReader;
import java.io.StringReader;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.util.*;

/**
 * Created by Josh on 10/20/15.
 */
public class StockDataSource implements Database.Datasource {
    public String name() {
        return "Stock";
    }

    public List<VisualizationData> generateVisualization(String input, Keywords keywords) {
        List<String> organizations = new ArrayList<String>();
        organizations.addAll(keywords.entities());

        StockSymbol symbol = null;
        String unresolvedCompanyName = "";
        for (String company : organizations) {
            symbol = resolveCompanyStockSymbol(company);
            if (symbol != null) {
                unresolvedCompanyName = company;
                break;
            }
        }

        if (symbol != null) {
            int rangeOfStockData = 0; // In years

            Calendar calendar = new GregorianCalendar();
            calendar.toInstant();
            calendar.add(Calendar.YEAR, -rangeOfStockData);

            int startYear = calendar.get(Calendar.YEAR);
            String stockData = Networking.downloadFile("http://ichart.finance.yahoo.com/table.csv?s="
                    + symbol.symbol + "&c=" + startYear).content;

            List<Date> dates = new ArrayList<Date>();
            List<Double> openingvalues = new ArrayList<Double>();

            BufferedReader reader = new BufferedReader(new StringReader(stockData));
            String line;
            try {
                int i = 0;
                while ((line = reader.readLine()) != null) {
                    if (i > 0) {
                        String components[] = line.split(",");
                        dates.add(DateFormatter.parse(components[0]));
                        openingvalues.add(Double.parseDouble(components[1]));
                    }
                    i++;
                }

                ArrayList<VisualizationData> results = new ArrayList<VisualizationData>();

                VisualizationData visualization = VisualizationData.createTimeseriesVisualizaion(dates, openingvalues, "Stock Price of " + symbol.company);
                visualization.annotationKeywords.add(symbol.company);
                visualization.annotationKeywords.add("stock");

                // Add debug information
                visualization.addDebugStatement("Resolved term: " + unresolvedCompanyName + " to company: " + symbol.company + "(" + symbol.symbol + ")");
                visualization.addDebugStatement("Creating stock visualization for " + symbol.company);

                results.add(visualization);

                return results;
            } catch (Exception e) {
                Logger.Log(e);
            }
        }

        return new ArrayList<VisualizationData>();
    }

    public class StockSymbol {
        public String symbol;
        public String company;
        public StockSymbol(String symbol, String company) {
            this.symbol = symbol;
            this.company = company;
        }
    }

    public StockSymbol resolveCompanyStockSymbol(String possibleCompanyName) {
        String baseURL = "https://s.yimg.com/aq/autoc?region=US&lang=en-US&callback=YAHOO.util.UHScriptNodeDataSource.callbacks&query=";

        String cleanedCompanyName = possibleCompanyName;
        // Remove "(text)" from name as Wikipedia will use these in some of the titles
        cleanedCompanyName = cleanedCompanyName.replaceAll("\\(([^)]+)\\)", "");
        cleanedCompanyName = cleanedCompanyName.trim();
        String encodedCompanyName = URLEncoder.encode(cleanedCompanyName);

        String jsonContent = Networking.downloadFile(baseURL + encodedCompanyName).content;
        try {
            jsonContent = jsonContent.substring(jsonContent.indexOf('(') + 1, jsonContent.lastIndexOf(')'));
            JSONObject json = new JSONObject(jsonContent);
            JSONArray array = json.getJSONObject("ResultSet").getJSONArray("Result");
            if (array.length() > 0) {
                StockSymbol symbol = new StockSymbol(array.getJSONObject(0).getString("symbol"), array.getJSONObject(0).getString("name"));
                return symbol;
            }
        } catch (Exception e) {
            Logger.Log("Unable to resolve string " + cleanedCompanyName + " to company stock symbol");
        }

        return null;
    }
}
