package org.newsviews.Database.Tables;

import au.com.bytecode.opencsv.CSVReader;
import org.newsviews.DataStructures.FIPS;
import org.newsviews.DataStructures.Keywords;
import org.newsviews.QueryExtraction.QueryExtractor;
import org.newsviews.QueryExtraction.TermSimilarity;
import org.newsviews.Reuters.ReutersIndexer;
import org.newsviews.Utilities.Logger;

import java.io.File;
import java.io.FileReader;
import java.util.*;
import java.util.concurrent.*;

/**
 * Created by Josh on 12/8/15.
 */
public class CensusTableDataSource extends TableDataSource {

    private List<String> variables;
    private Map<String, String> variableTablesMap;

    private Map<String, String> variableToDisplayName;
    private Map<String, String> displayNameToVariable;

    public CensusTableDataSource(int maximumNumberOfTerms, QueryExtractor queryExtractor, TermSimilarity termSimilarity, float minVariableFrequency, float maxVariableFrequency) {
        super(maximumNumberOfTerms, queryExtractor, termSimilarity, minVariableFrequency, maxVariableFrequency);
        variables = new ArrayList<String>();
        variableTablesMap = new HashMap<String, String>();

        variableToDisplayName = new HashMap<String, String>();
        displayNameToVariable = new HashMap<String, String>();

        // Load the table maps
        try {
            CSVReader reader = new CSVReader(new FileReader("table-map.csv"));

            String[] line;
            while ((line = reader.readNext()) != null) {
                String variable = cleanVariableName(line[1]);
                String displayName = cleanVariableName(line[0]);

                variableToDisplayName.put(variable, displayName);
                displayNameToVariable.put(displayName, variable);
            }
        } catch (Exception e) {
            Logger.Log(e);
        }

        // Load the available variables and file names from the text files
        File directory = new File("tables/");
        loadTablesFromFolder(directory);

        if (variables.size() == 0) {
            Logger.Log("Unable to load tables");
        }

        loadVariablesMap();
    }

    private String cleanVariableName(String displayName) {
        int index = displayName.lastIndexOf(".csv");
        if (index > 0) {
            displayName = displayName.substring(0, index);
        }

        displayName = displayName.replace("_", " ").toLowerCase();
        displayName = displayName.replace(".", " ");
        displayName = displayName.trim().replaceAll(" +", " ");

        return displayName;
    }

    private void loadTablesFromFolder(File file) {
        File[] directoryListing = file.listFiles();
        if (directoryListing != null) {
            for (File f : directoryListing) {
                if (f.isDirectory()) {
                    loadTablesFromFolder(f);
                } else {
                    String displayName = f.getName();
                    if (!displayName.endsWith(".csv")) {
                        continue;
                    }
                    displayName = cleanVariableName(displayName);

                    String variable = displayNameToVariable.get(displayName);
                    variables.add(variable);
                    variableTablesMap.put(variable, f.getPath());
                }
            }
        }
    }


    public String name() {
        return "Census Table";
    }

    public List<Map<String, String>> readTableForVariable(String variableName) {
        try {
            CSVReader reader = new CSVReader(new FileReader(variableTablesMap.get(variableName)));
            ArrayList<Map<String, String>> result = new ArrayList<Map<String, String>>();

            String[] header = reader.readNext();
            String[] line;
            while ((line = reader.readNext()) != null) {
                HashMap<String, String> map = new HashMap<String, String>();
                for (int i = 0; i < line.length; i++) {
                    map.put(header[i], line[i]);
                }
                result.add(map);
            }

            return result;
        } catch (Exception e) {
            Logger.Log(e);
        }
        return new ArrayList<Map<String, String>>();
    }
    public Map<FIPS, Double> getValuesFromTable(List<Map<String, String>> table, int year) {
        Map<FIPS, Double> result = new HashMap<FIPS, Double>();

        for (Map<String, String> map : table) {
            try {
                String fips = map.get("FIPS");
                fips = fips.replace("US", "");

                int dataYear = Integer.parseInt(map.get("Date"));

                if (year == dataYear) {
                    FIPS fipsCode = new FIPS(fips);
                    if (fipsCode.countyCode == 0 && fipsCode.stateCode == 0) {
                        // Ignore the value for the US
                        continue;
                    }
                    String value = map.get("value");
                    // Remove , and whitespace
                    value = value.replaceAll(",", "");

                    result.put(fipsCode, Double.parseDouble(value));
                }
            } catch (Exception e) {
                Logger.Log(e);
            }
        }

        return result;
    }
    public List<Integer> getYearsFromTable(List<Map<String, String>> table) {
        Set<Integer> years = new HashSet<Integer>();

        for (Map<String, String> map : table) {
            try {
                if (map.containsKey("Date")) {
                    years.add(Integer.parseInt(map.get("Date")));
                }
            } catch (Exception e) {
                Logger.Log(e);
            }
        }

        ArrayList<Integer> result = new ArrayList<Integer>();
        for (Integer i : years) {
            result.add(i);
        }

        result.sort(new YearComparator());

        return result;
    }
    public String getUnitsFromTable(List<Map<String, String>> table) {
        for (Map<String, String> map : table) {
            if (map.containsKey("units")) {
                return map.get("units");
            }
        }

        return null;
    }

    protected List<String> variables() {
        return variables;
    }

    @Override
    public String displayNameForVariable(String variable) {
        return super.displayNameForVariable(variableToDisplayName.get(variable));
    }

    @Override
    public String variableForDisplayName(String displayName) { return displayNameToVariable.get(super.variableForDisplayName(displayName)); }
}
