package org.newsviews.Database.Tables;

import au.com.bytecode.opencsv.CSVReader;
import org.newsviews.DataStructures.FIPS;
import org.newsviews.DataStructures.Keywords;
import org.newsviews.DataStructures.VisualizationData;
import org.newsviews.Database.Database;
import org.newsviews.QueryExtraction.QueryExtractor;
import org.newsviews.QueryExtraction.TermSimilarity;
import org.newsviews.Utilities.Logger;

import java.io.FileReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Josh on 12/8/15.
 */
public class MassShootingDataSource extends TableDataSource {
    private String fileLocation = "./mass shooting data/";
    private final Map<String, String> tableNames = new HashMap<String, String>() {{
                put("2013", "2013MASTER.csv");
                put("2014", "2014MASTER.csv");
                put("2015", "2015CURRENT.csv");
            }};
    private List<String> variables = new ArrayList<String>();

    public MassShootingDataSource(int maximumNumberOfTerms, QueryExtractor queryExtractor, TermSimilarity termSimilarity, float minVariableFrequency, float maxVariableFrequency) {
        super(maximumNumberOfTerms, queryExtractor, termSimilarity, minVariableFrequency, maxVariableFrequency);

        // Load the table data
        variables.add("Number of People who died in mass shootings");
        variables.add("Number of People who were injured in mass shootings");
        variables.add("Number of mass shootings");

        loadVariablesMap();
    }

    public String name() {
        return "Mass Shooting";
    }

    public List<Map<String, String>> readTableForVariable(String variableName) {
        ArrayList<Map<String, String>> result = new ArrayList<Map<String, String>>();
        try {
            for (String tableName : tableNames.values()) {
                CSVReader reader = new CSVReader(new FileReader(fileLocation + tableName));

                String[] header = reader.readNext();
                String[] line;
                while ((line = reader.readNext()) != null) {
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("date", line[1]);
                    map.put("location", line[5]);

                    if (variableName.equals(variables.get(0))) {
                        map.put("value", line[3]);
                    } else if (variableName.equals(variables.get(1))) {
                        map.put("value", line[4]);
                    } else if (variableName.equals(variables.get(2))) {
                        map.put("value", "1");
                    } else {
                        Logger.Log("Unknown variable " + variableName);
                    }

                    result.add(map);
                }
            }

            return result;
        } catch (Exception e) {
            Logger.Log(e);
        }
        return new ArrayList<Map<String, String>>();
    }
    public Map<FIPS, Double> getValuesFromTable(List<Map<String, String>> table, int year) {
        Map<FIPS, Double> result = new HashMap<FIPS, Double>();
        DateFormat format = new SimpleDateFormat("M/d/yyyy");
        Calendar cal = Calendar.getInstance();

        for (Map<String, String> dataPoint : table) {
            try {
                String date = dataPoint.get("date");
                if (date == null) {
                    continue;
                }

                Date parsedDate = format.parse(date);
                cal.setTime(parsedDate);
                if (cal.get(Calendar.YEAR) == year) {
                    // We are in the correct year
                    String location = dataPoint.get("location");
                    FIPS fips = FIPS.FIPSFromName(location);
                    if (fips != null) {
                        double value = Double.parseDouble(dataPoint.get("value"));
                        if (result.containsKey(fips)) {
                            value += result.get(fips);
                        }
                        result.put(fips, value);
                    }
                }
            } catch (Exception e) {
                Logger.Log(e);
            }
        }

        return result;
    }
    public List<Integer> getYearsFromTable(List<Map<String, String>> table) {
        List<Integer> years = new ArrayList<Integer>();
        years.add(2013);
        years.add(2014);
        years.add(2015);
        years.sort(new YearComparator());
        return years;
    }
    public String getUnitsFromTable(List<Map<String, String>> table) {
        return "People";
    }

    protected List<String> variables() {
        return variables;
    }
}
