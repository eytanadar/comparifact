package org.newsviews.Database.Tables;

import au.com.bytecode.opencsv.CSVReader;
import org.apache.commons.lang3.text.WordUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.newsviews.DataStructures.FIPS;
import org.newsviews.DataStructures.Keywords;
import org.newsviews.DataStructures.VisualizationData;
import org.newsviews.Database.Database;
import org.newsviews.QueryExtraction.QueryExtractor;
import org.newsviews.QueryExtraction.TermSimilarity;
import org.newsviews.Reuters.ReutersIndexer;
import org.newsviews.Utilities.DateFormatter;
import org.newsviews.Utilities.Logger;
import org.newsviews.Utilities.Networking;
import org.newsviews.Utilities.Wikibrain;

import java.io.File;
import java.io.FileReader;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by Josh on 10/25/15.
 */
 abstract public class TableDataSource implements Database.Datasource {
    private int maximumNumberOfTerms = 0;
    private QueryExtractor queryExtractor;
    private TermSimilarity termSimilarity;
    private float minVariableFrequency;
    private float maxVariableFrequency;

    TableDataSource(int maximumNumberOfTerms, QueryExtractor queryExtractor, TermSimilarity termSimilarity, float minVariableFrequency, float maxVariableFrequency) {
        this.maximumNumberOfTerms = maximumNumberOfTerms;
        this.queryExtractor = queryExtractor;
        this.termSimilarity = termSimilarity;
        this.minVariableFrequency = minVariableFrequency;
        this.maxVariableFrequency = maxVariableFrequency;
    }

    final int numberOfThreads = Runtime.getRuntime().availableProcessors();
    private Map<String, List<String>> variableKeywordsMap;
    protected void loadVariablesMap() {
        variableKeywordsMap = new HashMap<String, List<String>>();
        Logger.Log("Loading Tables");
        try {
            ExecutorService executor = Executors.newFixedThreadPool(numberOfThreads);
            CompletionService<VariableKeywordExtractor.VariableKeywordResult> completionService = new ExecutorCompletionService<VariableKeywordExtractor.VariableKeywordResult>(executor);
            for (String variable : variables()) {
                VariableKeywordExtractor search = new VariableKeywordExtractor(variable);
                completionService.submit(search);
            }

            int number = 0;
            for (String variable : variables()) {
                number++;
                if (number % 10 == 0) {
                    Logger.Log("Loaded " + number);
                }
                Future<VariableKeywordExtractor.VariableKeywordResult> future = completionService.take();
                VariableKeywordExtractor.VariableKeywordResult searchResult = future.get();
                variableKeywordsMap.put(searchResult.variable, searchResult.keywords);

                String debug = searchResult.variable + "\n";
                for (String keyword : searchResult.keywords) {
                    debug += "\t" + keyword + "\n";
                }
                Logger.Log(debug);
            }
        } catch (Exception e) {
            Logger.Log(e);
        }
        Logger.Log("Loading Finished");
    }

    // Used to extract keywords from a table name
    private final class VariableKeywordExtractor implements Callable<VariableKeywordExtractor.VariableKeywordResult> {
        class VariableKeywordResult {
            public String variable;
            public List<String> keywords;
            public VariableKeywordResult(String variable, List<String> keywords) {
                this.variable = variable;
                this.keywords = keywords;
            }
        }

        public String variable;

        public VariableKeywordExtractor(String variable) {
            this.variable = variable;
        }

        public VariableKeywordResult call() throws Exception {
            // Use the Query Extract to find all the entities in the variable and use those as keywords
            ArrayList<String> result = new ArrayList<String>();
            try {
                Keywords keywords = queryExtractor.extractKeywords(variable, new Date());

                // Find the keywords with the correct document frequency
                for (String key : keywords.entities()) {
                    double freq = ReutersIndexer.termFrequency(key);
                    if (freq <= 0.01 && freq >= 0.0001) {
                        result.add(key);
                    }
                }
            } catch (Exception e) {
                Logger.Log(e);
            }

            // If we were unable to extract any keywords using the query extraction
            // then just use all of the individual words,
            if (result.size() <= 0) {
                result = new ArrayList<String>(Arrays.asList(variable.split("^[^ ]*\\s")));
            }

            // Remove all the empty terms
            for (Iterator<String> iter = result.listIterator(); iter.hasNext(); ) {
                String a = iter.next();
                if (a.length() == 0) {
                    iter.remove();
                }
            }

            return new VariableKeywordResult(variable, result);
        }
    }

    protected class YearComparator implements Comparator<Integer> {
        public int compare(Integer a, Integer b) {
            return b.compareTo(a);
        }
    }

    public abstract List<Map<String, String>> readTableForVariable(String variableName);
    public abstract Map<FIPS, Double> getValuesFromTable(List<Map<String, String>> table, int year);
    public abstract List<Integer> getYearsFromTable(List<Map<String, String>> table);
    public abstract String getUnitsFromTable(List<Map<String, String>> table);

    protected abstract List<String> variables();

    public String displayNameForVariable(String variable) {
        return WordUtils.capitalize(variable);
    }
    public String variableForDisplayName(String displayName) { return WordUtils.uncapitalize(displayName); }

    public List<VisualizationData> generateVisualization(String input, Keywords keywords) {
        List<VisualizationData> result = new ArrayList<VisualizationData>();

        // Find the best variable using several threads
        try {
            List<String> rankedVarables = rankVariables(keywords);

            // Create visuaizations for the ranked variables
            for (String rankedVariable : rankedVarables) {
                if (result.size() >= maximumNumberOfTerms) {
                    for (int i = maximumNumberOfTerms; i < result.size(); i++) {
                        result.remove(result.size() - 1);
                    }
                    break;
                }

                result.addAll(createVisualizationForVariable(rankedVariable, keywords));
            }

            // Add debug info
            String variablesDebugString = "The Top 15 Ranked variables:\n";
            int end = Math.min(rankedVarables.size(), 15);
            for (int i = 0; i < end; i++) {
                // TODO: add back in debugging information
//                String rankedVariable = rankedVarables.get(i);
//                String variableDisplayName = displayNameForVariable(rankedVariable);
//                variablesDebugString += "\t- " + variableDisplayName + "\t(" + relatednessValues.get(rankedVariable) + ")\n";
//
//                String termSimilarityDebugInfo = termSimilairtyDebugStatements.get(rankedVariable);
//                for (String debugInfo : termSimilarityDebugInfo.split("\n")) {
//                    variablesDebugString += "\t\t\t" + debugInfo + "\n";
//                }
            }
            for (VisualizationData visualization : result) {
                visualization.addDebugStatement(variablesDebugString);
            }

        } catch (Exception e) {
            Logger.Log(e);
        }

        return result;
    }

    public List<String> rankVariables(Keywords keywords) throws InterruptedException, ExecutionException {
        Map<String, Double> relatednessValues = new HashMap<String, Double>();
        ExecutorService executor = Executors.newFixedThreadPool(numberOfThreads);
        CompletionService<TermSimilarity.TermSimilaritySearch.TermSimilaritySearchResult> completionService = new ExecutorCompletionService<TermSimilarity.TermSimilaritySearch.TermSimilaritySearchResult>(executor);
        for (String variable : variables()) {
            List<String> keyword = variableKeywordsMap.get(variable);

            TermSimilarity.TermSimilaritySearch search = termSimilarity.createSearch(variable, keyword, keywords.entities());
            completionService.submit(search);
        }

        Map<String, String> termSimilairtyDebugStatements = new HashMap<String, String>();
        for (String variable : variables()) {
            Future<TermSimilarity.TermSimilaritySearch.TermSimilaritySearchResult> future = completionService.take();
            TermSimilarity.TermSimilaritySearch.TermSimilaritySearchResult searchResult = future.get();
            relatednessValues.put(searchResult.variable, searchResult.value);
            termSimilairtyDebugStatements.put(searchResult.variable, searchResult.debugInformation);
        }

        class VaraiableComparator implements Comparator<String> {
            Map<String, Double> relatednessValues;

            public int compare(String o1, String o2) {
                return relatednessValues.get(o2).compareTo(relatednessValues.get(o1));
            }
        }

        VaraiableComparator comparator = new VaraiableComparator();
        comparator.relatednessValues = relatednessValues;

        List<String> rankedVarables = new ArrayList<String>(variables());
        Collections.sort(rankedVarables, comparator);
        return rankedVarables;
    }

    private List<VisualizationData> createVisualizationForVariable(String variable, Keywords keywords) {
        ArrayList<VisualizationData> result = new ArrayList<VisualizationData>();

        // Generate visualization
        try {
            List<Map<String, String>> table = readTableForVariable(variable);
            List<Integer> years = getYearsFromTable(table);
            Map<FIPS, Double> values = getValuesFromTable(table, years.get(0));

            List<FIPS> locations = new ArrayList<FIPS>();
            List<Double> locationValues = new ArrayList<Double>();

            for (Map.Entry<FIPS, Double> entry : values.entrySet()) {
                locations.add(entry.getKey());
                locationValues.add(entry.getValue());
            }

            String displayVariable = displayNameForVariable(variable);
            displayVariable = WordUtils.capitalize(displayVariable);

            VisualizationData visualization = VisualizationData.createMapVisualizaion(locations, locationValues, displayVariable);
            visualization.annotationKeywords.addAll(variableKeywordsMap.get(variable));
            result.add(visualization);

            visualization.addDebugStatement("Generating visualization for variable " + displayVariable);

            // Try to produce a time series visualization
            if (years.size() > 1 && keywords.locations().size() > 0) {
                String place = keywords.locations().get(0);
                FIPS code = FIPS.FIPSFromName(place);
                if (code != null) {
                    List<Date> dates = new ArrayList<Date>();
                    List<Double> dateValues = new ArrayList<Double>();

                    for (Integer year : years) {
                        // The date will be arbitrarily the first of the year
                        dates.add(DateFormatter.parse(year + "-1-1"));

                        Map<FIPS, Double> yearValues = getValuesFromTable(table, year);
                        dateValues.add(yearValues.get(code));
                    }

                    VisualizationData timeseriesVisualization = VisualizationData.createTimeseriesVisualizaion(dates, dateValues, displayVariable + " in " + place);
                    timeseriesVisualization.annotationKeywords.addAll(variableKeywordsMap.get(variable));

                    timeseriesVisualization.addDebugStatement("Generating visualization for variable " + variable + " in " + place);

                    result.add(timeseriesVisualization);
                }
            }
        } catch (Exception e) {
            Logger.Log(e);
        }
        return result;
    }
}
