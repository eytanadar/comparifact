package org.newsviews.Database;

import org.newsviews.DataStructures.Keywords;
import org.newsviews.Database.Tables.CensusTableDataSource;
import org.newsviews.Database.Tables.MassShootingDataSource;
import org.newsviews.Database.Tables.TableDataSource;
import org.newsviews.Utilities.Logger;
import org.newsviews.DataStructures.VisualizationData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Josh on 10/20/15.
 */
public class Database {
    public interface Datasource {
        List<VisualizationData> generateVisualization(String input, Keywords keywords);
        String name();
    }

    private ArrayList<Datasource> datasources = new ArrayList<Datasource>();
    public void addDatasource(Datasource d) {
        datasources.add(d);
    }

    public List<VisualizationData> generateVisualization(String input, Keywords keywords) {
        ArrayList<VisualizationData> results = new  ArrayList<VisualizationData>();

        for (Datasource source : datasources) {
            Logger.Log("DATASOURCE - " + source.name());
            try {
                results.addAll(source.generateVisualization(input, keywords));
            } catch (Exception e) {
                Logger.Log(e);
            }
        }

        return results;
    }
}
