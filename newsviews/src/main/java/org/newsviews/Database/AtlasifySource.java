package org.newsviews.Database;

import org.apache.commons.io.IOUtils;
import org.json.JSONObject;
import org.newsviews.DataStructures.FIPS;
import org.newsviews.DataStructures.Keywords;
import org.newsviews.Utilities.Logger;
import org.newsviews.Utilities.Networking;
import org.newsviews.DataStructures.VisualizationData;
import org.newsviews.Utilities.Wikibrain;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Josh on 11/8/15.
 */
public class AtlasifySource implements Database.Datasource {
    final private String atlasifyBaseURL = "http://spatialization.cs.umn.edu/wikibrain";
    final private String stateIdsFile = "Ids/statesIds.js";
    final private String countiesIdsFile = "Ids/countiesIds.js";

    private List<String> wikipediaStateIds;
    private List<String> wikipediaStateNames;
    private List<String> wikipediaCountiesIds;
    private List<String> wikipediaCountiesNames;

    int numberOfVisualizations;
    public AtlasifySource(int numberOfVisualizations) {
        this.numberOfVisualizations = numberOfVisualizations;

        wikipediaStateIds = new ArrayList<String>();
        wikipediaStateNames = new ArrayList<String>();
        wikipediaCountiesIds = new ArrayList<String>();
        wikipediaCountiesNames = new ArrayList<String>();

        try {
            FileInputStream inputStream = new FileInputStream(stateIdsFile);

            JSONObject stateIds = new JSONObject(IOUtils.toString(inputStream));

            inputStream.close();
            inputStream = new FileInputStream(countiesIdsFile);

            JSONObject countiesIds = new JSONObject(IOUtils.toString(inputStream));

            inputStream.close();


            for (String key : stateIds.keySet()) {
                wikipediaStateNames.add(key);
                wikipediaStateIds.add(stateIds.getString(key));
            }

            for (String key : countiesIds.keySet()) {
                wikipediaCountiesNames.add(key);
                wikipediaCountiesIds.add(countiesIds.getString(key));
            }
        } catch (Exception e) {
            Logger.Log(e);
        }
    }

    public String name() {
        return "Atlasify";
    }

    private VisualizationData createVisualization(String keyword) {
        JSONObject sendData = new JSONObject();
        sendData.put("keyword", keyword);
        sendData.put("refSystem", "state");
        sendData.put("featureIdList", wikipediaStateIds);
        sendData.put("featureNameList", wikipediaStateNames);

        String result = Networking.PostRequest(atlasifyBaseURL + "/send", sendData).content;

        JSONObject json = new JSONObject(result);
        List<FIPS> fips = new ArrayList<FIPS>();
        List<Double> values = new ArrayList<Double>();

        for (String key : json.keySet()) {
            if (key.equals("undefined")) {
                continue;
            }
            FIPS fipsCode = FIPS.FIPSFromName(key);
            if (fipsCode != null) {
                fips.add(fipsCode);
                values.add(json.getDouble(key));
            } else {
                Logger.Log("Unknown FIPS code for location " + key);
            }
        }

        VisualizationData visualization = VisualizationData.createMapVisualizaion(fips, values, "The relatedness between " + keyword + " and the United States");
        visualization.annotationKeywords.add(keyword);

        // Set the appropriate Atlasify scale
        visualization.maxYValue = 1.0;
        visualization.minYValue = 0.0;

        // Add debugging information
        visualization.addDebugStatement("Atlasify visualization with keyword: " + keyword);

        return visualization;
    }

    public List<VisualizationData> generateVisualization(String input, Keywords keywords) {
        List<VisualizationData> result = new ArrayList<VisualizationData>();

        int numberOfCreatedVisualizations = 0;
        for (String keyword : keywords.entities()) {
            if (numberOfCreatedVisualizations >= numberOfVisualizations) {
                break;
            }
            result.add(createVisualization(keyword));
            numberOfCreatedVisualizations++;
        }

        return result;
    }
}
