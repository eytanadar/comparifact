package org.newsviews.DataStructures;

import org.newsviews.Utilities.DateFormatter;
import org.newsviews.Utilities.Logger;

import java.security.Key;
import java.util.*;

/**
 * Created by Josh on 11/9/15.
 */
public class Keywords {
    public enum KeywordType {
        LOCATION, PERSON, ORGANIZATION, MONEY, PERCENT, DATE, NUMBER, OTHER;

        static public KeywordType TypeFromString(String type) {
            if (type.equals("LOCATION")) {
                return LOCATION;
            } else if (type.equals("PERSON")) {
                return PERSON;
            } else if (type.equals("ORGANIZATION")) {
                return ORGANIZATION;
            } else if (type.equals("MONEY")) {
                return MONEY;
            } else if (type.equals("PERCENT")) {
                return PERCENT;
            } else if (type.equals("DATE")) {
                return DATE;
            } else if (type.equals("NUMBER")) {
                return NUMBER;
            } else {
                return OTHER;
            }
        }

        @Override
        public String toString() {
            switch (this) {
                case LOCATION:
                    return "LOCATION";
                case PERSON:
                    return "PERSON";
                case ORGANIZATION:
                    return "ORGANIZATION";
                case MONEY:
                    return "MONEY";
                case PERCENT:
                    return "PERCENT";
                case DATE:
                    return "DATE";
                case NUMBER:
                    return "NUMBER";
                default:
                    return "OTHER";
            }
        }
    };
    static public class Keyword {
        public String entity;
        public KeywordType type;

        public Keyword(String entity, KeywordType type) {
            this.entity = entity;
            this.type = type;
        }

        @Override
        public boolean equals(Object obj) {
            if (!(obj instanceof Keyword)) {
                return false;
            }

            Keyword key = (Keyword)obj;
            return key.entity.equals(entity) && key.type == type;
        }

        @Override
        public int hashCode() {
            return entity.hashCode() ^ type.hashCode();
        }
    }

    private List<Keyword> keywords;

    public Keywords() {
        keywords = new ArrayList<Keyword>();
    }

    public void addKeyword(String entity, KeywordType type) {
        Keyword key = new Keyword(entity, type);
        addKeyword(key);
    }

    public void addKeyword(Keyword key) {
        keywords.add(key);
        needsArraysUpdate = true;
    }

    public void addKeywords(List<Keyword> keys) {
        keywords.addAll(keys);
        needsArraysUpdate = true;
    }

    public List<Keyword> toList() {
        List<Keyword> list = new ArrayList<Keyword>();
        for (Keyword key : keywords) {
            list.add(key);
        }
        return list;
    }

    private List<String> locations, entities;
    private List<Double> money, percents, numbers;
    private List<Date> dates;

    private boolean needsArraysUpdate = true;
    private void updateArrays() {
        if (needsArraysUpdate) {
            needsArraysUpdate = false;

            locations     = new ArrayList<String>();
            entities      = new ArrayList<String>();

            money    = new ArrayList<Double>();
            percents = new ArrayList<Double>();
            numbers  = new ArrayList<Double>();

            dates = new ArrayList<Date>();
            Set<Date> datesSet = new HashSet<Date>();

            for (Keyword key : keywords) {
                switch (key.type) {
                    case LOCATION:
                        locations.add(key.entity);
                        break;
                    case PERSON:
                        entities.add(key.entity);
                        break;
                    case ORGANIZATION:
                        entities.add(key.entity);
                        break;
                    case MONEY:
                        try {
                            String moneyString = key.entity.replace("$", "");
                            moneyString = moneyString.replace("<", "");
                            moneyString = moneyString.replace(">", "");
                            moneyString = moneyString.replace("=", "");
                            moneyString = moneyString.replace("~", "");
                            money.add(Double.parseDouble(moneyString));
                        } catch (Exception e) {
                            Logger.Log(e);
                        }

                        break;
                    case PERCENT:
                        try {
                            String percentString = key.entity.replace("%", "");
                            percents.add(Double.parseDouble(percentString));
                        } catch (Exception e) {
                            Logger.Log(e);
                        }

                        break;
                    case DATE:
                        Date date = DateFormatter.parse(key.entity);
                        if (date != null) {
                            if (!datesSet.contains(date)) {
                                datesSet.add(date);
                                dates.add(date);
                            }
                        }
                        break;
                    case NUMBER:
                        try {
                            String numberString = key.entity;
                            numberString = numberString.replace("<", "");
                            numberString = numberString.replace(">", "");
                            numberString = numberString.replace("=", "");
                            numberString = numberString.replace("~", "");
                            numbers.add(Double.parseDouble(numberString));
                        } catch (Exception e) {
                            Logger.Log(e);
                        }

                        break;
                    case OTHER:
                        entities.add(key.entity);
                        break;
                }
            }

            // Make all the collections immutable
            locations     = Collections.unmodifiableList(locations);
            entities      = Collections.unmodifiableList(entities);

            money    = Collections.unmodifiableList(money);
            percents = Collections.unmodifiableList(percents);
            numbers  = Collections.unmodifiableList(numbers);

            dates = Collections.unmodifiableList(dates);
        }
    }

    public List<String> locations() {
        updateArrays();
        return locations;
    }

    public List<Double> money() {
        updateArrays();
        return money;
    }

    public List<Double> percents() {
        updateArrays();
        return percents;
    }

    public List<Date> dates() {
        updateArrays();
        return dates;
    }

    public List<Double> numbers() {
        updateArrays();
        return numbers;
    }

    public List<String> entities() {
        updateArrays();
        return entities;
    }

    public List<String> phraseDescriptions() {
        List<String> result = new ArrayList<String>();

        for (String entity : entities) {
            result.add(entity + " (OTHER)");
        }

        for (String location : locations) {
            result.add(location + " (LOCATION)");
        }

        for (Date date : dates) {
            result.add(DateFormatter.format(date) + " (DATE)");
        }

        for (Double number : numbers) {
            result.add(number + " (NUMBER)");
        }

        for (Double moneyValue : money) {
            result.add(moneyValue + " (MONEY)");
        }

        for (Double percent : percents) {
            result.add(percent + " (PERCENT)");
        }

        return result;
    }
}
