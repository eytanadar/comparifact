package org.newsviews.DataStructures;

import org.apache.commons.lang3.StringEscapeUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.newsviews.Utilities.DateFormatter;
import org.newsviews.Utilities.Logger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * Created by Josh on 10/20/15.
 */
public class VisualizationData {
    static private long currentID = 0;
    public enum VisualizationType {
        Time(1),
        Map(2);
        private int value;
        private VisualizationType(int i) {
            value = i;
        }
        public int getValue() {
            return value;
        }

        public String toString() {
            switch (value) {
                case 1:
                    return "time";
                case 2:
                    return "map";
                default:
                    return "";
            }
        }
    };

    public List<String> entities;
    public List<Double> values;
    public VisualizationType type;
    public String minXValue, maxXValue;
    public double minYValue, maxYValue;
    public double mapCenterX, mapCenterY;
    public boolean useStateLevelMap;
    public int mapZoom;
    public String title;
    public List<Annotation> annotations;
    public List<String> annotationKeywords;
    public long id;
    public String debugString;

    public static class Annotation {
        public String title;
        public String text;
        public String url;
        public String location;
        public Annotation(String title, String text, String url, String location) {
            this.title = title;
            this.text = text;
            this.url = url;
            this.location = location;
        }

        public JSONObject toJSON() {
            JSONObject json = new JSONObject();
            json.put("title", title);
            json.put("text", text);
            json.put("url", url);
            json.put("location", location);
            return json;
        }
    }

    public static VisualizationData createTimeseriesVisualizaion(List<Date> dates, List<Double> values, String title) {
        VisualizationData data = new VisualizationData();
        data.title = title;

        if (dates.size() != values.size()) {
            throw new ExceptionInInitializerError("Input list lengths do not match");
        }

        if (dates.size() > 0) {
            data.type = VisualizationType.Time;
            data.values = values;
            for (Date d : dates) {
                data.entities.add(DateFormatter.format(d));
            }

            Date minDate = Collections.min(dates);
            Date maxDate = Collections.max(dates);
            Double minValue = Collections.min(values);
            Double maxValue = Collections.max(values);

            data.minXValue = DateFormatter.format(minDate);
            data.maxXValue = DateFormatter.format(maxDate);

            data.minYValue = minValue;
            data.maxYValue = maxValue;
        } else {
            ArrayList<Date> tempDates = new ArrayList<Date>();
            ArrayList<Double> tempValues = new ArrayList<Double>();
            tempDates.add(new Date());
            tempValues.add(0.0);
            return createTimeseriesVisualizaion(tempDates, tempValues, title);
        }

        return data;
    }

    public static VisualizationData createMapVisualizaion(List<FIPS> places, List<Double> values, String title) {
        VisualizationData data = new VisualizationData();
        data.title = title;

        if (places.size() != values.size()) {
            throw new ExceptionInInitializerError("Input list lengths do not match");
        }

        data.type = VisualizationType.Map;
        data.values = values;
        for (FIPS place : places) {
            String location = place.toString(); //  0 + "_" + place.countyCode + "_" + place.stateCode;
            data.entities.add(location);
        }

        data.mapCenterX = 0.0;
        data.mapCenterY = 0.0;
        data.mapZoom = 2;

        if (values.size() > 0) {
            Double minValue = Collections.min(values);
            Double maxValue = Collections.max(values);

            data.useStateLevelMap = true;
            for (FIPS place : places) {
                if (!place.isState) {
                    data.useStateLevelMap = false;
                    break;
                }
            }

            data.minYValue = minValue;
            data.maxYValue = maxValue;
        }

        return data;
    }

    public VisualizationData() {
        entities = new ArrayList<String>();
        values = new ArrayList<Double>();
        minXValue = "";
        maxXValue = "";
        minYValue = maxYValue = 0.0;
        mapCenterX = 0.0;
        mapCenterY = 0.0;
        mapZoom = 1;
        title = "";
        useStateLevelMap = true;
        annotations = new ArrayList<Annotation>();
        annotationKeywords = new ArrayList<String>();
        debugString = "";
        id = currentID++;
    }

    public JSONObject toJson(boolean debug) {
        JSONObject visualization = new JSONObject();
        visualization.put("type", type.toString());
        visualization.put("entities", entities);
        visualization.put("values", values);
        visualization.put("title", title);


        JSONArray yRange = new JSONArray();
        yRange.put(minYValue);
        yRange.put(maxYValue);
        visualization.put("y-range", yRange);

        switch (type) {
            case Time:
                JSONArray xRange = new JSONArray();
                xRange.put(minXValue);
                xRange.put(maxXValue);
                visualization.put("x-range", xRange);
                break;
            case Map:
                JSONArray mapCenter = new JSONArray();
                mapCenter.put(mapCenterX);
                mapCenter.put(mapCenterY);

                visualization.put("geo-center", mapCenter);
                visualization.put("geo-zoom", mapZoom);
                visualization.put("use-state-level", useStateLevelMap);
                break;
            default:
                break;
        }

        JSONArray jsonAnnotation = new JSONArray();
        for (Annotation annotation : annotations) {
            jsonAnnotation.put(annotation.toJSON());
        }
        visualization.put("annotations", jsonAnnotation);

        if (debug) {
            visualization.put("debug-info", debugString);
        }

        return visualization;
    }

    public void addDebugStatement(String statement) {
        debugString += statement + "\n";
    }
}
