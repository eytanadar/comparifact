package org.newsviews.DataStructures;

import au.com.bytecode.opencsv.CSVReader;
import org.json.JSONArray;
import org.json.JSONObject;
import org.newsviews.Spatial.Spatial;
import org.newsviews.Utilities.Logger;
import org.newsviews.Utilities.Networking;
import org.newsviews.Utilities.Wikibrain;

import java.io.FileReader;
import java.lang.reflect.Array;
import java.util.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by Josh on 10/28/15.
 */
public class FIPS implements Comparable<FIPS> {
    private static Map<FIPS, String> countyNameMap;
    private static Map<String, FIPS> fipsMap;
    private static Lock mapsLock = new ReentrantLock();
    private static Spatial spatial;
    private static Wikibrain wikibrain;
    private static void loadMaps() {
        mapsLock.lock();
        if (countyNameMap != null) {
            mapsLock.unlock();
            return;
        }

        wikibrain = new Wikibrain("simple", false);
        spatial = new Spatial(wikibrain);

        countyNameMap = new HashMap<FIPS, String>();
        fipsMap = new HashMap<String, FIPS>();

        try {
            CSVReader reader = new CSVReader(new FileReader("counties.csv"));
            String[] line;
            while ((line = reader.readNext()) != null) {
                String state = line[0];
                FIPS fips = new FIPS(line[1] + line[2]);
                String county = line[3];
                county = county + ", " + state;

                countyNameMap.put(fips, county);
                fipsMap.put(county, fips);

                FIPS stateFips = new FIPS(line[1]);
                countyNameMap.put(stateFips, state);
                fipsMap.put(state, stateFips);
            }
        } catch (Exception e) {
            Logger.Log(e);
        }
        mapsLock.unlock();
    }

    public int stateCode;
    public int countyCode;
    public boolean isState;

    public FIPS(int stateCode, int countyCode) {
        this.stateCode = stateCode;
        this.countyCode = countyCode;
        isState = false;
    }
    public FIPS(int stateCode) {
        this.stateCode = stateCode;
        this.countyCode = 0;
        isState = true;
    }
    public FIPS(String s) {
        s = s.replace("_", "");

        if (s.length() > 3) {
            countyCode = Integer.parseInt(s.substring(s.length() - 3));
            stateCode  = Integer.parseInt(s.substring(0, s.length() - 3));
            isState = false;
        } else {
            stateCode = Integer.parseInt(s);
            countyCode = 0;
            isState = true;
        }

    }

    static public FIPS FIPSFromName(String location) {
        loadMaps();
        // Look up the FIPS code from the initial database
        FIPS storedFIPS = fipsMap.get(location);
        if (storedFIPS != null) {
            return storedFIPS;
        }

        String code = spatial.getFIPSCodeString(location);
        if (code != null) {
            return new FIPS(code);
        }

        // Wikidata FIPS codes
        // P901, P883, P882,

        // Otherwise use spatial to get the coordinates
        Spatial.Location coordinates = spatial.getCoordinatesForArticle(location);
        if (coordinates == null) {
            return null;
        }

        return convertLatLngToFIPS(coordinates.latitude, coordinates.longitude);
    }

    public Spatial.Location getCoordinates() {
        return spatial.getCoordinatesForArticle(entityName());
    }

    static public FIPS convertLatLngToFIPS(Double lat, Double lon) {
        String url = "http://www.datasciencetoolkit.org/coordinates2politics/" + lat + "%2c" + lon;
        String fips = Networking.downloadFile(url, 2000).content;

        try {
            JSONObject json = new JSONObject(fips);
            JSONArray politics = json.getJSONArray("politics");
            String code = "";
            for (int i = 0; i < politics.length(); i++) {
                if (politics.getJSONObject(i).getString("type") == "admin6") {
                    code = politics.getJSONObject(i).getString("code");
                }
            }

            if (code.length() > 0) {
                return new FIPS(code);
            }
        } catch (Exception e) {
            Logger.Log(e);
        }

        return null;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof FIPS)) {
            return false;
        }

        FIPS fips = (FIPS) obj;

        // Custom equality check here.
        return this.stateCode == fips.stateCode && this.countyCode == fips.countyCode;
    }

    @Override
    public int hashCode() {
        return stateCode * 37 + countyCode;
    }

    public String entityName() {
        loadMaps();
        return countyNameMap.get(this);
    }

    @Override
    public String toString() {
        if (isState) {
            return stateCode + "";
        } else  {
            String countyString = "" + countyCode;
            if (countyString.length() == 2) {
                countyString = "0" + countyString;
            } else if (countyString.length() == 1) {
                countyString = "00" + countyString;
            }
            return stateCode + countyString;
        }
    }

    public int compareTo(FIPS o) {
        return toString().compareTo(o.toString());
    }

    public static List<FIPS> allFIPSCodes() {
        loadMaps();
        List<FIPS> codes = new ArrayList<FIPS>();
        for (FIPS fips : countyNameMap.keySet()) {
            codes.add(fips);
        }
        Collections.sort(codes);
        return codes;
    }
}
