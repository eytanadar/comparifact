package org.newsviews;
import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;
import com.mdimension.jchronic.Chronic;
import de.l3s.boilerpipe.BoilerpipeExtractor;
import de.l3s.boilerpipe.extractors.CommonExtractors;
import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.ling.Sentence;
import edu.stanford.nlp.process.DocumentPreprocessor;
import org.apache.commons.collections4.map.PassiveExpiringMap;
import org.apache.commons.io.FileUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.newsviews.Annotations.*;
import org.newsviews.Comparisons.ComparisonChecker;
import org.newsviews.DataStructures.FIPS;
import org.newsviews.DataStructures.Keywords;
import org.newsviews.DataStructures.VisualizationData;
import org.newsviews.Database.AtlasifySource;
import org.newsviews.Database.Database;
import org.newsviews.Database.StockDataSource;
import org.newsviews.Database.Tables.CensusTableDataSource;
import org.newsviews.Database.Tables.TableDataSource;
import org.newsviews.QueryExtraction.QueryExtractor;
import org.newsviews.QueryExtraction.TermSimilarity;
import org.newsviews.Spatial.Spatial;
import org.newsviews.Utilities.*;
import org.newsviews.VisualizationRanker.VisualizationRanker;

import java.io.*;
import java.math.BigInteger;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.SecureRandom;
import java.util.*;
import java.util.concurrent.TimeUnit;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

/**
 * Created by Josh on 10/10/15.
 */
@Path("/newsviews")
public class Resource {

    private SecureRandom random = new SecureRandom();
    private String generateCode() {
        String code = new BigInteger(130, random).toString(32);
        code = code.substring(0, 5) + "-" + code.substring(5, 10)
                + "-" + code.substring(10, 15)
                + "-" + code.substring(15, 20)
                + "-" + code.substring(20);
        return code.toUpperCase();
    }

    @POST
    @Path("/generateCode")
    @Produces("text/plain")
    public Response code() {
        String code = generateCode();
        while (Server.turkKeys.containsKey(code)) {
            code = generateCode();
        }

        Logger.Log("Generating code");
        Server.turkKeys.put(code, code);

        String line[] = {
                new Date().toString(),
                code
        };
        try {
            Server.codeLogger.writeNext(line);
            Server.codeLogger.flush();
        } catch (Exception e) {
            Logger.Log(e);
        }

        return Response.ok(code).build();
    }

    @POST
    @Path("/codeCheck")
    @Consumes("text/plain")
    @Produces("application/json")
    public Response checkCode(String code) {
        JSONObject result = new JSONObject();

        Logger.Log("Checking code");

        result.put("success", Server.turkKeys.containsKey(code));

        return Response.ok(result.toString()).build();
    }

    @GET
    @Path("/nextId")
    @Produces("text/plain")
    public Response getExperimentId() {
        final int interestingIds[] = {
                // 15, 22, 40, 33, 34, 1, 31, 26, 2
                1, 2
        };

        int min = 0;
        int max = interestingIds.length - 1;

        if (Server.currentID < min) {
            Server.currentID = min;
        }

        int currentId = Server.currentID;
        Server.currentID++;
        if (Server.currentID > max) {
            Server.currentID = min;
        }

        return Response.ok(interestingIds[currentId]).build();
    }

    @GET
    @Path("/helloworld")
    @Produces("text/plain")
    public Response textReponse() {
        return Response.ok("Hello World").build();
    }

    @GET
    @Path("/counties")
    @Produces("text/plain")
    public Response countiesReponse() {
        StringBuilder response = new StringBuilder();
        try {
            CSVReader reader = new CSVReader(new FileReader("counties.csv"));
            String[] line;
            while ((line = reader.readNext()) != null) {
                String state = line[0];
                FIPS fips = new FIPS(line[1] + line[2]);
                String county = line[3];
                county = county + ", " + state;

                response.append("'<option value=\"" + fips + "\">" + county + "</option>' +\n");
            }
        } catch (Exception e) {
            Logger.Log(e);
        }

        return Response.ok(response.toString()).build();
    }

    static BoilerpipeExtractor extractor = CommonExtractors.ARTICLE_EXTRACTOR;

    private final String ReadabilityAPIKey = "042e3228d910e85f442d5a00e95268ff71ccdd28";
    private final String ReadabilityBaseURL = "https://readability.com/api/content/v1";
    private class DownloadedPage {
        public String content;
        public String date;
        public String title;
    }
    private DownloadedPage extractHTMLPageText(String urlString) {
        DownloadedPage content = new DownloadedPage();
        try {
            // Check if the url is a NYTimes URL, then use the NYTimes API (I guess readability doesn't work with it :/
            try {
                URL url = new URL(urlString);
                if (url.getHost().equals("www.nytimes.com")) {
                    // A NYTimes URL
                    String[] components = url.getFile().split("/");
                    if (components.length > 0) {
                        String title = components[components.length - 1];
                        title = title.replaceFirst("\\?(.*)", "");
                        title = title.replace(".html", "");
                        List<NYTimesAnnotator.NYTimesArticle> articles = NYTimesAnnotator.getPossibleNYTimesArticles(Arrays.asList(title.split("-")));
                        if (articles.size() > 0) {
                            // Found a valid article
                            NYTimesAnnotator.NYTimesArticle article = articles.get(0);
                            Date publishDate = article.publishedDate;
                            if (publishDate == null) {
                                publishDate = new Date();
                            }
                            content.date = DateFormatter.format(publishDate);
                            content.content = article.leadPharagraph;
                            content.title = article.title;
                            return content;
                        }
                    }
                }
            } catch (Exception e) {
                Logger.Log(e);
            }

            String readabilityURL = ReadabilityBaseURL + "/parser?token=" + ReadabilityAPIKey + "&url=" + urlString;
            JSONObject jsonObject = new JSONObject(Networking.downloadFile(readabilityURL).content);
            content.content = extractor.getText(jsonObject.getString("content"));

            Date publishedDate = new Date();
            if (jsonObject.has("date_published") && !jsonObject.isNull("date_published")) {
                try {
                    String dateString = jsonObject.getString("date_published");
                    publishedDate = Chronic.parse(dateString).getBeginCalendar().getTime();
                } catch (Exception e) {
                    Logger.Log(e);
                }
            }
            content.date = DateFormatter.format(publishedDate);
            content.title = jsonObject.getString("title");
        } catch (Exception e) {
            Logger.Log(e);
        }

        return content;
    }

    @POST
    @Path("/wikibrain")
    @Consumes("text/plain")
    @Produces("application/json")
    public Response wikibrain(String jsonString) {
        String query = new JSONObject(jsonString).getString("query");
        List<String> arguments = new ArrayList<String>();
        Logger.Log("Wikibrain request: " + query);

        try {
            int index = query.indexOf('?');
            if (index >= 0) {
                String originalQuery = query;
                query = query.substring(0, index);

                for (String pair : originalQuery.substring(index + 1).split("&")) {
                    for (String item : pair.split("=")) {
                        arguments.add(item);
                    }
                }
            }
        } catch (Exception e) {
            Logger.Log(e);
            e.printStackTrace();
        }

        String wikibrainURL = Wikibrain.WikibrainBaseURL + "/" + query;
        Logger.Log("Downloading: " + wikibrainURL);

        Networking.NetworkingResult result = Networking.PostRequest(wikibrainURL, arguments.toArray(new String[arguments.size()]));

        Logger.Log("Finished downloading file");

        return Response.ok(result.content).build();
    }

    @GET
    @Path("/experiment")
    @Produces("text/plain")
    public Response getExperimentArticle() {
        String pageID = "";

        Logger.Log("Get experiment ID");



        try {
            FileReader fileReader = new FileReader("Visualization Corpus.csv");
            CSVReader reader = new CSVReader(fileReader);

            List<String[]> allLines = reader.readAll();
            int index = (int)(allLines.size() * Math.random());

            pageID = allLines.get(index)[0];
            reader.close();

            // Make sure the file has data (A few of the articles don't have associated data)
            // Otherwise, recursively return a new article
            File file = new File("./articles/" + pageID + ".json");
            if (file.length() == 0) {
                return  getExperimentArticle();
            }
        } catch (Exception e) {
            Logger.Log(e);
        }
        return Response.ok(pageID).build();
    }

    @POST
    @Path("/experimentExtract")
    @Consumes("text/plain")
    @Produces("application/json")
    public Response extractExperimentPage(String jsonString) {
        String pageID = new JSONObject(jsonString).getString("pageID");
        String text = "";

        Logger.Log("Text data for id: " + pageID);

        try {
            FileReader fileReader = new FileReader("Visualization Corpus.csv");
            CSVReader reader = new CSVReader(fileReader);
            String[] strings = reader.readNext(); // This should be the header
            while ((strings = reader.readNext()) != null) {
                if (Integer.parseInt(strings[0]) == Integer.parseInt(pageID)) {
                    break;
                }
            }

            if (strings == null) {
                Response.serverError().build();
            }

            reader.close();

            BufferedReader textReader = new BufferedReader(new FileReader("./articles/" + pageID + "-text.json"));
            String line = null;
            while ((line = textReader.readLine()) != null) {
                text += line;
            }

            JSONObject result = new JSONObject(text);

            result.put("question", strings[5]);
            result.put("answer", strings[6]);
            result.put("option-1", strings[7]);
            result.put("option-2", strings[8]);
            result.put("option-3", strings[9]);

            text = result.toString();
        } catch (Exception e) {
            Logger.Log(e);
        }

        return Response.ok(text).build();
    }

    @POST
    @Path("/experimentImages")
    @Consumes("text/plain")
    @Produces("application/json")
    public Response loadExperimentImages(String jsonString) {
        String id = new JSONObject(jsonString).getString("pageID");

        Logger.Log("Image data for id: " + id);

        StringBuilder data = new StringBuilder();
        try {
            FileReader reader = new FileReader("./articles/" + id + ".json");
            BufferedReader bufferedReader = new BufferedReader(reader);

            String line = "";
            while ((line = bufferedReader.readLine()) != null) {
                data.append(line);
            }

            bufferedReader.close();
        } catch (Exception e) {
            Logger.Log(e);
        }

        return Response.ok(data.toString()).build();
    }


    @POST
    @Path("/logging")
    @Consumes("text/plain")
    @Produces("application/json")
    public Response processLogQuery(String jsonString) throws Exception {
        try {
        JSONObject object = new JSONObject(jsonString);

        Logger.Log("Received Log request " + object.getString("type"));

        /*
            "userId": userID,
            "type": type,
            "data": data,
            "browser": window.navigator.userAgent.toLowerCase(),
            "language": getLanguage(),
            "ipAddress": ip,
            "ipLat": iplat.toString(),
            "ipLon": iplon.toString(),
            "ipOrg": iporg,
            "ipCountry": ipcountry,
            "ipCity": ipcity
         */

            String line[] = {
                    object.getString("userId"),
                    object.getString("type"),
                    new Date().toString(),
                    object.get("data").toString(),
                    object.getString("time")
            };

            Server.logWritter.writeNext(line);
            Server.logWritter.flush();
        } catch (Exception e) {
            Logger.Log(e);
        }

        return Response.ok("").build();
    }

    public void generateResearchArticles() {
        try {
            FileReader fileReader = new FileReader("Visualization Corpus.csv");
            CSVReader reader = new CSVReader(fileReader);

            reader.readNext();
            String line[];
            while ((line = reader.readNext()) != null) {
                String id = line[0];
                String url = line[4];

                Logger.Log("Processing Article " + id);
                if (new File("./articles/" + id + ".json").exists()) {
                    Logger.Log("Skipping Article " + id);
                    continue;
                }

                String text;
                File textFile = new File("./articles/" + id + "-text.json");
                if (textFile.exists()) {
                    text = new JSONObject(FileUtils.readFileToString(textFile)).getString("content");
                } else  {
                    DownloadedPage downloadedPage = extractHTMLPageText(url);
                    text = downloadedPage.content;

                    // Save text
                    JSONObject textJSON = new JSONObject();
                    textJSON.put("content", downloadedPage.content);
                    textJSON.put("title", downloadedPage.title);
                    textJSON.put("date", downloadedPage.date);

                    FileWriter textWriter = new FileWriter("./articles/" + id + "-text.json");
                    textWriter.write(textJSON.toString());
                    textWriter.close();
                }


                String wikibrainURL = Wikibrain.WikibrainBaseURL + "/images";
                Logger.Log("Downloading images");
                Networking.NetworkingResult result = Networking.PostRequest(wikibrainURL, "lang", "en", "method", "all", "text", text);
                Logger.Log("Writing to disk");

                new File("./articles").mkdirs();

                // Store data
                if (result.content.length() > 0) {
                    FileWriter output = new FileWriter("./articles/" + id + ".json");
                    output.write(result.content);
                    output.close();
                }
            }

            reader.close();

            Logger.Log("Done!");
        } catch (Exception e) {
            Logger.Log(e);
        }
    }


    @POST
    @Path("/save-document")
    @Consumes("text/plain")
    public Response saveAnnotations(String jsonString) {
        JSONObject data = new JSONObject(jsonString);
        try {
            CSVWriter writer = new CSVWriter(new FileWriter("comparisons.csv", true));
            String[] line = new String[3];
            line[0] = data.getString("name");
            line[1] = (new Date()).toString();
            line[2] = data.getString("data");
            writer.writeNext(line);
            writer.close();
        } catch (Exception e) {
            Logger.Log(e);
        }

        return Response.accepted().build();
    }

    @POST
    @Path("/tag")
    @Consumes("text/plain")
    @Produces("application/json")
    public Response tagText(String jsonString) {
        JSONObject json = new JSONObject(jsonString);
        String article =  json.getString("article");
        String urlString =  json.getString("url");
        String dateString = json.getString("date");
        Boolean usePMI = json.getBoolean("usePMI");

        Date articleDate = null;
        try {
            articleDate = Chronic.parse(dateString).getBeginCalendar().getTime();
        } catch (Exception e) {
            // Logger.Log(e);
        }

        if (urlString.length() > 0) {
            // Extract from the URL
            DownloadedPage page = extractHTMLPageText(urlString);
            article = page.content;
            try {
                articleDate = Chronic.parse(page.date).getBeginCalendar().getTime();
            } catch (Exception e) {
                Logger.Log(e);
            }
        }

        if (article == null) {
            article = "";
        }
        if (articleDate == null) {
            articleDate = new Date();
        }

        // Load the query extractor
        Wikibrain wikibrain = new Wikibrain("simple", false);
        QueryExtractor queryExtractor = new QueryExtractor(wikibrain, true, 1.0, false, 1.0, QueryExtractor.WeightingFunction.LINEAR,
                1.0, 0.0001, 0.05, true, true, QueryExtractor.NormalizationFunction.DYNAMIC_RANGE,
                QueryExtractor.NormalizationFunction.DYNAMIC_RANGE, 0, 10, 1.0, 0.0001, 0.05, 1.0, QueryExtractor.ExtractionTechnique.WIKIDATA,
                QueryExtractor.ExtractionTechnique.STANFORD, QueryExtractor.ExtractionTechnique.STANFORD);

        List<QueryExtractor.AnnotationRange> annotations = new ArrayList<QueryExtractor.AnnotationRange>();

        /*
        Pattern wordsPattern = Pattern.compile("(\\w+\\s)*(\\w+)");
        Matcher matcher = wordsPattern.matcher(article);
        while (matcher.find()) {
            int startLocation = matcher.start();
            String text = matcher.group();

            for (QueryExtractor.AnnotationRange range : queryExtractor.annotateText(text, articleDate)) {
                QueryExtractor.AnnotationRange newRange = new QueryExtractor.AnnotationRange(range.originalText,
                        range.resolvedText, range.type, range.score, range.startLocation() + startLocation, range.length());
                annotations.add(newRange);
            }
        }*/

        /*
        int textOffset = 0;
        for (String paragraph : article.split("\n")) {
            for (QueryExtractor.AnnotationRange r : queryExtractor.annotateText(paragraph, articleDate)) {
                QueryExtractor.AnnotationRange newRange = new QueryExtractor.AnnotationRange(r.originalText, r.resolvedText,
                        r.type, r.score, r.startLocation() + textOffset, r.length());
                annotations.add(newRange);
            }


            textOffset += paragraph.length();
            textOffset += 1; // For the newline
        }*/

        for (QueryExtractor.AnnotationRange range : queryExtractor.annotateText(article, articleDate)) {
            // QueryExtractor.AnnotationRange newRange = new QueryExtractor.AnnotationRange(range.originalText,
            //         range.resolvedText, range.type, range.score, range.startLocation() + startLocation, range.length());
            annotations.add(range);
        }

        // Send the results bad to the system
        JSONArray result = new JSONArray();
        for (QueryExtractor.AnnotationRange annotation : annotations) {
            JSONObject object = new JSONObject();

            object.put("start", annotation.startLocation());
            object.put("end", annotation.endLocation());
            object.put("type", annotation.type.toString());
            object.put("text", annotation.originalText);
            object.put("resolved-text", annotation.resolvedText);

            result.put(object);
        }

        return Response.ok(result.toString()).build();
    }

    private String referenceMapVariable = "Reference Map";

    @POST
    @Path("/find-variables")
    @Consumes("text/plain")
    @Produces("application/json")
    public Response findVariables(String jsonString) {
        JSONObject jsonObject = new JSONObject(jsonString);
        JSONArray jsonKeywords = jsonObject.getJSONArray("keywords");
        Boolean usePMI = jsonObject.getBoolean("usePMI");

        Keywords keywords = new Keywords();

        for (int i = 0; i < jsonKeywords.length(); i++) {
            keywords.addKeyword(jsonKeywords.getString(i), Keywords.KeywordType.OTHER);
        }

        Wikibrain wikibrain = new Wikibrain("simple", false);
        QueryExtractor queryExtractor = new QueryExtractor(wikibrain, true, 1.0, true, 1.0, QueryExtractor.WeightingFunction.LINEAR,
                1.0, 0.0001, 0.05, true, true, QueryExtractor.NormalizationFunction.DYNAMIC_RANGE,
                QueryExtractor.NormalizationFunction.DYNAMIC_RANGE, 0, 10, 1.0, 0.0001, 0.05, 1.0, QueryExtractor.ExtractionTechnique.WIKIDATA,
                QueryExtractor.ExtractionTechnique.STANFORD, QueryExtractor.ExtractionTechnique.STANFORD);
        TermSimilarity termSimilarity = new TermSimilarity(wikibrain, usePMI, 1.0, !usePMI, 1.0);

        TableDataSource datasource = new CensusTableDataSource(3, queryExtractor, termSimilarity, 0.0001f, 0.01f);
        JSONArray result = new JSONArray();

        result.put(referenceMapVariable);

        try {
            List<String> variables = datasource.rankVariables(keywords);

            int maxVariables = variables.size(); // Originally limitied to 15;
            for (String variable : variables) {
                if (maxVariables <= 0) {
                    break;
                }

                result.put(datasource.displayNameForVariable(variable));
                maxVariables--;
            }
        } catch (Exception e) {
            Logger.Log(e);
        }

        return Response.ok(result.toString()).build();
    }

    @POST
    @Path("/variable-data")
    @Consumes("text/plain")
    @Produces("application/json")
    public Response getDataForVariable(String jsonString) {
        JSONObject jsonObject = new JSONObject(jsonString);
        String displayName = jsonObject.getString("variable");
        Boolean usePMI = jsonObject.getBoolean("usePMI");

        Wikibrain wikibrain = new Wikibrain("simple", false);
        QueryExtractor queryExtractor = new QueryExtractor(wikibrain, true, 1.0, true, 1.0, QueryExtractor.WeightingFunction.LINEAR,
                1.0, 0.0001, 0.05, true, true, QueryExtractor.NormalizationFunction.DYNAMIC_RANGE,
                QueryExtractor.NormalizationFunction.DYNAMIC_RANGE, 0, 10, 1.0, 0.0001, 0.05, 1.0, QueryExtractor.ExtractionTechnique.WIKIDATA,
                QueryExtractor.ExtractionTechnique.STANFORD, QueryExtractor.ExtractionTechnique.STANFORD);
        TermSimilarity termSimilarity = new TermSimilarity(wikibrain, usePMI, 1.0, !usePMI, 1.0);

        JSONArray result = new JSONArray();

        if (displayName.equals(referenceMapVariable)) {
            // Create an empty reference map
            for (FIPS fips : FIPS.allFIPSCodes()) {
                JSONObject datapoint = new JSONObject();
                String location = fips.entityName();
                if (location == null) {
                    continue;
                }

                datapoint.put("loc", location);
                datapoint.put("value", 0.0);
                result.put(datapoint);
            }
        } else {
            TableDataSource datasource = new CensusTableDataSource(3, queryExtractor, termSimilarity, 0.0001f, 0.01f);
            String variable = datasource.variableForDisplayName(displayName);
            List<Map<String, String>> table = datasource.readTableForVariable(variable);
            List<Integer> years = datasource.getYearsFromTable(table);
            if (years.size() > 0) {
                Map<FIPS, Double> values = datasource.getValuesFromTable(table, years.get(0));

                for (FIPS fips : values.keySet()) {
                    JSONObject datapoint = new JSONObject();
                    String location = fips.entityName();
                    if (location == null) {
                        continue;
                    }

                    datapoint.put("loc", location);
                    datapoint.put("value", values.get(fips));
                    result.put(datapoint);
                }
            }
        }

        return Response.ok(result.toString()).build();
    }

    @POST
    @Path("/convert-data")
    @Consumes("text/plain")
    @Produces("application/json")
    public Response convertToVisualization(String jsonString) {
        JSONObject json = new JSONObject(jsonString);
        String title = json.getString("variable");
        Boolean usePMI = json.getBoolean("usePMI");

        List<FIPS> fips = new ArrayList<FIPS>();
        List<Double> values = new ArrayList<Double>();

        JSONArray data = json.getJSONArray("data");
        for (int i = 0; i < data.length(); i++) {
            JSONObject dataPoint = data.getJSONObject(i);
            FIPS fipsCode = FIPS.FIPSFromName(dataPoint.getString("loc"));
            if (fipsCode == null) {
                Logger.Log("Unable to resolve " + dataPoint.getString("loc"));
                continue;
            }

            fips.add(fipsCode);
            values.add(Double.parseDouble(dataPoint.getString("value")));
        }

        VisualizationData visualization = VisualizationData.createMapVisualizaion(fips, values, title);

        // TODO: Use the original text to rank all of the keyworks for better annotations
        String text = json.getString("article");

        // Get the keywords
        Keywords keywords = new Keywords();
        JSONArray locations = json.getJSONArray("locations");
        for (int i = 0; i < locations.length(); i++) {
            keywords.addKeyword(locations.getString(i), Keywords.KeywordType.LOCATION);
        }

        JSONArray dates = json.getJSONArray("dates");
        for (int i = 0; i < dates.length(); i++) {
            keywords.addKeyword(dates.getString(i), Keywords.KeywordType.DATE);
        }

        JSONArray nouns = json.getJSONArray("nouns");
        for (int i = 0; i < nouns.length(); i++) {
            keywords.addKeyword(nouns.getString(i), Keywords.KeywordType.OTHER);
        }

        // Annotate
        AnnotationGenerator generator = new AnnotationGenerator();
        Wikibrain wikibrain = new Wikibrain("simple", false);
        QueryExtractor queryExtractor = new QueryExtractor(wikibrain, true, 1.0, true, 1.0, QueryExtractor.WeightingFunction.LINEAR,
                1.0, 0.0001, 0.05, true, true, QueryExtractor.NormalizationFunction.DYNAMIC_RANGE,
                QueryExtractor.NormalizationFunction.DYNAMIC_RANGE, 0, 10, 1.0, 0.0001, 0.05, 1.0, QueryExtractor.ExtractionTechnique.WIKIDATA,
                QueryExtractor.ExtractionTechnique.STANFORD, QueryExtractor.ExtractionTechnique.STANFORD);

        generator.addAnnotator(new LocationAnnotator(wikibrain));
        generator.addAnnotator(new MinMaxAnnotator());
        generator.addAnnotator(new NYTimesAnnotator(wikibrain, queryExtractor, 3));
        generator.addAnnotator(new ReutersAnnotator(wikibrain, 3));

        generator.annotateVisualization(visualization, keywords);

        return Response.ok(visualization.toJson(false).toString()).build();
    }


    @POST
    @Path("/passing-comparisons")
    @Consumes("text/plain")
    @Produces("application/json")
    public Response determinePassingComparisons(String jsonString) {
        JSONObject json = new JSONObject(jsonString);
        JSONArray array = json.getJSONArray("comparisons");
        JSONObject map = json.getJSONObject("map");
        JSONObject result = new JSONObject();

        try {
            Map<FIPS, Double> values = new HashMap<FIPS, Double>();
            for (String key : map.keySet()) {
                FIPS fips = new FIPS(key);
                Double value = map.getDouble(key);
                values.put(fips, value);
            }

            ComparisonChecker comparisonChecker = new ComparisonChecker();
            List<Boolean> passingComparisons = comparisonChecker.passingComparisons(array, values);

            result.put("passing", passingComparisons);
        } catch (Exception e) {
            Logger.Log(e);
        }

        return Response.ok(result.toString()).build();
    }

    @POST
    @Path("/find-fips")
    @Consumes("text/plain")
    @Produces("application/json")
    public Response resolvePlacesToFipsCodes(String jsonString) {
        JSONObject json = new JSONObject(jsonString);
        JSONArray result = new JSONArray();

        JSONArray data = json.getJSONArray("data");
        for (int i = 0; i < data.length(); i++) {
            String location = data.getString(i);
            FIPS fipsCode = FIPS.FIPSFromName(location);
            if (fipsCode == null) {
                Logger.Log("Unable to resolve " + location);
                continue;
            }

            result.put(fipsCode.toString());
        }

        return Response.ok(result.toString()).build();
    }

    @POST
    @Path("/find-coords")
    @Consumes("text/plain")
    @Produces("application/json")
    public Response resolvePlacesToCoordinates(String jsonString) {
        JSONObject json = new JSONObject(jsonString);
        JSONArray result = new JSONArray();

        Wikibrain wikibrain = new Wikibrain("simple", false);
        Spatial spatial = new Spatial(wikibrain);

        JSONArray data = json.getJSONArray("data");
        for (int i = 0; i < data.length(); i++) {
            String locationName = data.getString(i);
            Spatial.Location location = spatial.getCoordinatesForArticle(locationName);
            if (location == null) {
                Logger.Log("Unable to resolve " + locationName);
                continue;
            }

            result.put(location.toList());
        }

        return Response.ok(result.toString()).build();
    }

    @POST
    @Path("/extract")
    @Consumes("text/plain")
    @Produces("application/json")
    public Response extractPage(String jsonString) {
        DownloadedPage page = extractHTMLPageText(new JSONObject(jsonString).getString("url"));

        JSONObject result = new JSONObject();
        result.put("content", page.content);
        result.put("date", page.date);
        result.put("title", page.title);

        return Response.ok(result.toString()).build();
    }

    @GET
    @Path("wikifyTest/{text}")
    @Consumes("text/plain")
    @Produces("application/json")
    public Response wikificationTest(@PathParam("text") String test) {
        StringBuilder result = new StringBuilder();

        test = test.replaceAll("_", " ");
        result.append(test);
        result.append("\n\n");

        Wikibrain local = new Wikibrain("simple", false);
        result.append("Local, simple:\n");
        for (Wikibrain.QueryLocalLink link : local.wikify(test)) {
            result.append("\t");
            result.append(link.title);
            result.append("\n");
        }

        result.append("\n\n");

        Wikibrain en = new Wikibrain("en", true);
        result.append("Web, En:\n");
        for (Wikibrain.QueryLocalLink link : en.wikify(test)) {
            result.append("\t");
            result.append(link.title);
            result.append("\n");
        }

        result.append("\n\n");

        return Response.ok(result.toString()).build();
    }

    @POST
    @Path("/visualize")
    @Consumes("text/plain")
    @Produces("application/json")
    public Response generateVisualization(String jsonString) {
        JSONObject json = new JSONObject(jsonString);
        String article =  json.getString("article");
        String urlString =  json.getString("url");
        String dateString = json.getString("date");
        JSONObject options = json.getJSONObject("options");

        Date articleDate = null;
        try {
            articleDate = Chronic.parse(dateString).getBeginCalendar().getTime();
        } catch (Exception e) {
            Logger.Log(e);
        }

        if (urlString.length() > 0) {
            // Extract from the URL
            DownloadedPage page = extractHTMLPageText(urlString);
            article = page.content;
            try {
                articleDate = Chronic.parse(page.date).getBeginCalendar().getTime();
            } catch (Exception e) {
                Logger.Log(e);
            }
        }
        if (article == null) {
            article = "";
        }
        if (articleDate == null) {
            articleDate = new Date();
        }

        // Get the first three sentences
        final int maxNumberOfSentencesToUse = 3;
        String text = "";
        DocumentPreprocessor documentPreprocessor = new DocumentPreprocessor(new StringReader(article));
        int numberOfSentencesProcessed = 0;
        for (List<HasWord> sentence : documentPreprocessor) {
            if (text.length() > 0) {
                text += " ";
            }
            text += Sentence.listToString(sentence);

            numberOfSentencesProcessed++;
            if (numberOfSentencesProcessed >= maxNumberOfSentencesToUse) {
                break;
            }
        }

        // Load our wikibrain instance
        Wikibrain wikibrain = new Wikibrain(options.getString("wikifier-lang"), options.getBoolean("wikifier-web"));
        TermSimilarity termSimilarity = new TermSimilarity(wikibrain, options.getBoolean("term-pmi-enable"),
                options.getDouble("term-pmi-scale"), options.getBoolean("term-sr-enable"), options.getDouble("term-sr-scale"));
        QueryExtractor queryExtractor = new QueryExtractor(wikibrain, options.getBoolean("wikifier-enable"), options.getDouble("wikifier-weight"),
                options.getBoolean("nlp-enable"), options.getDouble("nlp-weight"), QueryExtractor.WeightingFunction.fromString(options.getString("weight-func")),
                options.getDouble("weight-exp"), options.getDouble("term-freq-min"), options.getDouble("term-freq-max"),
                options.getBoolean("score-use-link"), options.getBoolean("score-use-term"), QueryExtractor.NormalizationFunction.fromString("score-link-norm"),
                QueryExtractor.NormalizationFunction.fromString("score-term-norm"), options.getDouble("score-link-min"),
                options.getDouble("score-link-max"), options.getDouble("score-link-scale"), options.getDouble("score-term-min"),
                options.getDouble("score-term-max"), options.getDouble("score-term-scale"), QueryExtractor.ExtractionTechnique.fromString(options.getString("extractor-loc")),
                QueryExtractor.ExtractionTechnique.fromString(options.getString("extractor-num")), QueryExtractor.ExtractionTechnique.fromString(options.getString("extractor-date")));

        // Create and configure the database
        Database database = new Database();
        if (options.getBoolean("census-enable")) {
            // Add census data
            database.addDatasource(new CensusTableDataSource(options.getInt("census-count"), queryExtractor, termSimilarity,
                    (float)options.getDouble("census-min-freq"), (float)options.getDouble("census-max-freq")));
        }
        if (options.getBoolean("stock-enable")) {
            // Add the stock data source
            database.addDatasource(new StockDataSource());
        }
        if (options.getBoolean("atlasify-enable")) {
            // Add the atlasify data source
            database.addDatasource(new AtlasifySource(options.getInt("atlasify-count")));
        }

        // Create and configure the annotator
        AnnotationGenerator annotationGenerator = new AnnotationGenerator();
        if (options.getBoolean("reutersAn-enable")) {
            // Add the reuters annotator
            annotationGenerator.addAnnotator(new ReutersAnnotator(wikibrain, options.getInt("reutersAn-count")));
        }
        if (options.getBoolean("nytimesAn-enable")) {
            // Add the nytimes annotator
            annotationGenerator.addAnnotator(new NYTimesAnnotator(wikibrain, queryExtractor, options.getInt("nytimesAn-count")));
        }
        if (options.getBoolean("mentionAn-enable")) {
            // Add the location annotator
            annotationGenerator.addAnnotator(new LocationAnnotator(wikibrain));
        }
        if (options.getBoolean("minMaxAn-enable")) {
            // Add the min/max annotator
            annotationGenerator.addAnnotator(new MinMaxAnnotator());
        }

        // Create the visualization ranker
        VisualizationRanker visualizationRanker = new VisualizationRanker(termSimilarity, (float)options.getDouble("viz-rank-min-score"));

        boolean debugEnabled = options.getBoolean("debug-enable");

        // Perform Wikification and then generate the visualizations
        Keywords keywords = queryExtractor.extractKeywords(text, articleDate);
        List<VisualizationData> visualizations = database.generateVisualization(text, keywords);

        // Remove any visualizations that contain no interesting data
        for (int i = 0; i < visualizations.size(); i++) {
            VisualizationData data = visualizations.get(i);

            boolean removeVisualization = true;
            double value = 0.0;

            if (data.values.size() > 0) {
                value = data.values.get(0);
            }

            for (int j = 1; j < data.values.size(); j++) {
                if (data.values.get(j) != value) {
                    removeVisualization = false;
                    break;
                }
            }

            if (removeVisualization) {
                visualizations.remove(i);
                i--;
            }
        }

        // Annotate the visualizations
        annotationGenerator.annotateVisualizations(visualizations, keywords);

        // Rank the visualizations
        visualizations = visualizationRanker.rankVisualizations(visualizations, keywords);

        // Combine the visualizations into JSON
        JSONArray jsonVisualizations = new JSONArray();
        for (int i = 0; i < visualizations.size(); i++) {
            jsonVisualizations.put(visualizations.get(i).toJson(debugEnabled));
        }

        // Extract the noun phrases and tag them to display in the front-end UI
        List<String> nounPhrases = keywords.phraseDescriptions();

        JSONObject result = new JSONObject();
        result.put("noun-phrases", nounPhrases);
        result.put("visualizations", jsonVisualizations);

        return Response.ok(result.toString()).build();
    }
}
