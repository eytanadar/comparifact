package org.newsviews;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Josh on 4/29/16.
 */
public class ImageId {
    static class UrlImageId extends ImageId {
        public String url;
        UrlImageId(String url) {
            this.url = url;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj instanceof UrlImageId) {
                return url.equals(((UrlImageId) obj).url);
            }

            return false;
        }

        @Override
        public int hashCode() {
            return url.hashCode();
        }
    }

    static class DataImageId extends ImageId  {
        public String data;
        DataImageId(String data) {
            this.data = data;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj instanceof DataImageId) {
                return data.equals(((DataImageId) obj).data);
            }

            return false;
        }

        @Override
        public int hashCode() {
            return data.hashCode();
        }
    }

    static class RefMapImageId extends ImageId  {
        static class Annotation {
            public String title;
            public double lat;
            public double lng;

            Annotation(String title, double lat, double lng) {
                this.title = title;
                this.lat = lat;
                this.lng = lng;
            }

            @Override
            public boolean equals(Object obj) {
                if (obj instanceof Annotation) {
                    return  title.equals(((Annotation) obj).title) &&
                            lat == ((Annotation) obj).lat &&
                            lng == ((Annotation) obj).lng;
                }

                return false;
            }

            @Override
            public int hashCode() {
                return title.hashCode() * 31 + ((int)lat ^ (int)lng);
            }
        }

        public double neLng;
        public double swLng;
        public double neLat;
        public double swLat;
        public List<String> styles;
        public List<Annotation> annotations;
        RefMapImageId(double neLng, double swLng, double neLat, double swLat, List<String> styles, List<Annotation> annotations) {
            this.neLng = neLng;
            this.swLng = swLng;
            this.neLat = neLat;
            this.swLat = swLat;
            this.styles = styles;
            this.annotations = annotations;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj instanceof RefMapImageId) {
                RefMapImageId map = (RefMapImageId)obj;
                return neLng == map.neLng &&
                        swLng == map.swLng &&
                        neLat == map.neLat &&
                        swLat == map.swLat &&
                        styles.equals(map.styles) &&
                        annotations.equals(map.annotations);
            }

            return false;
        }

        @Override
        public int hashCode() {
            return Double.hashCode(neLat) ^ Double.hashCode(swLng) ^ Double.hashCode(neLat) ^ Double.hashCode(swLat)
                    ^ styles.hashCode() ^ annotations.hashCode();
        }
    }

    static ImageId fromJSON(JSONObject object) {
        if (object.has("url")) {
            return new UrlImageId(object.getString("url"));
        } else if (object.has("data")) {
            return new DataImageId(object.getString("data"));
        } else if (object.has("refMap")) {
            JSONObject map = object.getJSONObject("refMap");

            // Get the style from the json
            List<String> styles = new ArrayList<String>();
            JSONArray styleJson = map.getJSONArray("styles");
            for (int i = 0; i < styleJson.length(); i++) {
                styles.add(styleJson.getString(i));
            }

            // Get the annotations from the json
            List<RefMapImageId.Annotation> annotations = new ArrayList<RefMapImageId.Annotation>();
            JSONArray annotationsJson = map.getJSONArray("annotations");
            for (int i = 0; i < annotationsJson.length(); i++) {
                JSONObject annotationJson = annotationsJson.getJSONObject(i);
                RefMapImageId.Annotation annotation = new RefMapImageId.Annotation(annotationJson.getString("title"),
                        annotationJson.getDouble("lat"), annotationJson.getDouble("lng"));
                annotations.add(annotation);
            }

            return new RefMapImageId(map.getDouble("ne-lng"), map.getDouble("sw-lng"),
                    map.getDouble("ne-lat"), map.getDouble("sw-lat"),
                    styles, annotations);
        }

        return null;
    }
}
