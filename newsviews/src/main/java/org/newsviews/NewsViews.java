package org.newsviews;

import java.io.IOException;

/**
 * Created by Josh on 10/10/15.
 */
public class NewsViews {
    public final static boolean useLocalHost = false;
    private static String baseURL = useLocalHost ? "http://localhost" : "http://104.236.255.214";
    private static int portNumber = 8080;

    public static void main( String[] args ) throws IOException
    {
        /*
        Resource resource = new Resource();
        resource.generateResearchArticles();
        */

        Server server = new Server(baseURL, portNumber);
        server.startServer();
        System.in.read();
        server.stopServer();
        System.exit(0);
    }
}
