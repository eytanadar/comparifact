package org.newsviews.Reuters;

import edu.emory.mathcs.backport.java.util.Arrays;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.*;
import org.apache.lucene.index.*;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.*;
import org.apache.lucene.search.similarities.PerFieldSimilarityWrapper;
import org.apache.lucene.search.similarities.Similarity;
import org.apache.lucene.search.similarities.TFIDFSimilarity;
import org.apache.lucene.spatial.prefix.RecursivePrefixTreeStrategy;
import org.apache.lucene.spatial.prefix.tree.GeohashPrefixTree;
import org.apache.lucene.spatial.prefix.tree.SpatialPrefixTree;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;
import org.json.XML;
import org.newsviews.Utilities.DateFormatter;
import org.newsviews.Utilities.Logger;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.XMLReader;

import org.w3c.dom.Document;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.lucene.spatial.SpatialStrategy;

import com.spatial4j.core.context.SpatialContext;
import com.spatial4j.core.distance.DistanceUtils;
import com.spatial4j.core.shape.Point;
import com.spatial4j.core.shape.Shape;

/**
 * Created by Josh on 11/15/15.
 */
public class ReutersIndexer {
    private static final String FIELD_ID = "id";
    private static final String FIELD_HEADLINE = "headline";
    private static final String FIELD_ARTICLE = "contents";
    private static final String FIELD_PUBLISH_DATE = "date";
    private static final String FIELD_LOCATION = "location";

    private static final String reutersBaseDirectory = "reuters-vol1";
    private static final String outputDirectory = "lucene";
    public static void main( String[] args ) {
        try {
            loadIndexSearcherIfNeeded();
            String term = "Academic";
            List<ReutersArticle> articles = searchArticles(term, 10);
            double termFrequency = termFrequency(term);
            double inverseDocumentFrequency = inverseDocumentFrequency(term);

            System.exit(1);
        } catch (Exception e) {
            Logger.Log(e);
        }
        System.exit(1);


        try {
            spatialContext = SpatialContext.GEO;
            SpatialPrefixTree grid = new GeohashPrefixTree(spatialContext, 11);
            strategy = new RecursivePrefixTreeStrategy(grid, FIELD_LOCATION);

            Directory indexDir = FSDirectory.open(new File(outputDirectory));
            IndexWriterConfig config = new IndexWriterConfig(Version.LUCENE_43, new StandardAnalyzer(Version.LUCENE_43));
            indexWriter = new IndexWriter(indexDir, config);

            // indexSearcher = new IndexSearcher(DirectoryReader.open(FSDirectory.open(new File(outputDirectory))));
            // queryParser = new QueryParser(Version.LUCENE_43, "contents", new StandardAnalyzer(Version.LUCENE_43));

            documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            File baseDirectory = new File(reutersBaseDirectory);
            loadArticlesFromFolder(baseDirectory);
            processArticles();

            indexWriter.close();

            indexDir.close();

            Logger.Log("Done!");
        } catch (Exception e) {
            Logger.Log(e);
        }
    }

    public static class ReutersArticle {
        public int id = -1;
        public String headline = "";
        public String body = "";
        public Date publishDate = new Date();
        public String location = "";
    }

    private static List<ReutersArticle> articlesToProcess = new ArrayList<ReutersArticle>();
    private static DocumentBuilder documentBuilder;
    private static void loadArticlesFromFolder(File file) {
        File[] directoryListing = file.listFiles();
        if (directoryListing != null) {
            for (File f : directoryListing) {
                if (f.isDirectory()) {
                    loadArticlesFromFolder(f);
                } else {
                    String fileName = f.getName();
                    if (!fileName.endsWith(".xml")) {
                        continue;
                    }
                    try {
                        Document doc = documentBuilder.parse(f);
                        doc.normalizeDocument();

                        ReutersArticle article = getReutersArticle(doc);

                        articlesToProcess.add(article);
                        if (articlesToProcess.size() >= 1000) {
                            processArticles();
                        }
                    } catch (Exception e) {
                        Logger.Log(e);
                    }
                }
            }
        }
    }
    private static ReutersArticle getReutersArticle(Document doc) {
        ReutersArticle article = new ReutersArticle();
        Node document = doc.getDocumentElement().getFirstChild().getParentNode();

        article.id = Integer.parseInt(document.getAttributes().getNamedItem("itemid").getTextContent());

        NodeList childNodes = document.getChildNodes();
        for (int i = 0; i < childNodes.getLength(); i++) {
            Node item = childNodes.item(i);
            if (item.getNodeName().equals("headline")) {
                article.headline = item.getTextContent();
            } else if (item.getNodeName().equals("text")) {
                String contents = "";
                NodeList paragraphs = item.getChildNodes();
                for (int j = 0; j < paragraphs.getLength(); j++) {
                    String paragraph = paragraphs.item(j).getTextContent();
                    paragraph = paragraph.replace("&quot;", "\"");
                    contents += paragraph;
                }
                article.body = contents.trim();
            } else if (item.getNodeName().equals("metadata")) {
                NodeList metadata = item.getChildNodes();
                for (int j = 0; j < metadata.getLength(); j++) {
                    Node metadataItem = metadata.item(j);
                    if (metadataItem.getNodeName().equals("dc")) {
                        Node element = metadataItem.getAttributes().getNamedItem("element");
                        Node value = metadataItem.getAttributes().getNamedItem("value");
                        if (element.getNodeValue().equals("dc.date.published")) {
                            article.publishDate = DateFormatter.parse(value.getNodeValue());
                        } else if (element.getNodeValue().equals("dc.creator.location")) {
                            article.location = value.getNodeValue();
                        } else if (element.getNodeValue().equals("dc.creator.location.country.name")) {
                            article.location = article.location + ", " + value.getNodeValue();
                        }
                    }
                }
            }
        }
        return article;
    }

    private static long processedArticles = 0;
    private static IndexWriter indexWriter;
    private static IndexReader indexReader;
    private static TFIDFSimilarity similarity;
    private static SpatialContext spatialContext;
    private static SpatialStrategy strategy;
    private static IndexSearcher indexSearcher;
    private static QueryParser queryParser;
    private static void processArticles() {
        processedArticles += articlesToProcess.size();

        // Convert the location to coordinates
        /*List<String> locations = new ArrayList<String>();
        for (ReutersArticle article : articlesToProcess) {
            locations.add(article.location);
        }
        // Map<String, Geocoder.Coordinates> coordinatesMap = Geocoder.geocoderPlaces(locations);
        for (ReutersArticle article : articlesToProcess) {
            Geocoder.Coordinates coordinates = coordinatesMap.get(article.location);
            article.lat = coordinates.latitude;
            article.lng = coordinates.longitude;
        }*/

        // Store the articles in the lucene database
        for (ReutersArticle article : articlesToProcess) {
            indexArticle(article);
        }

        articlesToProcess.clear();
        Logger.Log("Articles processed " + processedArticles);
    }
    private static void loadIndexSearcherIfNeeded() {
        if (indexSearcher != null) {
            return;
        }

        try {
            queryParser = new QueryParser(Version.LUCENE_43, FIELD_ARTICLE, new StandardAnalyzer(Version.LUCENE_43));
            indexReader = IndexReader.open(DirectoryReader.open(FSDirectory.open(new File(outputDirectory))).directory());
            indexSearcher = new IndexSearcher(indexReader);
            similarity = asTFIDF(indexSearcher.getSimilarity(), FIELD_ARTICLE);
        } catch (Exception e) {
            Logger.Log(e);
        }
    }

    // Adapted from http://massapi.com/source/apache/13/51/1351014658/dist/lucene/pylucene/pylucene-4.8.0-1-src.tar.gz/pylucene-4.8.0-1-src.tar.gz.unzip/pylucene-4.8.0-1/lucene-java-4.8.0/lucene/queries/src/java/org/apache/lucene/queries/function/valuesource/IDFValueSource.java.html#56
    private static TFIDFSimilarity asTFIDF(Similarity sim, String field) {
        while (sim instanceof PerFieldSimilarityWrapper) {
            sim = ((PerFieldSimilarityWrapper)sim).get(field);
        }
        if (sim instanceof TFIDFSimilarity) {
            return (TFIDFSimilarity)sim;
        } else {
            return null;
        }
    }

    private static void indexArticle(ReutersArticle article) {
        try {
            org.apache.lucene.document.Document document = new org.apache.lucene.document.Document();
            document.add(new TextField(FIELD_HEADLINE, article.headline, Field.Store.YES));
            document.add(new Field(FIELD_PUBLISH_DATE, DateTools.dateToString(article.publishDate, DateTools.Resolution.DAY),
                    Field.Store.YES, Field.Index.NOT_ANALYZED));
            document.add(new TextField(FIELD_ARTICLE, article.body, Field.Store.YES));
            document.add(new IntField(FIELD_ID, article.id, Field.Store.YES));
            document.add(new TextField(FIELD_LOCATION, article.location, Field.Store.YES));

            indexWriter.addDocument(document);
        } catch (Exception e) {
            Logger.Log("ERROR Article with id " + article.id);
            Logger.Log(e);
        }
    }

    public static List<ReutersArticle> searchArticlesWithQuery(String queryString, int resultsCount) {
        loadIndexSearcherIfNeeded();

        List<ReutersArticle> articles = new ArrayList<ReutersArticle>();
        try {
            Query query = queryParser.parse(queryString);
            TopDocs topDocs = indexSearcher.search(query, resultsCount);

            ScoreDoc[] hits = topDocs.scoreDocs;
            for (int i = 0; i < hits.length; i++) {
                org.apache.lucene.document.Document doc = indexSearcher.doc(hits[i].doc);
                ReutersArticle article = new ReutersArticle();
                article.id = Integer.parseInt(doc.get(FIELD_ID));
                article.headline = doc.get(FIELD_HEADLINE);
                article.body = doc.get(FIELD_ARTICLE);
                article.publishDate = DateTools.stringToDate(doc.get(FIELD_PUBLISH_DATE));
                article.location = doc.get(FIELD_LOCATION);
                articles.add(article);
            }
        } catch (Exception e) {
            Logger.Log(e);
        }
        return articles;
    }

    public static List<ReutersArticle> searchArticles(List<String> searchTerms, int resultsCount) {
        loadIndexSearcherIfNeeded();

        List<ReutersArticle> articles = new ArrayList<ReutersArticle>();
        try {
            if (searchTerms.size() == 0) {
                return articles;
            }

            BooleanQuery booleanQuery = new BooleanQuery();
            for (String term : searchTerms) {
                booleanQuery.add(new TermQuery(new Term(FIELD_ARTICLE, term)), BooleanClause.Occur.MUST);
            }

            TopDocs topDocs = indexSearcher.search(booleanQuery, resultsCount);

            ScoreDoc[] hits = topDocs.scoreDocs;
            for (int i = 0; i < hits.length; i++) {
                org.apache.lucene.document.Document doc = indexSearcher.doc(hits[i].doc);
                ReutersArticle article = new ReutersArticle();
                article.id = Integer.parseInt(doc.get(FIELD_ID));
                article.headline = doc.get(FIELD_HEADLINE);
                article.body = doc.get(FIELD_ARTICLE);
                article.publishDate = DateTools.stringToDate(doc.get(FIELD_PUBLISH_DATE));
                article.location = doc.get(FIELD_LOCATION);
                articles.add(article);
            }
        } catch (Exception e) {
            Logger.Log(e);
        }
        return articles;
    }

    public static List<ReutersArticle> searchArticles(List<String> searchTerms) {
        return searchArticles(searchTerms, 10);
    }

    public static List<ReutersArticle> searchArticles(String searchTerm, int resultsCount) {
        return searchArticles(Arrays.asList(searchTerm.split("^[^ ]*\\s")), resultsCount);
    }

    public static List<ReutersArticle> searchArticles(String searchTerm) {
        return searchArticles(searchTerm, 10);
    }

    public static double termFrequency(List<String> searchTerms) {
        loadIndexSearcherIfNeeded();

        ArrayList<String> terms = new ArrayList<String>();
        for (String term : searchTerms) {
            if (term.length() > 0) {
                terms.add(term.toLowerCase());
            }
        }

        if (terms.size() == 0) {
            return 0.0;
        } /* else if (terms.size() == 1) {
            return termFrequency(terms.get(0));
        } */

        BooleanQuery booleanQuery = new BooleanQuery();
        for (String term : terms) {
            booleanQuery.add(new TermQuery(new Term(FIELD_ARTICLE, term)), BooleanClause.Occur.MUST);
        }

        try {
            TopDocs topDocs = indexSearcher.search(booleanQuery, 100);
            return (double)topDocs.totalHits / (double)indexReader.numDocs();
        } catch (Exception e) {
            Logger.Log(e);
        }

        return 0.0;
    }

    public static double termFrequency(String termString) {
        return termFrequency(Arrays.asList(termString.split("^[^ ]*\\s")));

        /*
        loadIndexSearcherIfNeeded();

        try {
            Term term = new Term(FIELD_ARTICLE, termString);
            int documents = indexReader.docFreq(term);
            long numDocs = indexReader.numDocs();
            return (double)documents / (double)numDocs;
        } catch (Exception e) {
            Logger.Log(e);
        }

        return 0.0;
        */
    }

    public static double inverseDocumentFrequency(String termString) {
        loadIndexSearcherIfNeeded();

        try {
            Term term = new Term(FIELD_ARTICLE, termString);
            long termFreq = indexReader.totalTermFreq(term);
            long numDocs = indexReader.numDocs();
            return similarity.idf(termFreq, numDocs);
        } catch (Exception e) {
            Logger.Log(e);
        }

        return 0.0;
    }
}
