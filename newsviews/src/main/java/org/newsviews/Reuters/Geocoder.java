package org.newsviews.Reuters;

import org.json.JSONArray;
import org.json.JSONObject;
import org.newsviews.Utilities.Logger;
import org.newsviews.Utilities.Networking;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Josh on 11/15/15.
 */
public class Geocoder {
    private static String MapQuestAPIKey = "b9zHpo01H5ZG89wUx8Gp3oalUYUeMHel";

    public static class Coordinates {
        public double latitude, longitude;
        Coordinates() {
            latitude = longitude = 0.0;
        }

        Coordinates(double latitude, double longitude) {
            this.latitude = latitude;
            this.longitude = longitude;
        }
    }

    // There is a limit of 100 places per call
    private static Map<String, Coordinates> geocoderPlaces(List<String> locations) {
        String url = "http://www.mapquestapi.com/geocoding/v1/batch?key=" + MapQuestAPIKey + "&outFormat=json";
        if (locations.size() > 100) {
            throw new RuntimeException("A maximum of 100 locations can be specified per call");
        }

        for (String location : locations) {
            url += "&location=" + URLEncoder.encode(location);
        }

        Map<String, Coordinates> coordinates = new HashMap<String, Coordinates>();
        try {
            Networking.NetworkingResult result = Networking.downloadFile(url);
            JSONObject json = new JSONObject(result.content);
            JSONArray  results = json.getJSONArray("results");
            for (int i = 0; i < results.length(); i++) {
                JSONObject latlng = results.getJSONObject(i).getJSONArray("locations").getJSONObject(0).getJSONObject("latLng");
                Coordinates coord = new Coordinates(latlng.getDouble("lat"), latlng.getDouble("lng"));
                coordinates.put(locations.get(i), coord);
            }

        } catch (Exception e) {
            Logger.Log(e);
        }
        return coordinates;
    }
}
