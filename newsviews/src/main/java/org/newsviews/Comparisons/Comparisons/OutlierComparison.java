package org.newsviews.Comparisons.Comparisons;

/**
 * Created by Josh on 3/17/16.
 */
public class OutlierComparison extends MoransComparison {

    public OutlierComparison() {
        super(-0.2, true);
    }

    public String comparisonType() { return "outlier"; }
}
