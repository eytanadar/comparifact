package org.newsviews.Comparisons.Comparisons;

import org.json.JSONArray;
import org.json.JSONObject;
import org.newsviews.DataStructures.FIPS;
import org.newsviews.Database.Tables.TableDataSource;
import org.newsviews.Utilities.DateFormatter;

import java.util.*;

/**
 * Created by Josh on 3/17/16.
 */
public class TrendComparison extends RawComparison {
    List<FIPS> locations;

    public TrendComparison() {
        locations = new ArrayList<FIPS>();
    }

    public String comparisonType() {
        return "trend";
    }

    public void loadFromJSON(JSONObject json) {
        // The super class should correctly parse the trend
        super.loadFromJSON(json);

        JSONArray loc = json.getJSONArray("locations");
        for (int i = 0; i < loc.length(); i++) {
            String fipsString = loc.getString(i);
            FIPS fips = FIPS.FIPSFromName(fipsString);
            locations.add(fips);
        }
    }

    @Override
    public boolean variableShownInTableDataSource(String variable, TableDataSource dataSource) {
        List<Map<String, String>> table = dataSource.readTableForVariable(variable);
        List<Integer> years = dataSource.getYearsFromTable(table);

        if (years.size() < 2) {
            return false;
        }

        Collections.sort(years);

        for (int i = 0; i < years.size() - 1; i++) {
            int year1 = years.get(0);
            int year2 = years.get(1);

            Map<FIPS, Double> values1 = dataSource.getValuesFromTable(table, year1);
            Map<FIPS, Double> values2 = dataSource.getValuesFromTable(table, year2);

            for (FIPS fips : locations) {
                if (!values1.containsKey(fips) || !values2.containsKey(fips)) {
                    return false;
                }

                double value1 = values1.get(fips);
                double value2 = values2.get(fips);
                if (!performOperation(value1, value2)) {
                    return false;
                }
            }
        }

        return true;
    }
}
