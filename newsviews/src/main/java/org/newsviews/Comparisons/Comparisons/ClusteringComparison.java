package org.newsviews.Comparisons.Comparisons;

/**
 * Created by Josh on 3/17/16.
 */
public class ClusteringComparison extends MoransComparison {

    public ClusteringComparison() {
        super(0.2, false);
    }

    public String comparisonType() { return "cluster"; }
}
