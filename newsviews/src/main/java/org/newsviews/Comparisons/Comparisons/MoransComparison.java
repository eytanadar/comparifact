package org.newsviews.Comparisons.Comparisons;

import org.json.JSONArray;
import org.json.JSONObject;
import org.newsviews.Comparisons.Comparisons.Morans.Morans;
import org.newsviews.DataStructures.FIPS;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Josh on 3/17/16.
 */
public abstract class MoransComparison extends Comparison {
    List<FIPS> locations;
    double threshold;
    boolean isMin;

    public MoransComparison(double threshold, boolean isMin) {
        this.threshold = threshold;
        this.isMin = isMin;

        Morans.loadFileIfNeeded();
        locations = new ArrayList<FIPS>();
    }

    public void loadFromJSON(JSONObject json) {
        locations = new ArrayList<FIPS>();

        JSONArray loc = json.getJSONArray("locations");
        for (int i = 0; i < loc.length(); i++) {
            String fipsString = loc.getString(i);
            FIPS fips = FIPS.FIPSFromName(fipsString);
            locations.add(fips);
        }
    }

    @Override
    public boolean dataSetPasses(Map<FIPS, Double> map) {
        Map<FIPS, Double> result = Morans.I(map);

        for (FIPS fips : locations) {
            if (!result.containsKey(fips)) {
                continue;
            }

            if (isMin && !(result.get(fips) < threshold)) {
                return false;
            }
            if (!isMin && !(result.get(fips) > threshold)) {
                return false;
            }
        }

        return true;
    }
}
