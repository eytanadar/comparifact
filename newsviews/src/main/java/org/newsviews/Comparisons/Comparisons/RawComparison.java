package org.newsviews.Comparisons.Comparisons;

import org.json.JSONObject;
import org.newsviews.DataStructures.FIPS;

import java.util.Map;

/**
 * Created by Josh on 3/17/16.
 */
public abstract class RawComparison extends Comparison {
    Operation operation = Operation.EQUAL;
    boolean swapArguments = false;

    private enum Operation {
        MUCH_GREATER,
        GREATER,
        EQUAL,
        APPROXIMATELY_EQUAL
    };

    public RawComparison() {

    }

    public void loadFromJSON(JSONObject json) {
        swapArguments = false;
        operation = Operation.EQUAL;

        String comparison = json.getString("comp");
        if (comparison.equals(">>")) {
            operation = Operation.MUCH_GREATER;
        } else if (comparison.equals(">")) {
            operation = Operation.GREATER;
        } else if (comparison.equals("=")) {
            operation = Operation.EQUAL;
        } else if (comparison.equals("~=")) {
            operation = Operation.APPROXIMATELY_EQUAL;
        } else if (comparison.equals("<")) {
            operation = Operation.GREATER;
            swapArguments = true;
        } else if (comparison.equals("<<")) {
            operation = Operation.MUCH_GREATER;
            swapArguments = true;
        }
    }

    boolean performOperation(double firstValue, double secondValue) {
        // Perform the comparison
        switch (operation) {
            case MUCH_GREATER:
                //TODO: how do we implement this?!?!?
                return firstValue > secondValue;
            case GREATER:
                return firstValue > secondValue;
            case EQUAL:
                return firstValue == secondValue;
            case APPROXIMATELY_EQUAL:
                //TODO: What is the correct threshold?
                return within(firstValue, secondValue);
        }

        // Should never get here
        return false;
    }
}
