package org.newsviews.Comparisons.Comparisons;

import org.json.JSONArray;
import org.json.JSONObject;
import org.newsviews.DataStructures.FIPS;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Josh on 3/9/16.
 */
public class ValueComparison extends Comparison {
    List<FIPS> locations;
    double value;

    public ValueComparison() {
        locations = new ArrayList<FIPS>();
    }

    public String comparisonType() {
        return "value";
    }

    public void loadFromJSON(JSONObject json) {
        JSONArray loc = json.getJSONArray("locations");
        for (int i = 0; i < loc.length(); i++) {
            String fipsString = loc.getString(i);
            FIPS fips = FIPS.FIPSFromName(fipsString);
            locations.add(fips);
        }

        value = Double.parseDouble(json.getString("value"));
    }

    @Override
    public boolean dataSetPasses(Map<FIPS, Double> map) {
        for (FIPS code : locations) {
            if (!map.containsKey(code) || !within(value, map.get(code))) {
                return false;
            }
        }

        return true;
    }
}
