package org.newsviews.Comparisons.Comparisons;

import org.json.JSONArray;
import org.json.JSONObject;
import org.newsviews.DataStructures.FIPS;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Josh on 3/9/16.
 */
public class ComparisonComparison extends RawComparison {
    List<FIPS> firstLocation, secondLocation;

    public ComparisonComparison() {

    }

    public String comparisonType() {
        return "comparison";
    }

    public void loadFromJSON(JSONObject json) {
        super.loadFromJSON(json);

        firstLocation = new ArrayList<FIPS>();
        secondLocation = new ArrayList<FIPS>();

        JSONArray firstLocs = json.getJSONArray("loc1");
        for (int i = 0; i < firstLocs.length(); i++) {
            firstLocation.add(FIPS.FIPSFromName(firstLocs.getString(i)));
        }

        JSONArray secondLocs = json.getJSONArray("loc2");
        for (int i = 0; i < secondLocs.length(); i++) {
            secondLocation.add(FIPS.FIPSFromName(secondLocs.getString(i)));
        }

        if (swapArguments) {
            List<FIPS> temp = firstLocation;
            firstLocation = secondLocation;
            secondLocation = temp;
        }
    }

    @Override
    public boolean dataSetPasses(Map<FIPS, Double> map) {
        double firstValue, secondValue;

        for (FIPS f : firstLocation) {
            for (FIPS s : secondLocation) {
                // Get the first location's value
                if (map.containsKey(f)) {
                    firstValue = map.get(f);
                } else {
                    return false;
                }

                // Get the seoncd location's value
                if (map.containsKey(s)) {
                    secondValue = map.get(s);
                } else {
                    return false;
                }

                if (!performOperation(firstValue, secondValue)) {
                    return false;
                }
            }
        }

        return true;
    }
}
