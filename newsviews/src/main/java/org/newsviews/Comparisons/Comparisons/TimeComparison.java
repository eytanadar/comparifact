package org.newsviews.Comparisons.Comparisons;

import org.json.JSONArray;
import org.json.JSONObject;
import org.newsviews.DataStructures.FIPS;
import org.newsviews.Database.Tables.TableDataSource;
import org.newsviews.Utilities.DateFormatter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by Josh on 3/17/16.
 */
public class TimeComparison extends RawComparison {
    List<FIPS> locations;
    Date firstDate, secondDate;

    public TimeComparison() {
        locations = new ArrayList<FIPS>();
    }

    public String comparisonType() {
        return "time-comparison";
    }

    public void loadFromJSON(JSONObject json) {
        super.loadFromJSON(json);

        String firstDate = json.getString("date1");
        this.firstDate = DateFormatter.parse(firstDate);

        String secondDate = json.getString("date2");
        this.secondDate = DateFormatter.parse(secondDate);

        if (swapArguments) {
            Date temp = this.firstDate;
            this.firstDate = this.secondDate;
            this.secondDate = temp;
        }

        JSONArray loc = json.getJSONArray("locations");
        for (int i = 0; i < loc.length(); i++) {
            String fipsString = loc.getString(i);
            FIPS fips = FIPS.FIPSFromName(fipsString);
            locations.add(fips);
        }
    }

    @Override
    public boolean variableShownInTableDataSource(String variable, TableDataSource dataSource) {
        List<Map<String, String>> table = dataSource.readTableForVariable(variable);
        List<Integer> years = dataSource.getYearsFromTable(table);

        int firstYear = -1, secondYear = -1;
        for (int i = 0; i < years.size(); i++) {
            int year = years.get(i);
            if (firstDate.getYear() == year) {
                firstYear = year;
            }
            if (secondDate.getYear() == year) {
                secondYear = year;
            }
        }

        if (firstYear < 0 || secondYear < 0) {
            return false;
        }

        Map<FIPS, Double> values1 = dataSource.getValuesFromTable(table, firstYear);
        Map<FIPS, Double> values2 = dataSource.getValuesFromTable(table, secondYear);

        for (FIPS fips : locations) {
            if (!values1.containsKey(fips) || !values2.containsKey(fips)) {
                return false;
            }

            double value1 = values1.get(fips);
            double value2 = values2.get(fips);
            if (!performOperation(value1, value2)) {
                return false;
            }
        }

        return true;
    }
}
