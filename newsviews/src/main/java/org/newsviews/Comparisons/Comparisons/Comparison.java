package org.newsviews.Comparisons.Comparisons;

import org.json.JSONObject;
import org.newsviews.DataStructures.FIPS;
import org.newsviews.Database.Tables.TableDataSource;

import java.util.List;
import java.util.Map;

/**
 * Created by Josh on 3/9/16.
 */
public abstract class Comparison {
    abstract public String comparisonType();

    protected boolean within(double a, double b) {
        return within(a, b, 0.01);
    }

    protected boolean within(double a, double b, double threshold) {
        return Math.abs(a - b) <= threshold;
    }

    public abstract void loadFromJSON(JSONObject json);
    public boolean dataSetPasses(Map<FIPS, Double> map) {
        return false;
    }

    public boolean variableShownInTableDataSource(String variable, TableDataSource dataSource) {
        List<Map<String, String>> table = dataSource.readTableForVariable(variable);
        List<Integer> years = dataSource.getYearsFromTable(table);

        int year = 0;
        if (years.size() > 0) {
            year = years.get(0);
        }

        Map<FIPS, Double> valuesFromTable = dataSource.getValuesFromTable(table, year);
        return dataSetPasses(valuesFromTable);
    }
}
