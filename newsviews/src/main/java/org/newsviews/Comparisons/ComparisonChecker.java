package org.newsviews.Comparisons;

import org.json.JSONArray;
import org.json.JSONObject;
import org.newsviews.Comparisons.Comparisons.*;
import org.newsviews.DataStructures.FIPS;
import org.newsviews.Database.Tables.TableDataSource;
import org.newsviews.Utilities.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Josh on 3/9/16.
 */
public class ComparisonChecker {
    private Map<String, Comparison> comparisonsMap;
    public ComparisonChecker() {
        List<Comparison> list = new ArrayList<Comparison>();
        list.add(new ComparisonComparison());
        list.add(new ValueComparison());
        list.add(new ClusteringComparison());
        list.add(new OutlierComparison());
        list.add(new TimeComparison());
        list.add(new TrendComparison());

        comparisonsMap = new HashMap<String, Comparison>();
        for (Comparison c : list) {
            comparisonsMap.put(c.comparisonType(), c);
        }
    }

    public List<Boolean> passingComparisons(JSONArray comparisons, String variable, TableDataSource dataSource) {
        List<Boolean> result = new ArrayList<Boolean>();

        for (int i = 0; i < comparisons.length(); i++) {
            JSONObject comparison = comparisons.getJSONObject(i);
            String type = comparison.getString("type");
            Comparison c = comparisonsMap.get(type);

            result.add(c.variableShownInTableDataSource(variable, dataSource));
        }

        return result;
    }

    public List<Boolean> passingComparisons(JSONArray comparisons, Map<FIPS, Double> map) {
        List<Boolean> result = new ArrayList<Boolean>();

        for (int i = 0; i < comparisons.length(); i++) {
            try {
                JSONObject comparison = comparisons.getJSONObject(i);
                String type = comparison.getString("type");
                Comparison c = comparisonsMap.get(type);

                c.loadFromJSON(comparison);
                result.add(c.dataSetPasses(map));
            } catch (Exception e) {
                Logger.Log(e);
            }
        }

        return result;
    }
}
