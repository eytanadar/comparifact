//
//  ViewController.swift
//  DatasetGatherer
//
//  Created by Josh Ford on 1/6/16.
//  Copyright © 2016 Josh Ford. All rights reserved.
//

import Cocoa
import WebKit

class ViewController: NSViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override var representedObject: AnyObject? {
        didSet {
        // Update the view, if already loaded.
        }
    }
    
    
    let ReadabilityAPIKey = "042e3228d910e85f442d5a00e95268ff71ccdd28"
    let ReadabilityBaseURL = "https://readability.com/api/content/v1"
    
    func addAticle() {
        let a = Article()
        a.url = webView?.url as? String ?? ""
        
        if let lastArticle = arrayController.selectedObjects.first as? Article {
            a.publisher = lastArticle.publisher
        }
        
        // Use readability to get information about the page
        let url = ReadabilityBaseURL + "/parser?token=" + ReadabilityAPIKey + "&url=" + a.url.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLHostAllowedCharacterSet())!
        do {
            let u = NSURL(string: url)!
            let data = try NSData(contentsOfURL: u, options: NSDataReadingOptions())
            let json = try NSJSONSerialization.JSONObjectWithData(data, options:
            NSJSONReadingOptions())
            
            let contentString = json["content"] as! String
            let content = try NSAttributedString(data: contentString.dataUsingEncoding(NSUTF8StringEncoding)!, options: [NSDocumentTypeDocumentAttribute : NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: NSNumber(unsignedInteger: NSUTF8StringEncoding)], documentAttributes: nil)
            a.text = content.string
            
            a.title = json["title"] as? String ?? ""
            a.publishDate = json["date_published"] as? String ?? ""
        } catch {
            
        }
        
        arrayController.addObject(a)
    }
    func removeArticle() {
        arrayController.removeObjects(arrayController.selectedObjects)
    }
    
    override func prepareForSegue(segue: NSStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "ArticleController") {
            let articleController = segue.destinationController as! ArticleViewController
            articleController.bind("articles", toObject: arrayController, withKeyPath: "selectedObjects", options: nil)
        }
    }
    
    @objc var articles: NSMutableArray? = NSMutableArray()

    @IBOutlet var webView: PrivateWebView?
    @IBOutlet var arrayController: NSArrayController!
    @IBOutlet var tableView: NSTableView!
}

