//
//  ImageToDataTransformer.m
//  DatasetGatherer
//
//  Created by Josh Ford on 2/9/16.
//  Copyright © 2016 Josh Ford. All rights reserved.
//

#import <AppKit/AppKit.h>
#import "ImageToDataTransformer.h"

NSString * const ImageToDataTransformerName = @"ImageToDataTransformer";

@implementation ImageToDataTransformer

+ (void)initialize {
    if (self == [ImageToDataTransformer class]) {
        [NSValueTransformer setValueTransformer:[[ImageToDataTransformer alloc] init] forName:ImageToDataTransformerName];
    }
}

+ (Class)transformedValueClass {
    return [NSImage class];
}

+ (BOOL)allowsReverseTransformation {
    return YES;
}

- (id)reverseTransformedValue:(id)value {
    if ([value isKindOfClass:[NSImage class]]) {
        CGImageRef cgImage = [(NSImage *)value CGImageForProposedRect:NULL context:nil hints:nil];
        NSBitmapImageRep *newRep = [[NSBitmapImageRep alloc] initWithCGImage:cgImage];
        [newRep setSize:[(NSImage *)value size]];
        return [newRep representationUsingType:NSPNGFileType properties:@{}];
    } else if ([value isKindOfClass:[NSData class]]) {
        return value;
    }
    
    return nil;
}

- (id)transformedValue:(id)value {
    if ([value isKindOfClass:[NSData class]]) {
        return [[NSImage alloc] initWithData:value];
    } else if ([value isKindOfClass:[NSImage class]]) {
        return value;
    }
    
    return nil;
}

@end
