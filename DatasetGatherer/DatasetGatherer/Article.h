//
//  Article.h
//  DatasetGatherer
//
//  Created by Josh Ford on 1/6/16.
//  Copyright © 2016 Josh Ford. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AppKit/AppKit.h>

typedef enum : NSUInteger {
    GraphTypeOther = 0,
    GraphTypeSpatial = 1,
    GraphTypeTemporal = 2
} GraphType;

@interface Graph : NSObject <NSCoding>

@property (nonatomic) NSString *variable;
@property (nonatomic) NSString *source;
@property (nonatomic) NSNumber *type;
@property (nonatomic) NSData *image;

- (instancetype)init;

@end

@interface Article : NSObject <NSCoding>

@property (nonatomic) NSString *title;
@property (nonatomic) NSString *publishDate;
@property (nonatomic) NSString *publisher;
@property (nonatomic) NSString *url;
@property (nonatomic) NSString *text;
@property (nonatomic) NSMutableArray<Graph *> *graphs;

- (instancetype)init;

@end
