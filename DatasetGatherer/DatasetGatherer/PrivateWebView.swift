//
//  PrivateWebView.swift
//  DatasetGatherer
//
//  Created by Josh Ford on 1/6/16.
//  Copyright © 2016 Josh Ford. All rights reserved.
//

import Cocoa
import WebKit

class PrivateWebView: NSView, WKNavigationDelegate {
    let webView: WKWebView
    override init(frame frameRect: NSRect) {
        let config = WKWebViewConfiguration();
        config.websiteDataStore = WKWebsiteDataStore.nonPersistentDataStore()
        webView = WKWebView(frame: frameRect, configuration: config)
        
        super.init(frame: frameRect)
        loadWebView()
    }
    required init?(coder: NSCoder) {
        let config = WKWebViewConfiguration();
        config.websiteDataStore = WKWebsiteDataStore.nonPersistentDataStore()
        webView = WKWebView(frame: CGRect.zero, configuration: config)
        
        super.init(coder: coder)
        loadWebView()
    }
    
    func loadWebView() {
        webView.allowsBackForwardNavigationGestures = true
        webView.allowsMagnification = true
        webView.wantsLayer = true
        
        webView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(webView)
        addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("|[webView]|", options: NSLayoutFormatOptions(), metrics: nil, views: ["webView" : webView]));
        addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[webView]|", options: NSLayoutFormatOptions(), metrics: nil, views: ["webView" : webView]));
        
        webView.bind("canGoBack", toObject: self, withKeyPath: "canGoBack", options: nil)
        webView.bind("canGoForward", toObject: self, withKeyPath: "canGoForward", options: nil)
        
        self.bind("canGoBack", toObject: webView, withKeyPath: "canGoBack", options: nil)
        self.bind("canGoForward", toObject: webView, withKeyPath: "canGoForward", options: nil)
        
        url = "https://www.google.com/"
        webView.navigationDelegate = self
    }
    
    func clearBrowsingData() {
        let types = Set<String>(arrayLiteral: WKWebsiteDataTypeCookies, WKWebsiteDataTypeSessionStorage, WKWebsiteDataTypeLocalStorage)
        webView.configuration.websiteDataStore.fetchDataRecordsOfTypes(types) { data in
            self.webView.configuration.websiteDataStore.removeDataOfTypes(types, forDataRecords: data, completionHandler: {});
        }
    }
    
    func webView(webView: WKWebView, didFinishNavigation navigation: WKNavigation!) {
        (window?.windowController as? WindowController)?.setURL(webView.URL!.absoluteString)
        url = webView.URL!.absoluteString
    }
    
    @objc var canGoBack: Bool = false
    @objc var canGoForward: Bool = false
    @objc var url: NSString? {
        didSet {
            if let string = url, var u = NSURL(string: string as String) where oldValue != url {
                if u.scheme.isEmpty {
                    u = NSURL(string: "http://" + (string as String))!
                }
                
                webView.loadRequest(NSURLRequest(URL: u))
            }
        }
    }
    
    @IBAction func reload(sender: AnyObject) {
        clearBrowsingData()
        webView.reload()
    }
    
    @IBAction func back(sender: AnyObject) {
        webView.goBack()
    }
    
    @IBAction func forward(sender: AnyObject) {
        webView.goForward()
    }
}
