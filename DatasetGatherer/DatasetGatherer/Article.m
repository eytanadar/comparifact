//
//  Article.m
//  DatasetGatherer
//
//  Created by Josh Ford on 1/6/16.
//  Copyright © 2016 Josh Ford. All rights reserved.
//

#import "Article.h"

@implementation Graph

- (instancetype)init {
    self = [super init];
    if (self) {
        self.variable = @"";
        self.source = @"";
        self.type = @(GraphTypeSpatial);
        self.image = nil;
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    if (self) {
        self.variable = [aDecoder decodeObjectForKey:@"variable"];
        self.source = [aDecoder decodeObjectForKey:@"source"];
        self.type = [aDecoder decodeObjectForKey:@"type"];
        id data = [aDecoder decodeObjectForKey:@"image"];
        if ([data isKindOfClass:[NSData class]]) {
            self.image = data;
        } else {
            // It is an NSImage
            CGImageRef cgImage = [(NSImage *)data CGImageForProposedRect:NULL context:nil hints:nil];
            NSBitmapImageRep *newRep = [[NSBitmapImageRep alloc] initWithCGImage:cgImage];
            [newRep setSize:[(NSImage *)data size]];
            self.image = [newRep representationUsingType:NSPNGFileType properties:@{}];
        }
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.variable forKey:@"variable"];
    [aCoder encodeObject:self.source forKey:@"source"];
    [aCoder encodeObject:self.type forKey:@"type"];
    [aCoder encodeObject:self.image forKey:@"image"];
}

@end

@implementation Article

- (instancetype) init {
    self = [super init];
    if (self) {
        self.title = @"";
        self.publishDate = @"";
        self.publisher = @"";
        self.url = @"";
        self.text = @"";
        self.graphs = [NSMutableArray array];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    if (self) {
        self.title = [aDecoder decodeObjectForKey:@"title"];
        self.publisher = [aDecoder decodeObjectForKey:@"publisher"];
        self.publishDate = [aDecoder decodeObjectForKey:@"publishDate"];
        self.url = [aDecoder decodeObjectForKey:@"url"];
        self.text = [aDecoder decodeObjectForKey:@"text"];
        self.graphs = [aDecoder decodeObjectForKey:@"graphs"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.title forKey:@"title"];
    [aCoder encodeObject:self.publisher forKey:@"publisher"];
    [aCoder encodeObject:self.publishDate forKey:@"publishDate"];
    [aCoder encodeObject:self.url forKey:@"url"];
    [aCoder encodeObject:self.text forKey:@"text"];
    [aCoder encodeObject:self.graphs forKey:@"graphs"];
}

@end
